<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('news_category_id')->unsigned()->nullable();
            $table->string('image')->nullable();
            $table->integer('status')->unsigned()->default(0);
            $table->longText('content')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::table('news', function (Blueprint $table) {
            $table->foreign('news_category_id')->references('id')->on('news_categories')->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropForeign(['news_category_id']);
        });        
        Schema::dropIfExists('news');
    }
}
