<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table){
            $table->string('username')->unique();
            $table->string('phone_number')->nullable()->unique();
            $table->string('address')->nullable();
            $table->integer('level')->default(0);
            $table->integer('active')->unsigned()->default(0);
            $table->integer('status')->unsigned()->default(0);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table){
            $table->dropColumn('username');
            $table->dropColumn('phone_number');
            $table->dropColumn('address');
            $table->dropColumn('status');
            $table->dropColumn('active');
            $table->dropColumn('level');
            $table->dropSoftDeletes();
        });    
    }
}
