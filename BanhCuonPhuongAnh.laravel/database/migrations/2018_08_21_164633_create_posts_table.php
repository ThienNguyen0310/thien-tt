<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->integer('post_category_id')->unsigned()->nullable();
            $table->longText('description')->nullable();
            $table->longText('content')->nullable();
            $table->integer('status')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();
            
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('post_category_id')->references('id')->on('post_categories')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['post_category_id']);
        });
        Schema::dropIfExists('posts');
    }
}
