var responsiveflag = false;
$(document).ready(function () {

    new WOW().init();

    var animationDelay = 100;
    $("#block_ads article").each(function () {
        animationDelay = animationDelay * 2;
        $(this).css('animation-delay', animationDelay + 'ms');
    });

    var animationDelay2 = 100;
    $(".block_grid article").each(function () {
        animationDelay2 = animationDelay2 + 150;
        $(this).css('animation-delay', animationDelay2 + 'ms');
    });
    

    $('#bxslider').bxSlider({
        captions: true,
        preloadImages: 'all'
    });

    $('.scroller .owl-carousel').each(function () {
        $(this).owlCarousel({
            items: 3,
            lazyLoad: true,
            navigation: true,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [768, 2],
            itemsTabletSmall: [480, 2],
            itemsMobile: [360, 1],
        });
    });

//    $('a[href^="#"]').on('click', function (event) {
//        var target = $($(this).attr('href'));
//        if (target.length) {
//            event.preventDefault();
//            $('html, body').animate({
//                scrollTop: target.offset().top
//            }, 1000);
//        }
//
//    });
});