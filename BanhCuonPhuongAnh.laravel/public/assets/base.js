var App = App || {};

!function (window, $, App) {
    App.User = App.User || {};
    App.Url = App.Url || {};

    App.log = function (msg) {
        if (typeof console == 'object') {
            window.console.log(msg);
        }
    };

    App.on = function (event, element, callback) {
        /* Used jQbox-App-likeuery 1.7 event handler */
        $('body').on(event, element, function (e) {
            e.preventDefault();
            callback.apply(this, arguments); // Used arguments to fixed error in IE
        });
    };

    App.off = function (event, element) {
        /* Used jQuery 1.7 event handler */
        // $(element).die(event);
    };

    App.ajax = function (settings) {
        $.ajax({
            type: settings.type ? settings.type : 'get',
            url: settings.url ? settings.url : '',
            data: settings.data ? settings.data : '',
            dataType: settings.dataType ? settings.dataType : 'json',
            timeout: 10000, // 10 seconds,
            beforeSend: function (xhr, token) {
                token = $('meta[name="csrf-token"]').attr('content');
                xhr.setRequestHeader('X-CSRF-TOKEN', token);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (App.debugMode) {
                    console.log('XHR error: ' + textStatus);
                    console.log('HTTP error: ' + errorThrown);
                }
                if (settings.error)
                    settings.error.apply(this);
            },
            success: function (data) {
                settings.success(data);
                if (App.callback) {
                    App.callback.apply(this);
                }
            }
        });
    };

    App.alert = function ($msg) {
        alert($msg);
    };

    App.openPopup = function (url, w, h, popupName) {
        popupName = popupName ? popupName : '';
        var left = (window.screen.width / 2) - (w / 2),
            top = (window.screen.height / 2) - (h / 2),
            targetWin = window.open(url, popupName, 'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        return targetWin;
    };

    App.openHiddenPopup = function (url, w, h, popupName) {
        popupName = popupName ? popupName : '';
        var popupProps = !w || !h ? '' : "width=" + w + ",height=" + h + ",location=no,toolbar=no,menubar=no, status=no, scrollbars=yes,resizable=no",
            _parent = window.self,
            openWindow;
        if (window.top != window.self) {
            try {
                if (window.top.document.location.toString()) {
                    _parent = window.top
                }
            } catch (err) {
            }
        }

        if ($.browser.webkit) {
            var a = _parent.window.document.createElement("a");
            a.href = url;
            var evt = _parent.window.document.createEvent("MouseEvents");

            // the tenth parameter of initMouseEvent sets ctrl key
            evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, true, false, false, false, 0, null);
            a.dispatchEvent(evt);
            return null;
        }

        openWindow = _parent.window.open(url, popupName, popupProps);
        if (openWindow) {
            openWindow.blur();
            // IE < 10
            if ($.browser.msie && $.browser.version < 10) {
                window.focus();
                try {
                    openWindow.opener.window.focus();
                } catch (err) {
                }
            } else {
                // Trick Mozilla
                if (typeof openWindow.window.mozPaintCount != "undefined") {
                    var x = openWindow.window.open("about:blank");
                    x.close();
                }
                try {
                    openWindow.opener.window.focus();
                } catch (err) {
                }
            }
        }

        return openWindow;
    };

    /*
     * Store string
     */
    App.Storage = function () {
        var _storage = null;
        // Use HTML5 Storage, cookies is fallback
        if (typeof (window.Storage) != 'undefined') {
            _storage = window.localStorage;
        }

        var getStore = function (key) {
                key = String(key);
                if (_storage) {
                    return _storage[key];
                }
                return $.cookie(key);
            },
            setStore = function (key, value) {
                key = String(key);
                value = value ? String(value) : null;
                if (_storage) {
                    if (!value) {
                        delete _storage[key];
                    } else {
                        _storage[key] = value;
                    }
                } else {
                    $.cookie(key, value);
                }
                return value;
            };

        return {
            get: function (key) {
                return getStore(key);
            },
            set: function (key, value) {
                return setStore(key, value);
            }
        }
    }();

    App.refresh = function (url, timeout) {
        timeout = typeof (timeout) == 'undefined' ? 0 : timeout;
        return window.setTimeout(function () {
            if (typeof (url) == 'undefined' || url === '') {
                window.location.reload()
            } else {
                window.location = url;
            }
        }, timeout);
    };

    App.dataURItoBlob = function (dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: mimeString});
    };

    App.scriptLoadFile = function (url) {
        var script = document.createElement('script');

        script.src = url;

        document.getElementsByTagName('head')[0].appendChild(script);
    };

    App.lazyEval = function () {
        var lazyElement = document.getElementsByClassName('lazyEval');

        var i;
        for (i = 0; i < lazyElement.length; i++) {
            eval(App.stripOutCommentBlock(lazyElement[i].innerHTML));
        }
    };

    App.stripOutCommentBlock = function (code) {
        return code.replace(/^[\s\xA0]+\/\*|\*\/[\s\xA0]+$/g, "");
    };

    // JS one-liner function:
    // -----------------------
    App.lazyEvalOneLiner = function (id) {
        (0, eval)(document.getElementById(id).innerHTML.replace(/^[\s\xA0]+\/\*|\*\/[\s\xA0]+$/g, ""))
    };

    // DOM ready
    $(function () {


    }); // End DOM ready
}(window, window.jQuery, window.App); // put semicolon to prevent error when minify script