!function (window, $, App) {
    App.onFileSelected = function (event, element) {
        var selectedFile = event.target.files[0];
        var reader = new FileReader();

        reader.onload = function (event) {
            element.attr('src', event.target.result);
        };

        reader.readAsDataURL(selectedFile);
    };

    App.setThumbAspectRatio = function (ratio) {
        var width = 150, height = 150 / ratio;

        $(".image-crop > img").cropper("setAspectRatio", ratio);

        $('#img-preview').height(height);
    };

    // DOM ready
    $(function () {
        $('body').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('table').on('click', ".check_all", function () {
            var checked = $('.check_all').is(':checked');
            $(this).closest('table').find('.check_item').each(function () {
                if ($(this).is(':checked') != checked)
                    $(this).click();
            });
        });

        $('.delete_item').click(function () {
            var __this = $(this);

            if (confirm('Bạn có muốn thực hiện hành động này hahahahaha?')) {
                App.ajax({
                    url: __this.attr('href'),
                    type: 'PATCH',
                    success: function (data) {
                        App.refresh();
                    }
                });
            }

            return false;
        });

        $('._check_add_to').click(function () {
            var _url = $(this).attr('href'), _target = $('#' + $(this).data('target')), _items = new Array();

            _target.find("input[type=checkbox]:checked").each(function () {
                if ('' != $(this).val())
                    _items.push(parseInt($(this).val()));
            });

            App.ajax({
                url: _url,
                type: 'post',
                data: {
                    ids: _items
                },
                success: function (data) {
                    App.refresh(_url);
                }
            });

            return false;
        });
    }); // End DOM ready
}(window, window.jQuery, window.App); // put semicolon to prevent error when minify script