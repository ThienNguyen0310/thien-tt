<!DOCTYPE html>
<html lang="en">
<head>
    @yield('meta_javascript')
    <title>@yield('title')</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">CMS</h1>

        </div>
        <h3>Trang quản trị </h3>

    @yield('content')

            <p class="m-t">
                <small>CMS &copy; @2017  by ThienNV</small>
            </p>
        </div>
    </div>

    
    <script src="{{asset('js/jquery-2.1.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    
</body>
</html>