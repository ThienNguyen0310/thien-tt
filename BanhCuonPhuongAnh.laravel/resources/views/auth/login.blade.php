@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning">
            {{ session('warning') }}
        </div>
    @endif

    <p>Điền đầy đủ thông tin để đăng nhập.</p>   
        <form method="POST" action="{{route('login')}}" accept-charset="UTF-8" class="m-t">
            @csrf

            <div class="form-group">
                <input class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="Tài khoản" required="" name="username" type="text" value="{{ old('email') }}">
                @if ($errors->has('username'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Mật khẩu" required="" name="password" type="password" >
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <!-- <div class="form-group">
                <label for="remember" class="col-sm-2 control-label">Remember:</label>
                <div class="col-sm-10">
                    <input name="active" type="checkbox" {{ old('remember') ? 'checked' : '' }} id="remmember">
                </div>
            </div> -->

            <div class="form-group">
                <input class="btn btn-primary block full-width m-b" type="submit" value="Đăng nhập">
                <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
            </div>
        </form>
@endsection
