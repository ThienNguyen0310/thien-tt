@extends('layouts.app')

@section('content')
    <div>{{ __('Reset Password') }}</div>
    <form method="POST" action="{{ route('password.request') }}" accept-charset="UTF-8" class="m-t">
            @csrf
            <div class="form-group">
                <input id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" required="" name="email" type="email" value="{{ $email ?? old('email') }}">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <input id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Mật khẩu" required="" name="password" type="password" >
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <input id="password-confirm" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Xác thực Mật khẩu" required="" name="password_confirmation" type="password" >
            </div>

            <div class="form-group">
                <input class="btn btn-primary block full-width m-b" type="submit" value="Reset Password">
            </div>
        </form>
@endsection
