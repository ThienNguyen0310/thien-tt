@extends('layouts.app')

@section('content')
<div>{{ __('Reset Password') }}</div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
                {{ session('status') }}
        </div>
    @endif
    <form method="POST" action="{{ route('password.email') }}" accept-charset="UTF-8" class="m-t">
            @csrf
            <div class="form-group">
                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" required="" name="email" type="email" value="{{ $email ?? old('email') }}">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <input class="btn btn-primary block full-width m-b" type="submit" value="Reset Password">
            </div>
        </form>
@endsection
