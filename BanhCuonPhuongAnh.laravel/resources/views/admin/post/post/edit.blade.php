@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="yElcQk6pcPhyXnrUuy3PnEYoIazBY6tpr5uwgliE">
    <script>
        var csrf_token = 'yElcQk6pcPhyXnrUuy3PnEYoIazBY6tpr5uwgliE';
    </script>
@endsection
@section('title',$post->name)
@section('link')
    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    
    <style>
        .image-crop {
            background: #000;
            width: 100%;
        }

        .cropper-container {
            left: 0 !important;
            margin: 0 auto !important;
        }

        .img-preview-sm {
            width: 150px !important;
            height: 100px !important;
        }

        .lightBoxGallery {
            text-align: center;
        }

        .lightBoxGallery img {
            margin: 5px;
        }
    </style>
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{$post->name}}</h2>
            <ol class="breadcrumb">
                @if(Auth::user()->level)
                    <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                @else 
                    <li><a href="{{route('post.index')}}">Trang chủ</a></li>
                @endif
                <li><a href="{{route('post.index')}}">Danh sách Bài viết</a></li>
                <li><a href="{{route('post.show',$post->id)}}">Xem chi tiết</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <form method="POST" action="{{route('post.update',$post->id)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                <!-- <input name="_method" type="hidden" value="PATCH">
                <input name="_token" type="hidden" value="yElcQk6pcPhyXnrUuy3PnEYoIazBY6tpr5uwgliE"> -->
                @csrf
                @method('PATCH')
                <div class="row">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Thông tin Bài viết</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 control-label">Bài viết:</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" autocomplete="off" value="{{$post->name}}" placeholder="Tên Bài viết" name="name" type="text" id="title">
                                                <span class="text-danger">{{ $errors->first('name')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" autocomplete="off" value="{{$post->slug}}" placeholder="Slug" name="slug" type="text" id="slug">
                                                <span class="text-danger">{{ $errors->first('slug')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" name="description" cols="50" rows="10" id="description">{{$post->description}}</textarea>
                                                <span class="text-danger">{{ $errors->first('description')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="post_category_id" class="col-sm-2 control-label">Thể loại:</label>
                                            <div class="col-sm-10">
                                                <select id="categories" class="form-control chosen-select" data-placeholder="Chọn Thể loại" name="post_category_id">
                                                    <option value="">Thể loại cha</option>
                                                    @foreach($post_categories as $cat)
                                                        @if($cat->id == $post->post_category_id)
                                                            <option value="{{$cat->id}}" selected="selected">{{$cat->name}}</option>
                                                        @else
                                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="content" class="col-sm-2 control-label">Nội dung:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control summernote" name="content" cols="50" rows="10" id="content">{{$post->content}}</textarea>
                                                <span class="text-danger">{{ $errors->first('content')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="status" class="col-sm-2 control-label">Trạng thái:</label>
                                            <div class="col-sm-10">
                                                <input name="status" type="checkbox" id="active" {{ $post->status == 1 ? 'checked' : ''}}>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật</button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="post_id" value="{{$post->id}}">
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                onImageUpload: function (files, editor, welEditable) {
                    console.log(123);
                    sendFile(files[0], editor, welEditable);
                }
            });

            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);

                $.ajax({
                    type: "POST",
                    url: "/post/image-editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });
    </script>
@endsection