@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc">
    <script>
        var csrf_token = '5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{$post->name}}</h2>
            <ol class="breadcrumb">
                @if(Auth::user()->level)
                    <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                @else 
                    <li><a href="{{route('post.index')}}">Trang chủ</a></li>
                @endif
                <li><a href="{{route('post.index')}}">Danh sách Bài viết</a></li>
                <li><a href="{{route('post.edit',$post->id)}}">Sửa thông tin</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Thông tin chi tiết</h5>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="150px">#</th>
                                <th>Giá trị</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="150px">ID</td>
                                <td>{{$post->id}}</td>
                            </tr>
                            <tr>
                                <td>Bài viết</td>
                                <td><a href="{{'http://banhcuonphuonganh.com/bai-viet/'.$post->slug}}" target="_blank">{{$post->name}}</a></td>
                            </tr>
                            <tr>
                                <td>Mô tả</td>
                                <td>{{$post->description}}</td>
                            </tr>
                            <tr>
                                <td>Nội dung</td>
                                <td>{!!$post->content!!}</td>
                            </tr>
                            <tr>
                                <td>Tạo bởi</td>
                                <td>admin vào {{$post->created_at}}</td>
                            </tr>
                            <tr>
                                <td>Cập nhật bởi</td>
                                <td>admin vào {{$post->updated_at}}</td>
                            </tr>
                            <tr>
                                <td>Trạng thái</td>
                                <td>
                                    @if($post->status == 1)
                                        <span class="text-success">Xuất bản</span>
                                    @else
                                        <span class="text-warning">Chưa xuất bản</span>
                                    @endif
                            </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection