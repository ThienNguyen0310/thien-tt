@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD">
    <script>
        var csrf_token = 'vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD';
    </script>
@endsection
@section('title','Thêm bài viết')
@section('link')
    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Thêm Bài viết</h2>
            <ol class="breadcrumb">
                @if(Auth::user()->level)
                    <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                @else 
                    <li><a href="{{route('post.index')}}">Trang chủ</a></li>
                @endif
                <li><a href="{{route('post.index')}}">Danh sách Bài viết</a></li>
                <li>Thêm Bài viết</li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="POST" action="{{route('post.store')}}" accept-charset="UTF-8" class="form-horizontal">
                        <!-- <input name="_token" type="hidden" value="vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD"> -->
                        @csrf
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Bài viết:</label>
                            <div class="col-sm-10">
                                <input class="form-control" autocomplete="off" value="{{old('name')}}" placeholder="Tên Bài viết" name="name" type="text"  id="title">
                                <span class="text-danger">{{ $errors->first('name')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="slug" class="col-sm-2 control-label">Slug:</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{old('slug')}}" autocomplete="off" placeholder="Slug" name="slug" type="text" id="slug">
                                <span class="text-danger">{{ $errors->first('slug')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" cols="50" rows="10" id="description">{{old('description')}}</textarea>
                                <span class="text-danger">{{ $errors->first('description')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="post_category_id" class="col-sm-2 control-label">Thể loại:</label>
                            <div class="col-sm-10">
                                <select value="{{old('post_category_id')}}" class="form-control chosen-select" data-placeholder="Chọn Thể loại" id="category_id" name="post_category_id">
                                    <option value="">Thể loại cha</option>
                                    @foreach($post_categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="content" class="col-sm-2 control-label">Nội dung:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control summernote" name="content" cols="50" rows="10" id="content">{{old('content')}}</textarea>
                                <span class="text-danger">{{ $errors->first('content')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-sm-2 control-label">Trạng thái:</label>
                            <div class="col-sm-10">
                                <input name="status" type="checkbox" id="active">
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Thêm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                onImageUpload: function (files, editor, welEditable) {
                    console.log(123);
                    sendFile(files[0], editor, welEditable);
                }
            });

            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);

                $.ajax({
                    type: "POST",
                    url: "/post/image-editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });
    </script>
@endsection
