@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD">
    <script>
        var csrf_token = 'vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Thêm Thể loại</h2>
            <ol class="breadcrumb">
                @if(Auth::user()->level)
                    <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                @else 
                    <li><a href="{{route('post.index')}}">Trang chủ</a></li>
                @endif
                <li><a href="{{route('postcategory.index')}}">Danh sách Thể loại</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Thêm Thể loại</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="POST" action="{{route('postcategory.store')}}" accept-charset="UTF-8" class="form-horizontal">
                            <!-- <input name="_token" type="hidden" value="vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD"> -->
                            @csrf
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Thể loại:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="Nhập Thể loại" value="{{old('name')}}" autocomplete="off" name="name" type="text" id="name">
                                    <span class="text-danger">{{ $errors->first('name')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="slug" type="text" autocomplete="off" value="{{old('slug')}}" id="slug">
                                    <span class="text-danger">{{ $errors->first('slug')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" placeholder="Nhập mô tả" name="description" cols="50" rows="10" id="description">{{old('description')}}</textarea>
                                    <span class="text-danger">{{ $errors->first('description')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="meta_title" class="col-sm-2 control-label">Meta title:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" autocomplete="off" value="{{old('meta_title')}}" name="meta_title" type="text" id="meta_title">
                                    <span class="text-danger">{{ $errors->first('meta_title')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="meta_description" class="col-sm-2 control-label">Meta description:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="2" name="meta_description" cols="50" id="meta_description">{{old('meta_description')}}</textarea>
                                    <span class="text-danger">{{ $errors->first('meta_description')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="meta_keyword" class="col-sm-2 control-label">Meta keyword:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" autocomplete="off" value="{{old('meta_keyword')}}" name="meta_keyword" type="text" id="meta_keywords">
                                    <span class="text-danger">{{ $errors->first('meta_keyword')}}</span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Thêm </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection