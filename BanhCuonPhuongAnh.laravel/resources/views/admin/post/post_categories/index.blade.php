@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD">
    <script>
        var csrf_token = 'vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD';
    </script>
@endsection
@section('title','Thể loại tin tức')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Danh sách Thể loại</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li>Danh sách Thể loại</li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{route('postcategory.create')}}" class="btn btn-primary "><i class="fa fa-plus"></i> Thêm</a>
                            </div>
                        </div>
                        <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Thể loại</th>
                                        <th>Slug</th>
                                        <th width="100"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($post_categories as $category)
                                        <tr>
                                            <td>
                                                <a href="{{'http://banhcuonphuonganh.com/bai-viet/'.$category->slug}}"
                                                   target="_blank">{{$category->name}}</a>
                                            </td>
                                            <td>{{$category->slug}}</td>
                                            <td>
                                                <ul style="list-style-type:none; padding: 0px; margin:0">
                                                    <li style="float:left; margin-top: 6px">
                                                        <a class="btn btn-white btn-bitbucket" href="{{route('postcategory.edit',$category->id)}}" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i></a>
                                                    </li>
                                                    <li style="float:right; width:40px; margin-top: 6px">
                                                        <form method="POST" action="{{route('postcategory.destroy',$category->id)}}" accept-charset="UTF-8">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-white btnDelete" title="Xóa" data-toggle="tooltip"data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>   
                        <div style="float: right">
                            {{ $post_categories->links() }}
                        </div>     
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
</script>
@endsection