@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="TjPQHUlSMFGDmIxalQhNXpRFZC6X4ddMSLajjsKe">
    <script>
        var csrf_token = 'yElcQk6pcPhyXnrUuy3PnEYoIazBY6tpr5uwgliE';
    </script>
@endsection
@section('title',$menu->name)
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{$menu->name}}</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('menu.index')}}">Danh sách Thể loại</a></li>
                <li><a href="{{route('menu.show',$menu->id)}}">Xem chi tiết</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <form method="POST" action="{{route('menu.update',$menu->id)}}" accept-charset="UTF-8" class="form-horizontal">
            @csrf
            @method('PATCH')
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Menu:</label>
                        <div class="col-sm-10">
                            <input class="form-control" placeholder="Tên Menu" autocomplete="off" name="name" type="text" value="{{$menu->name}}" id="title">
                            <span class="text-danger">{{ $errors->first('name')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">Url:</label>
                        <div class="col-sm-10">
                            <input class="form-control" placeholder="Url" name="url" autocomplete="off" type="text" value="{{$menu->url}}" id="url">
                            <span class="text-danger">{{ $errors->first('url')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="parent_id" class="col-sm-2 control-label">Thể loại cha:</label>
                        <div class="col-sm-10">
                            <select class="form-control chosen-select" data-placeholder="Chọn Thể loại cha" id="parent_id" name="parent_id">
                                <option value="">Chọn Thể loại cha</option>
                                @foreach($menus as $m)
                                    @if($m->id == $menu->parent_id)
                                        <option value="{{$m->id}}" selected="selected">{{$m->name}}</option>
                                    @elseif($m->name != $menu->name)
                                        <option value="{{$m->id}}">{{$m->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="order" class="col-sm-2 control-label">Sắp xếp:</label>
                        <div class="col-sm-10">
                            <input class="form-control" placeholder="Sắp xếp" name="order" autocomplete="off" type="text" value="{{$menu->sort}}" id="order">
                            <span class="text-danger">{{ $errors->first('sort')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">Trạng thái:</label>
                        <input name="status" type="checkbox" id="active" {{ $menu->status == 1 ? 'checked' : ''}}>
                    </div>
                    <input type="hidden" name="menu_id" value="{{$menu->id}}">

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật
                            </button>
                        </div>
                    </div>
        </form>
    </div>
@endsection