@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD">
    <script>
        var csrf_token = 'vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD';
    </script>
@endsection
@section('title','Thêm Menu')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Thêm Menu</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('menu.index')}}">Danh sách Menu</a></li>
                <li>Thêm menu</li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="POST" action="{{route('menu.store')}}" accept-charset="UTF-8" class="form-horizontal">
                        @csrf
                        <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Menu:</label>
                        <div class="col-sm-10">
                            <input class="form-control" placeholder="Tên Menu" autocomplete="off" name="name" type="text" value="{{old('name')}}" id="title">
                            <span class="text-danger">{{ $errors->first('name')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">Url:</label>
                        <div class="col-sm-10">
                            <input class="form-control" placeholder="Url" autocomplete="off" name="url" type="text" value=" {{old('url')}}" id="url">
                            <span class="text-danger">{{ $errors->first('url')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="parent_id" class="col-sm-2 control-label">Thể loại cha:</label>
                        <div class="col-sm-10">
                            <select class="form-control chosen-select" value="{{old('parent_id')}}" data-placeholder="Chọn Thể loại cha" id="parent_id" name="parent_id">
                                <option value="">Thể loại cha</option>
                                @foreach($menus as $menu)
                                    <option value="{{$menu->id}}">{{$menu->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sort" class="col-sm-2 control-label">Sắp xếp:</label>
                        <div class="col-sm-10">
                            <input class="form-control" autocomplete="off" placeholder="Sắp xếp" name="sort" type="text" value="{{old('sort')}}" id="order">
                            <span class="text-danger">{{ $errors->first('sort')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">Trạng thái:</label>
                        <div class="col-sm-10">
                            <input name="status" type="checkbox" id="active">
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Thêm
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
