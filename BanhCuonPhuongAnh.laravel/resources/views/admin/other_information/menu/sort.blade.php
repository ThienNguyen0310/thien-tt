@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="q8WKwpSn0cPOIGv3npFPhZw2sYJWzIgkwpPeJjd5">
    <script>
        var csrf_token = 'q8WKwpSn0cPOIGv3npFPhZw2sYJWzIgkwpPeJjd5';
    </script>
@endsection
@section('title','Sắp xếp menu')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Sắp xếp Menu</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('product.index')}}">Trang chủ</a>
                </li>
            </ol>
        </div>
    </div>

    
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{route('menu.index')}}" class="btn btn-primary"><i class="fa fa-plus"></i>
                                    Dánh sách Menu</a>
                                <a href="{{route('menu.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm</a>

                                <div class="dd" id="nestable">
                                    <ol class="dd-list">
                                        @foreach($menu_parent as $menu)
                                            <li class="dd-item" data-id="{{$menu->id}}">
                                                <div class="dd-handle">{{$menu->name}}</div>
                                                <ol class="dd-list">
                                                    @if($menu->menu_children)
                                                        @foreach($menu->menu_children as $m)
                                                            <li class="dd-item" data-id="{{$m['id']}}">
                                                                <div class="dd-handle">{{$m['name']}}</div>
                                                                <ol class="dd-list"></ol>
                                                            </li>
                                                        @endforeach   
                                                    @endif
                                                </ol>
                                            </li>
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{asset('js/plugins/nestable/jquery.nestable.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target), output = list.data('output');

                if (window.JSON) {
                    var data = window.JSON.stringify(list.nestable('serialize'));

                    console.log(data);

                    $.ajax({
                        url: '/menu/orderStore',
                        type: 'post',
                        dataType:'json',
                        data: {
                            orders: data,
                            '_token': '{!! csrf_token() !!}'
                        },
                        success: function (data) {
                            console.log(data);
                        }
                    });
                } else {
                    console.log('JSON browser support required for this demo.');
                }
            };

            $('#nestable').nestable({
                group: 1
            }).on('change', updateOutput);
        });
    </script>
@endsection