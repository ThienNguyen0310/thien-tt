@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="q8WKwpSn0cPOIGv3npFPhZw2sYJWzIgkwpPeJjd5">
    <script>
        var csrf_token = 'q8WKwpSn0cPOIGv3npFPhZw2sYJWzIgkwpPeJjd5';
    </script>
@endsection
@section('title','Danh sách menu')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Danh sách menu</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
            </ol>
        </div>
    </div>
 
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{route('menu.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm</a>
                                <a href="{{route('menu.order')}}" class="btn btn-primary"><i class="fa fa-sort"></i> Sắp xếp</a>
                            </div>
                        </div>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tiêu đề</th>
                                    <th>Url</th>
                                    <th>Thể loại cha</th>
                                    <th>Sắp xếp</th>
                                    <th>Trạng thái</th>
                                    <th width="200"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($menus as $menu)
                                    <tr>
                                        <td>{{$menu->id}}</td>
                                        <td>
                                            @if($menu->parent_id)
                                                {{$menu->name}}
                                            @else 
                                                <b>{{$menu->name}}</b>
                                            @endif
                                        </td>
                                        <td>{{$menu->url}}</td>
                                        <td>@if($menu->parent_id)
                                                {{$menu->parent_id}}
                                            @else 
                                                0
                                            @endif
                                        </td>
                                        <td>
                                            @if($menu->sort)
                                                {{$menu->sort}}
                                            @else 
                                                0
                                            @endif
                                        </td>
                                        <td>
                                            @if($menu->status == 1)
                                                <span class="text-success">Hiển thị</span>
                                            @else
                                                <span class="text-warning">Không hiển thị</span>
                                            @endif
                                        </td>
                                        <td width="150">
                                            <ul style="list-style-type:none; padding: 0px; margin:0">
                                                <li style="float:left; margin-top: 6px">
                                                    <a class="btn btn-white btn-bitbucket" style="padding:6px 8px" href="{{route('menu.show',$menu->id)}}" title="Xem" data-toggle="tooltip" data-placement="top" data-original-title="Xem"><i class="fa fa-search"></i>Xem</a>
                                                </li>
                                                <li style="float:left; margin-top: 6px; margin-left: 2px">
                                                    <a class="btn btn-white btn-bitbucket" style="padding:6px 8px" href="{{route('menu.edit',$menu->id)}}" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i>Sửa</a>
                                                </li>
                                                <li style="float:right; margin-top: 6px">
                                                    <form method="POST" action="{{route('menu.destroy',$menu->id)}}" accept-charset="UTF-8">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-white btnDelete" style="padding:6px 8px" title="Xóa" data-toggle="tooltip"data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i>Xóa</button>
                                                    </form>
                                                </li>
                                             </ul>
                                        </td>
                                    </tr>
                                @endforeach                     
                            </tbody>
                        </table>
                        <div style="float: right">
                            {{ $menus->links() }}       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
</script>
@endsection