@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="ZzvXx740ES7XHFbt57YJBrBETkreIGKpWg4CBhPb">
    <script>
        var csrf_token = 'ZzvXx740ES7XHFbt57YJBrBETkreIGKpWg4CBhPb';
    </script>
@endsection
@section('title','Thêm từ khóa')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Thêm Từ khóa</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('category.index')}}">Danh sách Từ khóa</a></li>
                <li>Thêm Từ khóa</li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Thêm Từ khóa</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="POST" action="{{route('tag.store')}}" accept-charset="UTF-8" class="form-horizontal">
                            <!-- <input name="_token" type="hidden" value="ZzvXx740ES7XHFbt57YJBrBETkreIGKpWg4CBhPb"> -->
                            @csrf
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Từ khóa:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="Từ khóa" autocomplete="off" name="name" type="text" value="{{old('name')}}" id="name">
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="Slug" autocomplete="off" name="slug" type="text" value="{{old('slug')}}" id="slug">
                                    <span class="text-danger">{{ $errors->first('slug') }}</span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Thêm</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
