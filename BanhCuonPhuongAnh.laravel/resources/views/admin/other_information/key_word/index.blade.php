@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="ZzvXx740ES7XHFbt57YJBrBETkreIGKpWg4CBhPb">
    <script>
        var csrf_token = 'ZzvXx740ES7XHFbt57YJBrBETkreIGKpWg4CBhPb';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Danh sách Từ khóa</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li>Danh sách từ khóa</li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{route('tag.create')}}" class="btn btn-primary "><i class="fa fa-plus"></i> Thêm</a>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Từ khóa</th>
                                    <th>Slug</th>
                                    <th width="150"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($key_words as $kw)
                                    <tr>
                                        <td>{{$kw->name}}</td>
                                        <td>{{$kw->slug}}</td>
                                        <td>
                                            <ul style="list-style-type:none; padding: 0px; margin:0">
                                                <li style="float:left; margin-top: 6px">
                                                    <a class="btn btn-white btn-bitbucket" href="{{route('tag.edit',$kw->id)}}" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i>Sửa</a>
                                                </li>
                                                <li style="float:right; margin-top: 6px">
                                                    <form method="POST" action="{{route('tag.destroy',$kw->id)}}" accept-charset="UTF-8">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-white btnDelete" title="Xóa" data-toggle="tooltip"data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i>Xóa</button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div style="float: right">
                            {{ $key_words->links() }}
                        </div>      
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
</script>
@endsection