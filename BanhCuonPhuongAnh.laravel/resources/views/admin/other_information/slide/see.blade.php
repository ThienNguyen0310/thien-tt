@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc">
    <script>
        var csrf_token = '5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{$slide->name}}</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('slide.index')}}">Danh sách Tin tức</a></li>
                <li><a href="{{route('slide.edit',$slide->id)}}">Sửa thông tin</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Thông tin chi tiết</h5>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="150px">#</th>
                                <th>Giá trị</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>ID</td>
                                <td>{{$slide->id}}</td>
                            </tr>
                            <tr>
                                <td>Bài viết</td>
                                <td>{{$slide->name}}</td>
                            </tr>
                            <tr>
                                <td>Mô tả</td>
                                <td>{{$slide->description}}</td>
                            </tr>
                            <tr>
                                <td>Tạo bởi</td>
                                <td>admin vào lúc {{$slide->created_at}}</td>
                            </tr>
                            <tr>
                                <td>Cập nhật bởi</td>
                                <td>admin vào lúc {{$slide->updated_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection