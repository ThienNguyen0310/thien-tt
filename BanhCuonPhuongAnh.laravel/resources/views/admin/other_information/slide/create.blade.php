@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD">
    <script>
        var csrf_token = 'vgyC6rbdRxpxx3ykD9C2Fh0D5fdQCc9vFJtaPcWD';
    </script>
@endsection
@section('title','Thêm Slide')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Thêm Slide</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('slide.index')}}">Danh sách Tin tức</a></li>
                <li>Thêm slide</li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="POST" action="{{route('slide.store')}}" accept-charset="UTF-8" class="form-horizontal">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Slide:</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="Tên Slide" autocomplete="off" name="name" type="text" value="{{old('name')}}" id="title">
                                <span class="text-danger">{{ $errors->first('name')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="url" class="col-sm-2 control-label">Url:</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="Url" autocomplete="off" name="url" type="text" value="{{old('url')}}" id="url">
                                <span class="text-danger">{{ $errors->first('url')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" cols="50" rows="10" id="description">{{old('description')}}</textarea>
                                <span class="text-danger">{{ $errors->first('description')}}</span>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Thêm
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection