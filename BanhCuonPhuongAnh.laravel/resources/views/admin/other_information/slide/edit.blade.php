@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc">
    <script>
        var csrf_token = '5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc';
    </script>
@endsection
@section('title',$slide->name)
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{$slide->name}}</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('slide.index')}}">Danh sách Tin tức</a></li>
                <li><a href="{{route('slide.show',$slide->id)}}">Xem chi tiết</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <form method="POST" action="{{route('slide.update',$slide->id)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="row">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Thông tin chi tiết</a>
                            </li>
                            <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">Ảnh đại diện</a>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 control-label">Slide:</label>

                                            <div class="col-sm-10">
                                                <input class="form-control" placeholder="Tên Slide" name="name" type="text" value="{{$slide->name}}" id="title">
                                                <span class="text-danger">{{ $errors->first('name')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="url" class="col-sm-2 control-label">Url:</label>

                                            <div class="col-sm-10">
                                                <input class="form-control" placeholder="Url" name="url" type="text" value="{{$slide->url}}" id="url">
                                                <span class="text-danger">{{ $errors->first('url')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" name="description" cols="50" rows="10" id="description">{{$slide->description}}</textarea>
                                                <span class="text-danger">{{ $errors->first('description')}}</span>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="slide_id" value="{{$slide->id}}">
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="files" style="width:120px;margin-top: 25px" class="btn btn-primary btn-block btn-outlined">Select Image</label>
                                            <input type="file" style="visibility:hidden;" name="image" class="form-file-control" id ="files" onchange="previewFile();">
                                            <img id = "preview" src="{{Storage::url('images/'.$slide->image)}}">
                                            <span class="text-danger">{{ $errors->first('image') }}</span>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('javascript')
    <script> 
        function previewFile(){ 
            var preview = $('#preview');
            var file = $('#files').prop('files')[0];
            var reader = new FileReader();
            if(file){
                reader.readAsDataURL(file);
            }else{
                preview.attr('src', '');
            }
            reader.onloadend = function() {
                preview.attr('src', reader.result);
            }
        }
    </script>
@endsection