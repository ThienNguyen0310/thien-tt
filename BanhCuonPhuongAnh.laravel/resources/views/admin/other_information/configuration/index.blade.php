@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq">
    <script>
        var csrf_token = 'D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq';
    </script>
@endsection
@section('title','Danh sách cấu hình')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Danh sách Cấu hình</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{route('config.create')}}" class="btn btn-primary "><i class="fa fa-plus"></i> Thêm</a>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Cấu hình</th>
                                    <th>Giá trị</th>
                                    <th>Mô tả</th>
                                    <th width="150"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($configs as $config)
                                    <tr>
                                        <td>{{$config->name}}</td>
                                        <td>{{$config->value}}</td>
                                        <td>{{$config->description}}</td>
                                        <td align="center">
                                            <ul style="list-style-type:none; padding: 0px; margin:0">
                                                <li style="float:left; margin-top: 6px">
                                                    <a class="btn btn-white btn-bitbucket" href="{{route('config.edit',$config->id)}}" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i>Sửa</a>
                                                </li>
                                                <li style="float:right; margin-top: 6px">
                                                    <form method="POST" action="{{route('config.destroy',$config->id)}}" accept-charset="UTF-8">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-white btnDelete" title="Xóa" data-toggle="tooltip"data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i>Xóa</button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div style="float: right">
                            {{ $configs->links() }}
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
</script>
@endsection