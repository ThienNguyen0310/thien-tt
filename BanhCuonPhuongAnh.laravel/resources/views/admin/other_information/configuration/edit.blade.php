@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq">
    <script>
        var csrf_token = 'D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq';
    </script>
@endsection
@section('title',$config->name)
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>$config->name</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('config.index')}}">Danh sách Cấu hình</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Sửa thông tin $config->name</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="POST" action="{{route('config.update',$config->id)}}" accept-charset="UTF-8" class="form-horizontal">
                            <!-- <input name="_method" type="hidden" value="PATCH">
                            <input name="_token" type="hidden" value="D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq"> -->
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Tên:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" rows="2" value="{{$config->name}}" name="name" type="text" id="name">
                                    <span class="text-danger">{{ $errors->first('name')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="value" class="col-sm-2 control-label">Giá trị:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4" name="value" cols="50" id="value">{{$config->value}}</textarea>
                                    <span class="text-danger">{{ $errors->first('value')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="2" name="description" cols="50" id="description">{{$config->description}}</textarea>
                                    <span class="text-danger">{{ $errors->first('description')}}</span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật</button>
                                </div>
                            </div>
                            <input type="hidden" name="config_id" value="{{$config->id}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection