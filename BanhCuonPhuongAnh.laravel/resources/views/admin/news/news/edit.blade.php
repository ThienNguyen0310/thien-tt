@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc">
    <script>
        var csrf_token = '5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc';
    </script>
@endsection
@section('title',$news->name)
@section('link')
    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/cropper/cropper.min.css')}}" rel="stylesheet">
    <style>
        .image-crop {
            background: #000;
            width: 100%;
        }

        .cropper-container {
            left: 0 !important;
            margin: 0 auto !important;
        }

        .img-preview-sm {
            width: 150px !important;
            height: 100px !important;
        }

        .lightBoxGallery {
            text-align: center;
        }

        .lightBoxGallery img {
            margin: 5px;
        }
    </style>
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{$news->name}}</h2>
            <ol class="breadcrumb">
                @if(Auth::user()->level)
                    <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                @else 
                    <li><a href="{{route('post.index')}}">Trang chủ</a></li>
                @endif
                <li><a href="{{route('news.index')}}">Danh sách Tin tức</a></li>
                <li><a href="{{route('news.show',$news->id)}}">Xem chi tiết</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <form method="POST" action="{{route('news.update',$news->id)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                <!-- <input name="_method" type="hidden" value="PATCH">
                <input name="_token" type="hidden" value="5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc"> -->
                @csrf
                @method('PATCH')
                <div class="row">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Thông tin Sản phẩm</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">Ảnh đại diện</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 control-label">Bài viết:</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" placeholder="Tên Bài viết" name="name" type="text" autocomplete="off" value="{{$news->name}}" id="title">
                                                <span class="text-danger">{{ $errors->first('name')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" placeholder="Slug" name="slug" type="text" autocomplete="off" value="{{$news->slug}}" id="slug">
                                                <span class="text-danger">{{ $errors->first('slug')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" name="description" cols="50" rows="10" id="description">{{$news->description}}</textarea>
                                                <span class="text-danger">{{ $errors->first('description')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="content" class="col-sm-2 control-label">Nội dung:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control summernote" name="content" cols="50" rows="10" id="content">{!!$news->content!!}</textarea>
                                                <span class="text-danger">{{ $errors->first('content')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="category_id" class="col-sm-2 control-label">Thể loại:</label>
                                            <div class="col-sm-10">
                                                <select id="news_categories" class="form-control chosen-select" data-placeholder="Chọn Thể loại" name="news_category_id">
                                                    <option value="">Thể loại cha</option>
                                                    @foreach($news_categories as $cat)
                                                        @if($cat->id == $news->news_category_id)
                                                            <option value="{{$cat->id}}" selected="selected">{{$cat->name}}</option>
                                                        @else
                                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="active" class="col-sm-2 control-label">Trạng thái:</label>
                                            <div class="col-sm-10">
                                                <input name="status" type="checkbox" id="active" {{ $news->status == 1 ? 'checked' : ''}}>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật</button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="news_id" value="{{$news->id}}">
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <!-- <div class="panel-body">
                                    <div class="col-md-12">
                                        <div style="margin-bottom: 15px;">
                                            <label title="Upload image file" for="inputImage" class="btn btn-success">
                                                <input type="file" accept="image/*" name="image" id="inputImage" class="hide">
                                                <i class="fa fa-upload"></i> Chọn ảnh
                                            </label>&nbsp;&nbsp;
                                            <label title="Donwload image" id="setDrag" class="btn btn-primary"><i class="fa fa-check"></i> Lưu ảnh</label>
                                        </div>
                                        <div>
                                            <div class="img-preview img-preview-sm pull-left"></div>
                                            <div class="image-crop pull-left" style="width: 450px; margin-left: 5px; height: 400px;">
                                                <img src="{{Storage::url('images/'.$news->image)}}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <label for="files" style="width:120px;margin-top: 25px" class="btn btn-primary btn-block btn-outlined">Select Image</label>
                                    <input type="file" style="visibility:hidden;" name="image" class="form-file-control" id ="files" onchange="previewFile();">
                                    <img id = "preview" src="{{Storage::url('images/'.$news->image)}}">
                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('javascript')
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('js/plugins/cropper/cropper.min.js')}}"></script>
    <script> 
        $(document).ready(function () {
            $('.summernote').summernote({
                height: 400,
                onImageUpload: function (files, editor, welEditable) {
                    console.log(123);
                    sendFile(files[0], editor, welEditable);
                }
            });

            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);

                $.ajax({
                    type: "POST",
                    url: "/news/image-editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });

        function previewFile(){ 
            var preview = $('#preview');
            var file = $('#files').prop('files')[0];
            var reader = new FileReader();
            if(file){
                reader.readAsDataURL(file);
            }else{
                preview.attr('src', '');
            }
            reader.onloadend = function() {
                preview.attr('src', reader.result);
            }
        }
    </script>
    
    <!-- <script type="text/javascript">
        $(document).ready(function () {
            var $image = $(".image-crop > img");
            $($image).cropper({
                aspectRatio: 1.76,
                preview: ".img-preview",
                done: function (data) {
                    // console.log(data);
                }
            }).on('crop.cropper', function (e) {
                console.log(e.type); // cropstart
                console.log(e.namespace); // cropper
                console.log(e.action); // ...
                console.log(e.originalEvent.pageX);

                // Prevent to start cropping, moving, etc if necessary
                if (e.action === 'crop') {
                    e.preventDefault();
                }
            });

            var $inputImage = $("#inputImage");
            if (window.FileReader) {
                $inputImage.change(function () {
                    var fileReader = new FileReader(),
                            files = this.files,
                            file;

                    if (!files.length) {
                        return;
                    }

                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                            $inputImage.val("");
                            $image.cropper("reset", true).cropper("replace", this.result);
                        };
                    } else {
                        showMessage("Please choose an image file.");
                    }
                });
            } else {
                $inputImage.addClass("hide");
            }

            $("#setDrag").click(function () {
                var data = new FormData();
                var id = "3";

                data.append("image", App.dataURItoBlob($image.cropper("getDataURL")));
                data.append("_id", id);

                $.ajax({
                    type: "POST",
                    url: "/news/image-crop-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        App.refresh('http://cms.banhcuonphuonganh.com.vn/news/3/edit');
                    }
                });

            });

        });
    </script> -->
@endsection