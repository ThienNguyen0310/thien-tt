@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc">
    <script>
        var csrf_token = '5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc';
    </script>
@endsection
@section('title','Thêm tin tức')
@section('link')
    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Thêm Tin tức</h2>
            <ol class="breadcrumb">
                @if(Auth::user()->level)
                    <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                @else 
                    <li><a href="{{route('post.index')}}">Trang chủ</a></li>
                @endif
                <li><a href="{{route('news.index')}}">Danh sách Tin tức</a></li>
                <li>Thêm Tin tức</li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="POST" action="{{route('news.store')}}" accept-charset="UTF-8" class="form-horizontal">
                        <!-- <input name="_token" type="hidden" value="5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc"> -->
                        @csrf
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Tin tức:</label>
                            <div class="col-sm-10">
                                <input class="form-control" autocomplete="off" value="{{old('name')}}" placeholder="Tên Tin tức" name="name" type="text" value="" id="title">
                                <span class="text-danger">{{ $errors->first('name')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="slug" class="col-sm-2 control-label">Slug:</label>
                            <div class="col-sm-10">
                                <input class="form-control" autocomplete="off" value="{{old('slug')}}" placeholder="Slug" name="slug" type="text" value="" id="slug">
                                <span class="text-danger">{{ $errors->first('slug')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" cols="50" rows="10" id="description"> {{old('description')}}</textarea>
                                <span class="text-danger">{{ $errors->first('description')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="content" class="col-sm-2 control-label">Nội dung:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control summernote" name="content" cols="50" rows="10" id="content">{{old('content')}}</textarea>
                                <span class="text-danger">{{ $errors->first('content')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="news_category_id" class="col-sm-2 control-label">Thể loại:</label>
                            <div class="col-sm-10">
                                <select class="form-control chosen-select" data-placeholder="Chọn Thể loại" id="category_id" name="news_category_id">
                                    <option value="">Thể loại cha</option>
                                    @foreach($news_categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="active" class="col-sm-2 control-label">Trạng thái:</label>
                            <div class="col-sm-10">
                                <input name="active" type="checkbox" value="1" id="active">
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Thêm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.summernote').summernote({
                onImageUpload: function (files, editor, welEditable) {
                    console.log(123);
                    sendFile(files[0], editor, welEditable);
                }
            });

            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);

                $.ajax({
                    type: "POST",
                    url: "/post/image-editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });
    </script>
@endsection
