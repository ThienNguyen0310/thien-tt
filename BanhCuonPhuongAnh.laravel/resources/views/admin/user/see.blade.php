@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq">
    <script>
        var csrf_token = 'D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq';
    </script>
@endsection
@section('title',$user->name)
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>admin</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('user.index')}}">Danh sách Người dùng</a></li>
                <li><a href="{{route('user.edit',$user->id)}}">Sửa thông tin {{$user->name}}</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Thông tin chi tiết</h5>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="150px">#</th>
                                <th>Giá trị</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>ID</td>
                                <td>{{$user->id}}</td>
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td>{{$user->username}}</td>
                            </tr>
                            <tr>
                                <td>Tên hiển thị</td>
                                <td>{{$user->name}}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <td>Điện thoại</td>
                                <td>{{$user->phone_number}}</td>
                            </tr>
                            <tr>
                                <td>Địa chỉ</td>
                                <td>{{$user->address}}</td>
                            </tr>
                            <tr>
                                <td>Vai trò</td>
                                <td>
                                    @if($user->level)
                                        Admin
                                    @else
                                        Member
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Ngày tạo</td>
                                <td>{{$user->created_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection