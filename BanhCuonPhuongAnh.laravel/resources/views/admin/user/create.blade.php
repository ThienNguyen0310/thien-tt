@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq">
    <script>
        var csrf_token = 'D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq';
    </script>
@endsection
@section('title','')
@section('link')
    <link href="{{asset('css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Thêm Người dùng</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('user.index')}}">Danh sách Người dùng</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <form method="POST" action="{{route('user.store')}}" accept-charset="UTF-8" class="form-horizontal">
            <!-- <input name="_token" type="hidden" value="D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq"> -->
            @csrf
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="form-group">
                            <label for="username" class="col-sm-2 control-label">Tài khoản:</label>
                            <div class="col-sm-10">
                                <input class="form-control" autocomplete="off" value="{{old('username')}}" placeholder="Tài khoản" name="username" type="text" id="name">
                                <span class="text-danger">{{ $errors->first('username')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" autocomplete="off" value="{{old('email')}}" placeholder="Email" class="form-control"/>
                                <span class="text-danger">{{ $errors->first('email')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Mật khẩu:</label>
                            <div class="col-sm-10">
                                <input type="password" name='password' autocomplete="off" value="{{old('password')}}" placeholder="Mật khẩu" class="form-control">
                                <span class="text-danger">{{ $errors->first('password')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation" class="col-sm-2 control-label">Nhập lại mật khẩu:</label>
                            <div class="col-sm-10">
                                <input type="password" name='password_confirmation' autocomplete="off" value="{{old('password')}}"  placeholder="Nhập lại mật khẩu" class="form-control">
                                <span class="text-danger">{{ $errors->first('password_confirmation')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Tên hiển thị:</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="Tên hiển thị" autocomplete="off" value="{{old('name')}}" name="name" type="text" id="fullname">
                                <span class="text-danger">{{ $errors->first('name')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone_number" class="col-sm-2 control-label">Điện thoại:</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="Điện thoại" autocomplete="off" value="{{old('phone_number')}}" name="phone_number" type="text" id="mobile">
                                <span class="text-danger">{{ $errors->first('phone_number')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Điạ chỉ:</label>
                            <div class="col-sm-10">
                                <input class="form-control" placeholder="Điạ chỉ" autocomplete="off" value="{{old('address')}}" name="address" type="text" id="address">
                                <span class="text-danger">{{ $errors->first('address')}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="level" class="col-sm-2 control-label">Vai trò:</label>
                            <div class="col-sm-10">
                                <select value="{{old('level')}}" class="form-control chosen-select" data-placeholder="Chọn level"  name="level">
                                    <option value="1">Admin</option>
                                    <option value="0">Member</option>
                                </select>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label for="active" class="col-sm-2 control-label">Trạng thái:</label>
                            <div class="col-sm-10">
                                <input name="active" type="checkbox" autocomplete="off" value="{{old('name')}}" id="active">
                            </div>
                        </div> -->

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Thêm</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
@section('javascript')
    <script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
@endsection