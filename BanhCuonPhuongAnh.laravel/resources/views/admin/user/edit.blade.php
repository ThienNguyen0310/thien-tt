@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq">
    <script>
        var csrf_token = 'D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq';
    </script>
@endsection
@section('title',$user->name)
@section('link')
    <link href="{{asset('css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>
                @if($user->level)
                    Admin
                @else
                    Member
                @endif
            </h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('user.index')}}">Danh sách Người dùng</a></li>
                <li><a href="{{route('user.show',$user->id)}}">{{$user->name}}</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <form method="POST" action="{{route('user.update',$user->id)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
            <!-- <input name="_method" type="hidden" value="PATCH">
            <input name="_token" type="hidden" value="D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq"> -->
            @csrf
            @method('PATCH')
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Sửa Thông tin {{$user->name}}</h5>
                </div>
                <div class="ibox-content">

                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Tài khoản:</label>
                        <div class="col-sm-10">
                            <input class="form-control"  name="username" type="text" value="{{$user->username}}" id="name">
                            <span class="text-danger">{{ $errors->first('username')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email:</label>
                        <div class="col-sm-10">
                            <input class="form-control" name="email" type="text" value="{{$user->email}}" id="email">
                            <span class="text-danger">{{ $errors->first('email')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Mật khẩu:</label>
                            <div class="col-sm-10">
                                <input type="password" name='password' value="{{$user->password}}" placeholder="Mật khẩu" class="form-control">
                                <span class="text-danger">{{ $errors->first('password')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation" class="col-sm-2 control-label">Nhập lại mật khẩu:</label>
                            <div class="col-sm-10">
                                <input type="password" name='password_confirmation' value="{{$user->password}}"  placeholder="Nhập lại mật khẩu" class="form-control">
                                <span class="text-danger">{{ $errors->first('password_confirmation')}}</span>
                            </div>
                        </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Tên hiển thị:</label>

                        <div class="col-sm-10">
                            <input class="form-control" name="name" type="text" value="{{$user->name}}" id="fullname">
                            <span class="text-danger">{{ $errors->first('name')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone_number" class="col-sm-2 control-label">Điện thoại:</label>
                        <div class="col-sm-10">
                            <input class="form-control" name="phone_number" type="text" value="{{$user->phone_number}}" id="mobile">
                            <span class="text-danger">{{ $errors->first('phone_number')}}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">Điạ chỉ:</label>
                        <div class="col-sm-10">
                            <input class="form-control" name="address" type="text" value="{{$user->address}}" id="address">
                            <span class="text-danger">{{ $errors->first('address')}}</span>
                        </div>
                    </div>
                    <div class="form-group">
                            <label for="level" class="col-sm-2 control-label">Vai trò:</label>
                            <div class="col-sm-10">
                                <select  class="form-control chosen-select" data-placeholder="Chọn level"  name="level">
                                    @if($user->level)
                                        <option selected="selected" value="{{$user->level}}">Admin</option>
                                        <option value="">Member</option>
                                    @else 
                                        <option selected="selected" value="{{$user->level}}">Member</option>
                                        <option value="">Admin</option>
                                    @endif
                                </select>
                            </div>
                        </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật</button>
                        </div>
                    </div>
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                </div>
            </div>
        </div>

        </form>
    </div>

@endsection
@section('javascript')
    <script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
@endsection