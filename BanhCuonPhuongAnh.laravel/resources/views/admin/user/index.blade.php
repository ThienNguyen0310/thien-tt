@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq">
    <script>
        var csrf_token = 'D7GhlWGsxn3FvIBna1eaSh6iOl1SlEhYg5v58hCq';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Danh sách Quản trị viên</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li>Danh sách Quản trị viên</li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{route('user.create')}}" class="btn btn-primary">Thêm</a>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Tài khoản</th>
                                    <th>Email</th>
                                    <th>Vai trò</th>
                                    <th>Ngày tạo</th>
                                    <th width="200"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            <h4></h4>
                                            <p>{{$user->username}}</p>
                                        </td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                            @if($user->level)
                                                Admin
                                            @else
                                                Member
                                            @endif
                                        </td>
                                        <td>{{$user->created_at}}</td>
                                        <td width="150">
                                            <ul style="list-style-type:none; padding: 0px; margin:0">
                                                    <li style="float:left; margin-top: 6px">
                                                        <a class="btn btn-white btn-bitbucket" style="padding:6px 8px" href="{{route('user.show',$user->id)}}" title="Xem" data-toggle="tooltip" data-placement="top" data-original-title="Xem"><i class="fa fa-search"></i>Xem</a>
                                                    </li>
                                                    <li style="float:left; margin-top: 6px; margin-left: 2px">
                                                        <a class="btn btn-white btn-bitbucket" style="padding:6px 8px" href="{{route('user.edit',$user->id)}}" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i>Sửa</a>
                                                    </li>
                                                    <li style="float:right; margin-top: 6px">
                                                        <form method="POST" action="{{route('user.destroy',$user->id)}}" accept-charset="UTF-8">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-white btnDelete" style="padding:6px 8px" title="Xóa" data-toggle="tooltip"data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i>Xóa</button>
                                                        </form>
                                                    </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div style="float: right">
                            {{ $users->links() }}       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
    <script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
</script>
@endsection