@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="6QPUtq9xy8PQzRCTYJFBr3t7UufEuAk2lX98N111">
    <script>
        var csrf_token = '6QPUtq9xy8PQzRCTYJFBr3t7UufEuAk2lX98N111';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Danh sách Sản phẩm hot</h2>
            <ol class="breadcrumb">
                <li><a href="route('product.index')">Trang chủ</a></li>
            </ol>
        </div>
    </div>

    
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <table id='_list' class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Hình ảnh</th>
                                    <th>Tiêu đề</th>
                                    <th>Thể loại</th>
                                    <th>Trạng thái</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                 @foreach($hot_products as $hot_product)                                   
                                    <tr>
                                        <td>{{$hot_product->product->id}}</td>
                                        <td width="150">
                                            <img src="{{Storage::url('images/'.$hot_product->product->image)}}" alt="" width="150px" height="100px">
                                        </td>
                                        <td>
                                            <a target="_blank" href="{{'http://banhcuonphuonganh.com/'.$hot_product->product->slug.','.$hot_product->product->hashids}}">
                                                <h4>{{$hot_product->product->name}}</h4>
                                            </a>
                                            <p>{{$hot_product->product->created_at}}</p>
                                        </td>
                                        <td>
                                            @if($hot_product->product->product_category)
                                                {{$hot_product->product->product_category->name}}
                                            @else
                                                
                                            @endif
                                        </td>
                                        <td>
                                            @if($hot_product->product->status == 1)
                                                <span class="text-success">Xuất bản</span>
                                            @else
                                                <span class="text-warning">Chưa xuất bản</span>
                                            @endif
                                        </td>
                                        <td width="150">
                                            <form method="POST" action="{{route('hot.destroy',$hot_product->product->id)}}" accept-charset="UTF-8">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-white btnDelete" style="padding:6px 8px" title="Xóa" data-toggle="tooltip"data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix">
                            <div class="pull-right"></div>
                        </div>
                        <div style="float: right">
                            {{ $hot_products->links() }}       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
    <script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
</script>
@endsection