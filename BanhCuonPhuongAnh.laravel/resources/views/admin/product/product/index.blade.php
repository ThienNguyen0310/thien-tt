@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="6QPUtq9xy8PQzRCTYJFBr3t7UufEuAk2lX98N111">
    <script>
        var csrf_token = '6QPUtq9xy8PQzRCTYJFBr3t7UufEuAk2lX98N111';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Danh sách Sản phẩm</h2>
            <ol class="breadcrumb">
                <li><a href="route('product.index')">Trang chủ</a></li>
            </ol>
        </div>
    </div>

    
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <form method="GET" action="{{route('product.index')}}" accept-charset="UTF-8" id="search-frm" class="form-horizontal">
                                <div class="col-lg-4">
                                    <div class="btn-group">
                                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">Hành động <span
                                                    class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{route('selective.store')}}" class="_check_add_to"
                                                   data-target="_list"><i class="fa fa-plus"></i> Chọn lọc</a></li>
                                            <li><a href="{{route('hot.store')}}" class="_check_add_to" data-target="_list"><i class="fa fa-plus"></i> Hot</a></li>
                                        </ul>
                                    </div>

                                    <a href="{{route('product.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm</a>
                                </div>
                                <div class="col-lg-2" style="padding-right: 0;">
                                    <select class="form-control chosen-select" data-placeholder="Chọn Trạng thái" onchange="$('#search-frm').submit();" name="status">
                                        @if($status)
                                            <option value="" >Chọn Trạng thái</option>
                                            <option value="1" selected="selected">Xuất bản</option>
                                            <option value="0">Chưa xuất bản</option>
                                        @elseif($status == 0)
                                            <option value="" >Chọn Trạng thái</option>
                                            <option value="1">Xuất bản</option>
                                            <option value="0" selected="selected">Chưa xuất bản</option>
                                        @else
                                            <option value="" selected="selected">Chọn Trạng thái</option>
                                            <option value="1">Xuất bản</option>
                                            <option value="0" >Chưa xuất bản</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-lg-3" style="padding-right: 0;">
                                    <select class="form-control chosen-select" data-placeholder="Chọn Thể loại" 
                                    onchange="$('#search-frm').submit();" name="category_id">
                                        <option value="0" selected="selected">Chọn Thể loại</option>
                                        @foreach($product_categories as $cat)
                                            @if($cat->id == $category_id)
                                                <option value="{{$cat->id}}" selected="selected">{{$cat->name}}</option>
                                            @else 
                                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="Tìm kiếm" autocomplete="off" name="keyword" type="text" value="{{ $id ? $id : ''}}">

                                        <input type="hidden" name="action" id="search-action" value="id"/>

                                        <div class="input-group-btn">
                                            <button data-toggle="dropdown" class="btn btn-white dropdown-toggle"
                                                    type="button" aria-expanded="false"><i class="fa fa-search"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="#" class="action" 
                                                       onclick='$("#search-action").val("id");$("#search-frm").submit();'>Tìm
                                                        theo ID</a></li>
                                                <li><a href="#"
                                                       onclick='$("#search-action").val("eid");$("#search-frm").submit();'>Tìm
                                                        theo ID Mã hóa</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li><a href="#"
                                                       onclick='$("#search-action").val("title");$("#search-frm").submit();'>Tìm
                                                        theo Tiêu đề</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <table id='_list' class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" value="" class="check_all"></th>
                                    <th>ID</th>
                                    <th>Hình ảnh</th>
                                    <th>Tiêu đề</th>
                                    <th>Thể loại</th>
                                    <th>Trạng thái</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                 @foreach($products as $pro)                                   
                                    <tr>
                                        <td><input type="checkbox" value="{{$pro->id}}" name = check_item[] class="check_item"></td>
                                        <td>{{$pro->id}}</td>
                                        <td width="150">
                                            <img src="{{Storage::url('images/'.$pro->image)}}" alt="" width="150px" height="100px">
                                        </td>
                                        <td>
                                            <a target="_blank" href="{{'http://banhcuonphuonganh.com/'.$pro->slug.','.$pro->hashids}}">
                                                <h4>{{$pro->name}}</h4>
                                            </a>
                                            <p>{{$pro->created_at}}</p>
                                        </td>
                                        <td>
                                            @if($pro->product_category)
                                                {{$pro->product_category->name}}
                                            @else
                                                
                                            @endif
                                        </td>
                                        <td>
                                            @if($pro->status == 1)
                                                <span class="text-success">Xuất bản</span>
                                            @else
                                                <span class="text-warning">Chưa xuất bản</span>
                                            @endif
                                        </td>
                                        <td width="150">
                                            <ul style="list-style-type:none; padding: 0px; margin:0">
                                                <li style="float:left; margin-top: 6px">
                                                    <a class="btn btn-white btn-bitbucket" style="padding:6px 8px" href="{{route('product.show',$pro->id)}}" title="Xem" data-toggle="tooltip" data-placement="top" data-original-title="Xem"><i class="fa fa-search"></i></a>
                                                </li>
                                                <li style="float:left; margin-top: 6px; margin-left: 16px">
                                                    <a class="btn btn-white btn-bitbucket" style="padding:6px 8px" href="{{route('product.edit',$pro->id)}}" title="Sửa" data-toggle="tooltip" data-placement="top" data-original-title="Sửa"><i class="fa fa-pencil"></i></a>
                                                </li>
                                                <li style="float:right; margin-top: 6px">
                                                    <form method="POST" action="{{route('product.destroy',$pro->id)}}" accept-charset="UTF-8">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-white btnDelete" style="padding:6px 8px" title="Xóa" data-toggle="tooltip"data-placement="top" data-original-title="Xóa"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix">
                            <div class="pull-right"></div>
                        </div>
                        <!-- -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
    <script type="text/javascript">
        $(".btnDelete").click(function (){
          var isDelete = confirm('Are you sure want to delete ?');
          if(isDelete){
            return true;
          }else{
            return false;
          }
        });
    

        $('body').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('table').on('click', ".check_all", function () {
            var checked = $('.check_all').is(':checked');
            $(this).closest('table').find('.check_item').each(function () {
                if ($(this).is(':checked') != checked)
                    $(this).click();
            });
        });

        $('._check_add_to').click(function () {
            var _url = $(this).attr('href'), _target = $('#' + $(this).data('target')), _items = new Array();
            var link =
            _target.find("input[type=checkbox]:checked").each(function () {
                if ('' != $(this).val())
                    _items.push(parseInt($(this).val()));
            });

            App.ajax({
                url: _url,
                type: 'post',
                data: {
                    ids: _items,
                    '_token': '{!! csrf_token() !!}',
                },
                success: function (data) {
                    window.location.href = data
                }
            });

            return false;
        });
</script>
@endsection