@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc">
    <script>
        var csrf_token = '5loXHGehGmtCsWmIakTOsoidgy8QHc3YEGk62cAc';
    </script>
@endsection
@section('title',$product->name)
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{$product->name}}</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('product.index')}}">Danh sách sản phẩm</a></li>
                <li><a href="{{route('product.edit',$product->id)}}">Sửa thông tin</a></li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Thông tin chi tiết</h5>
                </div>
                <div class="ibox-content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="150px">#</th>
                                <th>Giá trị</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="150px">ID</td>
                                <td>{{$product->id}}</td>
                            </tr>
                            <tr>
                                <td>Hình ảnh</td>
                                <td><img src="{{Storage::url('images/'.$product->image)}}" alt="" height="64px"/></td>
                            </tr>
                            <tr>
                                <td>Sản phẩm</td>
                                <td><a href="{{'http://banhcuonphuonganh.com/'.$product->slug.','.$product->hashids}}" target="_blank">{{$product->name}}</a></td>
                            </tr>
                            <tr>
                                <td>Thể loại cha</td>
                                @if($product->product_category)
                                    <td>{{$product->product_category->name}}</td>
                                @else
                                    Null
                                @endif
                            </tr>
                            <tr>
                                <td>Mô tả</td>
                                <td>{{$product->description}}</td>
                            </tr>
                            <tr>
                                <td>Nội dung</td>
                                <td>{!!$product->content!!}</td>
                            </tr>
                            <tr>
                                <td>Gía bán</td>
                                <td>{{$product->price}}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection