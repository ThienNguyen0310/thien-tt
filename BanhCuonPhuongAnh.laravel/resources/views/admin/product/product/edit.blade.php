@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="F1hgy4TW6skx6OCMuSH5qaHpBMYWMhuFgtWl4HLs">
@endsection
@section('title',$product->name)
@section('link')
    <link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/cropper/cropper.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">

    <style>
        .image-crop {
            background: #000;
            width: 100%;
        }

        .image-crop img {
            max-width: 100%;
        }

        .cropper-container {
            left: 0 !important;
            margin: 0 auto !important;
        }

        .lightBoxGallery {
            text-align: center;
        }

        .lightBoxGallery img {
            margin: 5px;
        }
    </style>
@endsection
@section('content')
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>Sửa Sản phẩm</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                        <li><a href="{{route('product.index')}}">Danh sách Sản phẩm</a></li>
                        <li><a href="{{route('product.show',$product->id)}}">{{$product->name}}</a></li>
                    </ol>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <form method="POST" action="{{route('product.update',$product->id)}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                <!-- <input name="_method" type="hidden" value="PATCH">
                <input name="_token" type="hidden" value="F1hgy4TW6skx6OCMuSH5qaHpBMYWMhuFgtWl4HLs"> -->
                @csrf
                @method('PATCH')
                <div class="col-lg-12">
                    <div class="row">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Thông tin Sản phẩm</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">Ảnh đại diện</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">Thư viện ảnh</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false">SEO</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="col-lg-12">

                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-2 control-label">Tên Sản phẩm:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="name" autocomplete="off" type="text" value="{{$product->name}}" id="name">
                                                    <span class="text-danger">{{ $errors->first('name')}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="slug" autocomplete="off" type="text" value="{{$product->slug}}" id="slug">
                                                    <span class="text-danger">{{ $errors->first('slug')}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="product_category_id" class="col-sm-2 control-label">Thể loại:</label>
                                                <div class="col-sm-10">
                                                    <select id="categories" class="form-control chosen-select" data-placeholder="Chọn Thể loại" name="product_category_id">
                                                        <option value="">Thể loại cha</option>
                                                        @foreach($product_categories as $cat)
                                                            @if($cat->id == $product->product_category_id)
                                                                <option value="{{$cat->id}}" selected="selected">{{$cat->name}}</option>
                                                            @else
                                                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" rows="2" name="description" cols="50" id="description">{{$product->description}}</textarea>
                                                    <span class="text-danger">{{ $errors->first('description')}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="content" class="col-sm-2 control-label">Nội dung:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control summernote" name="content" cols="50" rows="10" id="content">{!!$product->content!!}</textarea>
                                                    <span class="text-danger">{{ $errors->first('content')}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="price" class="col-sm-2 control-label">Giá bán:</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <input class="form-control" autocomplete="off" name="price" type="text" value="{{$product->price}}">
                                                        <span class="input-group-addon">VNĐ</span>
                                                        <span class="text-danger">{{ $errors->first('price')}}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="key_words[]" class="col-sm-2 control-label">Từ khóa:</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control chosen-select" multiple="true" name="key_words[]" id="tags[]">
                                                        
                                                        @foreach($key_words as $key_word)
                                                            {{$k = 0 }}
                                                            @foreach($product->key_words as $p_key)
                                                                @if( $p_key->id == $key_word->id)
                                                                    {{ $k = 1 }}
                                                                    @break
                                                                @endif
                                                            @endforeach
                                                            @if( $k == 1)
                                                                    <option value="{{$key_word->id}}" selected>{{$key_word->name}}</option>
                                                            @else
                                                                 <option value="{{$key_word->id}}">{{$key_word->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label for="status" class="col-sm-2 control-label">Trạng thái:</label>
                                                <div class="col-sm-10">
                                                    <input name="status" type="checkbox" value="0" id="status" {{ $product->status == 1 ? 'checked' : ''}}>
                                                </div>
                                            </div>

                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-10">
                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    <div class="form-group">
                                    <label for="files" style="width:120px;margin-top: 25px" class="btn btn-primary btn-block btn-outlined">Select Image</label>
                                    <input type="file" style="visibility:hidden;" name="image" class="form-file-control" id ="files" onchange="previewFile();">
                                    <img id = "preview" src="{{Storage::url('images/'.$product->image)}}">
                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                </div>
                                </div>
                                <div id="tab-3" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <div style="text-align: right; margin-bottom: 10px;">
                                                <a class="btn btn-white btn-bitbucket" href="{{route('gallery.edit',$product->id)}}" title="Đăng ảnh" data-toggle="tooltip" data-placement="top" data-original-title="Đăng ảnh"><i class="fa fa-upload"></i> Đăng ảnh
                                                </a>
                                            </div>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Hình ảnh</th>
                                                        <th style="width: 100px;">Hành động</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($product->products_photos as $product_photo)
                                                        <tr id="{{$product_photo->id}}">
                                                            <td>
                                                                <a href="{{Storage::url('images/'.$product_photo->filename)}}"
                                                                   data-gallery="">
                                                                    <img src="{{Storage::url('images/'.$product_photo->filename)}}"
                                                                         alt='{{$product->name}}' width="120px"/>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a  onclick="deleteImage(this)" class="btn btn-white btn-sm m-l-3"  data-id="{{$product_photo->id}}"
                                                                id="btnDelete">
                                                                <i class="fa fa-trash"></i>
                                                                    Xóa
                                                                </a>
                                                            </td> 
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-4" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="meta_title" class="col-sm-2 control-label">Meta title:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="meta_title" type="text" autocomplete="off" value="{{$product->meta_title}}" id="meta_title">
                                                    <span class="text-danger">{{ $errors->first('meta_title')}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="meta_description" class="col-sm-2 control-label">Meta description:</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" rows="2" name="meta_description" cols="50" id="meta_description">{{$product->meta_description}}</textarea>
                                                    <span class="text-danger">{{ $errors->first('description')}}</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="meta_keyword" class="col-sm-2 control-label">Meta keyword:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="meta_keyword" type="text" autocomplete="off"
                                                    value="{{$product->meta_keyword}}" id="meta_keywords">
                                                    <span class="text-danger">{{ $errors->first('meta_keyword')}}</span>
                                                </div>
                                            </div>

                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-10">
                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật</button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="product_id" id="p_id" value="{{$product->id}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

@endsection
@section('javascript')
    <script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>
    <script src="{{asset('js/plugins/cropper/cropper.min.js')}}"></script>
    <script src="{{asset('js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.chosen-select').chosen({});

            $('.input-group.date').datepicker({
                format: 'yyyy-mm-dd',
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });
        });
        function deleteImage(This) {
                var id =This.dataset.id;
                 $.ajax({
                    url: "{{url('/')}}" + '/deleteRecord/'+id,
                    type: 'DELETE',
                    data : {'_token': '{!! csrf_token() !!}'},
                    async: false,
                    success:function(result)
                    {  
                        $("tr#"+id).remove();
                    }
                });
              

            }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.summernote').summernote({
                height: 400,
                toolbar: [
                    ['color', ['color']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture']],
                    ['view', ['fullscreen', 'codeview']],
                    ['help', ['help']]
                ],
                onImageUpload: function (files, editor, welEditable) {
                    console.log(123);
                    sendFile(files[0], editor, welEditable);
                }
            });
            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);

                $.ajax({
                    type: "POST",
                    url: "/product/image-editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });
        
        function previewFile(){ 
            var preview = $('#preview');
            var file = $('#files').prop('files')[0];
            var reader = new FileReader();
            if(file){
                reader.readAsDataURL(file);
            }else{
                preview.attr('src', '');
            }
            reader.onloadend = function() {
                preview.attr('src', reader.result);
            }
        }
       
    </script>
@endsection