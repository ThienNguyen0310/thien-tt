@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="O7cUgAccd7PxAGWbjTQXJaf6CqF3IzzenelSyomI">
    <script>
        var csrf_token = 'O7cUgAccd7PxAGWbjTQXJaf6CqF3IzzenelSyomI';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('link')
    <link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/chosen/chosen.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Thêm Sản phẩm</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('product.index')}}">Danh sách Sản phẩm</a></li>
                <li>Thêm Sản phẩm</li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="POST" action="{{route('product.store')}}" accept-charset="UTF-8" class="form-horizontal">
                        <!-- input name="_token" type="hidden" value="TjPQHUlSMFGDmIxalQhNXpRFZC6X4ddMSLajjsKe"> -->
                        @csrf
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Tên Sản phẩm:</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="name" autocomplete="off" type="text" value="{{old('name')}}" id="name">
                                <span class="text-danger">{{ $errors->first('name')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="slug" class="col-sm-2 control-label">Slug:</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="slug" autocomplete="off" type="text" value="{{old('slug')}}" id="slug">
                                <span class="text-danger">{{ $errors->first('slug')}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="product_category_id" class="col-sm-2 control-label">Thể loại:</label>
                            <div class="col-sm-10">
                                <select class="form-control chosen-select" data-placeholder="Chọn Thể loại" id="category_id" name="product_category_id">
                                    <option value="">Chọn thể loại cha</option>
                                    @foreach($product_categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                                                                                                   
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="2" name="description" cols="50" id="description">{{old('meta_title')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="content" class="col-sm-2 control-label">Nội dung:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control summernote" name="content" cols="50" rows="10" id="content">{{old('content')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="key_words[]" class="col-sm-2 control-label">Từ khóa:</label>
                            <div class="col-sm-10">
                                <select class="form-control chosen-select" multiple="true" name="key_words[]" id="tags[]">
                                    @foreach($key_words as $key_word)
                                        <option value="{{$key_word->id}}">{{$key_word->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="rents" class="col-sm-2 control-label">Gía bán:</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input class="form-control" name="price" autocomplete="off" type="text" value="{{old('price')}}">
                                    <span class="input-group-addon">VNĐ</span>
                                </div>
                                <span class="text-danger">{{ $errors->first('price')}}</span>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="meta_title" class="col-sm-2 control-label">Meta title:</label>
                            <div class="col-sm-10">
                                <input class="form-control"  name="meta_title" autocomplete="off" type="text" value="{{old('meta_title')}}" id="meta_title">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="meta_description" class="col-sm-2 control-label">Meta description:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="2" name="meta_description" cols="50" id="meta_description">{{old('meta_description')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="meta_keyword" class="col-sm-2 control-label">Meta keyword:</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="meta_keyword" type="text" autocomplete="off" value="{{old('meta_keywords')}}" id="meta_keywords">
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label for="status" class="col-sm-2 control-label">Trạng thái:</label>
                            <div class="col-sm-10">
                                <input name="status" type="checkbox" id="active" />
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Thêm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('javascript')
    <script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.chosen-select').chosen({});

            $('.input-group.date').datepicker({
                format: 'yyyy-mm-dd',
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });

            $('.summernote').summernote({
                height: 400,
                onImageUpload: function (files, editor, welEditable) {
                    console.log(123);
                    sendFile(files[0], editor, welEditable);
                }
            });

            function sendFile(file, editor, welEditable) {
                var data = new FormData();

                data.append("file", file);

                $.ajax({
                    type: "POST",
                    url: "/product/image-editor-upload",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr, token) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                    },
                    success: function (url) {
                        editor.insertImage(welEditable, url);
                    }
                });
            }
        });
    </script>
@endsection