@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="TjPQHUlSMFGDmIxalQhNXpRFZC6X4ddMSLajjsKe">
    <script>
        var csrf_token = 'TjPQHUlSMFGDmIxalQhNXpRFZC6X4ddMSLajjsKe';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Thêm Thể loại</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('category.index')}}">Danh sách Thể loại</a></li>
                <li>Thêm Thể loại</li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Thêm Thể loại</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="POST" action="{{route('category.store')}}" accept-charset="UTF-8" class="form-horizontal">
                            @csrf
                            <!-- <input name="_token" type="hidden" value="TjPQHUlSMFGDmIxalQhNXpRFZC6X4ddMSLajjsKe"> -->
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Thể loại:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" autocomplete="off" placeholder="Nhập Thể loại" name="name" type="text" id="name" value="{{old('name')}}">
                                    <span class="text-danger">{{ $errors->first('name')}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" autocomplete="off" placeholder="Nhập Slug" name="slug" value="{{old('slug')}}" type="text" id="slug">
                                    <span class="text-danger">{{ $errors->first('slug') }}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" autocomplete="off" placeholder="Nhập mô tả" name="description" cols="50" rows="10" id="description">{{old('description')}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="parent_id" class="col-sm-2 control-label">Thể loại cha:</label>
                                <div class="col-sm-10">
                                    <select class="form-control chosen-select" value="{{old('parent_id')}}" data-placeholder="Chọn Thể loại cha" id="parent_id" name="parent_id">
                                        <option value="">Thể loại cha</option>
                                        @foreach($product_categories as $categories)
                                            <option value="{{$categories->id}}">{{$categories->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="meta_title" class="col-sm-2 control-label">Meta title:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" autocomplete="off" value="{{old('meta_title')}}" name="meta_title" type="text" id="meta_title">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="meta_description" class="col-sm-2 control-label">Meta description:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" autocomplete="off" rows="2" name="meta_description" cols="50" id="meta_description">{{old('meta_description')}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="meta_keywords" class="col-sm-2 control-label">Meta keyword:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" autocomplete="off" value="{{old('meta_keyword')}}" name="meta_keyword" type="text" id="meta_keywords">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> Thêm</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection