@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="TjPQHUlSMFGDmIxalQhNXpRFZC6X4ddMSLajjsKe">
    <script>
        var csrf_token = 'yElcQk6pcPhyXnrUuy3PnEYoIazBY6tpr5uwgliE';
    </script>
@endsection
@section('title',$product_category->name)
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>{{$product_category->name}}</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                <li><a href="{{route('category.index')}}">Danh sách Thể loại</a></li>
                <li>{{$product_category->name}}</li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <form method="POST" action="{{route('category.update',$product_category->id)}}" accept-charset="UTF-8" class="form-horizontal">
            <!-- <input name="_method" type="hidden" value="PATCH"> -->
            <!-- <input name="_token" type="hidden" value="TjPQHUlSMFGDmIxalQhNXpRFZC6X4ddMSLajjsKe"> -->
            @csrf
            @method('PATCH')
            <div class="col-lg-12">
                <div class="row">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true">Thể loại</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">SEO</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-2 control-label">Thể loại:</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" autocomplete="off" name="name" type="text" value="{{$product_category->name}}" id="name">
                                                <span class="text-danger">{{ $errors->first('name')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="slug" autocomplete="off" type="text" value="{{$product_category->slug}}" id="slug">
                                                <span class="text-danger">{{ $errors->first('slug')}}</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="description" class="col-sm-2 control-label">Mô tả:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" autocomplete="off" name="description" cols="50" rows="10" id="description">{{$product_category->description}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="parent_id" class="col-sm-2 control-label">Thể loại cha:</label>
                                            <div class="col-sm-10">
                                                <select class="form-control chosen-select" data-placeholder="Chọn Thể loại cha" id="parent_id" name="parent_id">
                                                    <option value="">Chọn Thể loại cha</option>
                                                    @foreach($product_categories as $cat)
                                                        @if($cat->id == $product_category->parent_id)
                                                            <option value="{{$cat->id}}" selected="selected">{{$cat->name}}</option>
                                                        @elseif($cat->name != $product_category->name)
                                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="meta_title" class="col-sm-2 control-label">Meta title:</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="meta_title" type="text" value="{{$product_category->meta_title}}" id="meta_title">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="meta_description" class="col-sm-2 control-label">Meta description:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" rows="2"  name="meta_description" cols="50" id="meta_description">{{$product_category->meta_description}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="meta_keywords" class="col-sm-2 control-label">Meta keyword:</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="meta_keyword" type="text" value="{{$product_category->meta_keyword}}" id="meta_keywords">
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>

                                        <div class="form-group">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-10">
                                                <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Cập nhật</button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="product_category_id" value="{{$product_category->id}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


@endsection