@extends('admin.layouts.master')
@section('meta_javascript')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="F1hgy4TW6skx6OCMuSH5qaHpBMYWMhuFgtWl4HLs">
    <script>
        var csrf_token = 'F1hgy4TW6skx6OCMuSH5qaHpBMYWMhuFgtWl4HLs';
    </script>
@endsection
@section('title','banhcuonphuonganh.com')
@section('link')
    <link href="{{asset('css/plugins/dropzone/dropzone.css')}}" rel="stylesheet">
    <style>
        .lightBoxGallery {
            text-align: center;
        }

        .lightBoxGallery img {
            margin: 5px;
        }
    </style>
@endsection
@section('content')
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>Thư viện ảnh</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{route('product.index')}}">Trang chủ</a></li>
                        <li><a href="{{route('product.index')}}">Danh sách Sản phẩm</a></li>
                        <li><a href="{{route('product.edit',$product->id)}}">{{$product->name}}</a></li>
                    </ol>
            </div>
        </div>

        <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Đăng ảnh</h5>

                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <form method="POST" action="{{route('gallery.update',$product->id)}}" accept-charset="UTF-8" class="dropzone" enctype="multipart/form-data" id="my-awesome-dropzone">
                            @csrf
                            @method('PATCH')
                            <div class="dropzone-previews"></div>

                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-upload"></i> Đăng ảnh
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
@endsection
@section('javascript')
    <script src="{{asset('js/plugins/dropzone/dropzone.js')}}"></script>
    <script>
        $(document).ready(function () {
            Dropzone.options.myAwesomeDropzone = {

                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,

                // Dropzone settings
                init: function () {
                    var myDropzone = this;

                    this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        myDropzone.processQueue();
                    });
                    this.on("sendingmultiple", function () {
                    });
                    this.on("successmultiple", function (files, response) {
                    });
                    this.on("errormultiple", function (files, response) {
                    });
                }
            }
        });
    </script>
@endsection