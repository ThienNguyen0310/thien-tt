<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> 
                                <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->name}}</strong></span> 
                                <span class="text-muted text-xs block">Thông tin tài khoản <b class="caret"></b></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{route('user.edit',Auth::user()->id)}}">Thông tin cá nhân</a></li>
                            <li class="divider"></li>
                            <li><a href="{{route('logout')}}">Thoát</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">CMS</div>
                </li>
                @if(Auth::user()->level)
                    <li class="active">
                        <a href="#"><i class="fa fa-th-large"></i> 
                            <span class="nav-label">Sản phẩm</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{route('product.index')}}">Sản phẩm</a></li>
                            <li><a href="{{route('category.index')}}">Thể loại Sản phẩm</a></li>
                            <li><a href="{{route('selective.index')}}">Sản phẩm Chọn lọc</a></li>
                            <li><a href="{{route('hot.index')}}">Sản phẩm Hot</a></li>
                        </ul>
                    </li>
                @endif
                @if(Auth::user()->level)
                    <li class="">
                @else
                    <li class="active">
                @endif
                    <a href="#">
                        <i class="fa fa-th-large"></i> 
                        <span class="nav-label">Bài viết</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{route('post.index')}}">Bài viết</a></li>
                        <li><a href="{{route('postcategory.index')}}">Thể loại Bài viết</a></li>
                    </ul>
                </li>
                <li class="">
                    <a href="#">
                        <i class="fa fa-th-large"></i> 
                        <span class="nav-label">Tin tức</span>
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{route('news.index')}}">Tin tức</a></li>
                        <li><a href="{{route('newscategory.index')}}">Thể loại Tin tức</a></li>
                    </ul>
                </li>
                @if(Auth::user()->level)
                    <li class="">
                        <a href="#">
                            <i class="fa fa-users"></i> 
                            <span class="nav-label">Người dùng</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{route('user.index')}}">Người dùng</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#">
                            <i class="fa fa-th-large"></i> 
                            <span class="nav-label">Thông tin khác</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{route('menu.index')}}">Menu</a></li>
                            <li><a href="{{route('slide.index')}}">Slide</a></li>
                            <li><a href="{{route('tag.index')}}">Từ khóa</a></li>
                            <li><a href="{{route('config.index')}}">Cấu hình</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="{{route('logout')}}"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-top-links navbar-right">
                        <li><h4><span class="m-r-sm text-muted welcome-message">Trang quản trị</span></h4></li>
                        <li><form class="" action="{{route('logout')}}" method="post">
                            @csrf
                            <button style="margin-top: 20px;margin-bottom: 20px" type="submit" name="logout" class="btn btn-success" style="float:right;margin-right: 40px"><i class="fa fa-sign-out"></i> Thoát</button>
                        </form></li>
                        <!-- <li><a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Thoát</a></li> -->
                    </ul>
                </nav>
            </div>