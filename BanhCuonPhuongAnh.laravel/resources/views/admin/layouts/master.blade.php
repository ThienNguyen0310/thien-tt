<!DOCTYPE html>
<html lang="en">
<head>
	@yield('meta_javascript')
	<title>@yield('title')</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
	
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

	<link rel="shortcut icon" href="favicon.ico">    
	<script src="{{asset('js/jquery-2.1.1.js')}}"></script>
    <script src="{{asset('assets/base.js')}}"></script>
</head>
<body>
	@yield('link')
	@include('admin.layouts.header')
	@yield('content')
	@include('admin.layouts.footer')
	@yield('javascript')
</body>
</html>