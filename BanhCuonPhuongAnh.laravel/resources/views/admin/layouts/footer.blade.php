        <div class="row">
            <div class="footer">
                <div class="pull-right">
                    <strong>Liên hệ:</strong> 0978279190
                </div>
                 <div><strong>Copyright</strong> ThienNV &copy; 2018</div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('js/inspinia.js')}}"></script>
<script src="{{asset('js/plugins/chartJs/Chart.min.js')}}"></script>
<script src="{{asset('assets/js/app.js')}}"></script>
