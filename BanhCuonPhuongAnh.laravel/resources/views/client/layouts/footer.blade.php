<footer id="footer" class="footer">
    <div class="container">
    	<div class="row">
        	<div class="col col-md-8 col-sm-12">
	            <ul> 
	            	@foreach($configs as $config)
	                	@if($config->name == 'contact_company')
	                		<?php  $contact_company = $config->value?>
	                	@elseif($config->name == 'contact_address') 
	                		<?php $contact_address = $config->value?>
	                	@elseif($config->name == 'contact_mobile')
	                		<?php $contact_mobile  =  $config->value?>
	                	@elseif($config->name == 'contact_email')   
			                <?php $contact_email   =  $config->value?>
			            @endif
	                @endforeach
					<li><h4><b>{{$contact_company}}</b></h4></li>
					<li> <i class="fa fa-map-marker"></i>{{$contact_address}}</li>
					<li><i class="fa fa-phone"></i> {{$contact_mobile}}</li>
					<li><i class="fa fa-paper-plane-o"></i>      
			            <a href="mailto:banhcuonphuonganh@gmail.com">{{$contact_email}}</a>
			        </li>
	            </ul>
        	</div>

        	<div class="col col-md-2 col-sm-12">
	            <ul class="list-block">
	            	<li></li>
	            	@foreach($menus as $menu)
	                	@if($menu->name == 'Giới thiệu')
	                		<li><a href="{{$menu->url}}" >Giới thiệu</a></li>
	                	@elseif($menu->name == 'Hệ thống cửa hàng')
	                		<li><a href="{{$menu->url}}" >Hệ thống cửa hàng</a></li>
	                	@endif
	                @endforeach
	            </ul>
        	</div>

        	<div class="col col-md-2 col-sm-12">
	            <ul class="list-block">
	                <li></li>
	                @foreach($configs as $config)
	                	@if($config->name == 'facebook_url')
	                		<li><a class="fa fa-facebook" href="{{$config->value}}" title="{{$config->description}}" target="_blank">{{$config->description}}</a>
	                		</li>
	                		@break
	                	@endif
	               	@endforeach
	            </ul>
        	</div>
    	</div>
	</div>
	<div class="bottomBar">
	    <div class="container">
	        <div class="row">
	            <div class="col-sx-12 col-sm-12 col-md-9">Copyright © 2018 H2T . All Rights Reserved.</div>
	            <div class="col-sx-12 col-sm-12 col-md-3 pull-right" style="text-align: right;">
	                <a href="http://h2t.vn/" target="_blank">Thiết kế website</a> và phát triển bởi H2T
	            </div>
	        </div>
	    </div>
	</div>        
</footer>

    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.bxslider.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/owl.carousel.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/global.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
    <script>
	    $(document).ready(function () {
	        $("#fb-chat-header").click(function () {
	            $('#fb-chat-box').toggle();
	        });
	    });
	</script>
		<div id="fb-chat">
		    <div id="fb-chat-header" class="fb-chat-header">
		        <i class="fa fa-facebook"></i> <span style="text-transform: uppercase; margin-left: 10px;">Hỗ trợ khách hàng</span>
		    </div>

		    <div id="fb-chat-box" class="fb-chat-box">
		        <div class="fb-page" data-tabs="messages" data-href="https://www.facebook.com/thiennguyen0397" data-width="300"
		             data-height="325" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
		             data-show-facepile="true" data-show-posts="false">
		        </div>
		    </div>
		</div>
