<header>
<div id="header" class="header">
    <div class="container">
        <div class="col-xs-12 col-sm-4 col-md-3 text-left">

            <span id="logo" itemscope="" itemtype="http://schema.org/Products">
                <a href="/" itemprop="url" title="Bánh cuốn Phương Anh" class="logo_brand">
                    <img class="img-responsive" src="{{asset('img/landing/logo-banhcuongiaan.png')}}" alt="Banh cuon Phương Anh" itemprop="logo">
                </a>
            </span>

        </div>
        <div class="col-xs-12 col-sm-8 col-md-9 text-right">
            <div class="header-contact-info">
                <span class="icon-phone"><i class="fa fa-phone"></i></span>
                <div class="contact-info">
                    <span>Giao hàng tận nơi</span> <strong>0934 813 689</strong>
                </div>
            </div>
            <form action="/tim-kiem">
                <div class="search-form">
                    <input name="search" type="text" id="search" placeholder=" Tìm kiếm sản phẩm..." class="input-large" value="">
                    <button type="submit" title="Tìm kiếm" class="button btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>            <div class="center-block">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="true">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="navbar-collapse collapse in" id="bs-example-navbar-collapse-1" aria-expanded="true">
            <div class="container">
                <ul class="nav navbar-nav">
                    @foreach($menu_parent as $menu)
                        @if($menu->status)
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="{{$menu->url}}" onclick="location.href='{{$menu->url}}'">{{$menu->name}}</a>
                                @if($menu->menu_children)
                                    @foreach($menu->menu_children as $m)
                                        @if($m->status)
                                            <ul class="dropdown-menu">
                                                <li class="dropdown">
                                                    <a href="{{$m->url}}" >{{$m->name}}</a>
                                                </li>
                                            </ul>
                                        @endif
                                     @endforeach   
                                @endif
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </nav>
</div>
</header>