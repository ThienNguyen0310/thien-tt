<!DOCTYPE html>
<html lang="en">
<head>
	<title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="deRV7Oe2SrVA31k7uCQOXrtRiy5tZI04yiF5KJ3n">
    <meta http-equiv="content-language" itemprop="inLanguage" content="vi">
    <meta name="language" content="vietnamese">
    <meta name="robots" content="index,follow,noodp">
    <meta name="copyright" content="Copyright © 2016 by FiANCE">
    <meta name="abstract" content="FIANCE Website buôn bán số 1 Việt Nam">
    <meta name="distribution" content="Global">
    <meta name="author" itemprop="author" content="banhcuonphuonganh.com">
    <meta name="keywords" content="Bánh cuốn Phương Anh"/>
    <meta name="description" content="Bánh cuốn Phương Anh"/>
    <meta name="robots" content="index,follow,noodp,noydir"/>
    <meta name="Googlebot" content="index, follow"/>
    <meta http-equiv="refresh" content="1200">

    <link rel="alternate" href="http://banhcuonphuonganh.com/" hreflang="x-default">
    <link rel="canonical" href="http://banhcuonphuonganh.com/">
    <link rel="alternate" media="handheld" href="http://banhcuonphuonganh.com/">

    <link rel="shortcut icon" href="{{asset('img/landing/favicon.ico')}}">
        <!-- Css -->
    <link href="{{asset('css/css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.min.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/flug-in.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/global.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animates.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}" media="all">
    <!-- <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
    <style type="text/css">
        .submenu {
            display: none
        }
    </style>
 
	<script language="javascript">
	    document.onmousedown = disableclick;
	    status = "Right Click Disabled";
	    function disableclick(event)
	    {
	        if (event.button == 2)
	        {
	            return false;
	        }
	    }
	</script>
  </head>
    <body>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=1212916368848877&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
	@yield('link')
	@include('client.layouts.header')
	@yield('content')
	@include('client.layouts.footer')
	@yield('javascript')
</body>
</html>