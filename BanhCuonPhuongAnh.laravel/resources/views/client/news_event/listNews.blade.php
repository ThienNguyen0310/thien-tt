@extends('client.layouts.master')
@section('title','Tin Tức')
@section('content')     
 <section role="main" class="bgr_body">
    <div class="bgr_content">
        <div id="main" class="container">
            <div id="col_main" class="col-md-9 page">
                <div class="breadCrumb">
                    <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <i class="fa fa-home" aria-hidden="true"></i>
                        <a href="/" itemprop="url"><span itemprop="title">Trang chủ</span></a> ›
                    </span>
                    <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="/tin-tuc" itemprop="url"><span itemprop="title">Tin Tức</span> </a>
                    </span>
                    <span>
                    </span>
                    <!-- ./ breadCrumb rich snippets -->
                </div>

                <h1 class="post-title">
                    Tin Tức
                </h1>
                <div id="ctl14_ctl00_pnlData">
                    @foreach($news as $new)
                        @if($new->status)
                            <div class="row inline_page">
                                <div class="col-md-5">
                                    <div class="thumbnail" style="height:125px;width:220px;">
                                        @if($new->image)
                                            <a href="{{'/'.$new->slug.','.$new->hashids}}">
                                                <img style="height:125px;width:220px;" alt="{{$new->name}}" src="{{Storage::url('images/'.$new->image)}}" class="featurette-image img-responsive center-block" title="{{$new->name}}"></a>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <h2>
                                        <a href="{{'/'.$new->slug.','.$new->hashids}}">{{$new->name}}</a></h2>
                                    <span class="time_index">{{$new->updated_at}}</span>
                                    <p class="article_short">{!!Str::Words($new->description,30,'...')!!}</p>
                                        <div class="article_more">
                                            <a href="{{'/'.$new->slug.','.$new->hashids}}" class="btn"><i class="fa fa-check-square-o" aria-hidden="true"></i>
                                            Xem chi tiết</a>
                                        </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="row">
                        <ul class="block_list"></ul>
                        <div id="ctl14_ctl00_pnlCollection">
                            <div class="pagenav">
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <div id="col_complementary" class="col-md-3">
                    <div class="widget_box">
                        <h4 class="widget_box_title">
                            <a href="/tin-tuc"> <i class="fa fa-bars"></i>Danh mục tin tức</a>
                        </h4>
                        @foreach($newsCategories as $category)
                                <ul class="cate_list">
                                    <li>
                                        <h2 class="tab_title"><a href="{{'/tin-tuc/'.$category->slug}}">{{$category->name}}</a></h2>
                                    </li>
                                </ul>
                        @endforeach
                                            
                    </div>
                    <div class="widget_box main-field">
                        <h4 class="widget_box_title">
                            <i class="fa fa-film"></i>
                            VIDEO
                        </h4>
                        <div style="text-align: center; width: 100%; margin: 0 auto; position: relative;">
                            <object width="100%" height="226">
                                <param value="http://www.youtube.com/v/imDrIMvZ91g&amp;hl=en_US&amp;fs=1&amp;rel=0&amp;autoplay=0" name="movie">
                                <param value="true" name="allowFullScreen">
                                <param value="Transparent" name="WMode">
                                <param value="always" name="allowscriptaccess">
                                <embed width="100%" height="226" allowfullscreen="true" allowscriptaccess="always" wmode="Transparent" type="application/x-shockwave-flash" src="http://www.youtube.com/v/imDrIMvZ91g&amp;hl=en_US&amp;fs=0&amp;rel=0&amp;autoplay=0"></object>
                        </div>
                        <h5 class="article_title"><a href="#">Cách làm bánh cuốn tại nhà đơn giản</a></h5>
                    </div>
                </div>
            </div>
            <!-- Extension -->
        </div>
    </section>
@endsection 
@section('javascript') 
    <script src="{{asset('js/plugins/jquery.elevatezoom.js')}}"></script>
    <script type="text/javascript">
        $("#zoom_01").elevateZoom({
            cursor: 'pointer',
            galleryActiveClass: 'active',
            imageCrossfade: true,
            zoomWindowWidth:500,
            zoomWindowHeight:500
        });

        $(".product_image").on('click', function(e) {
            var $this = $(this), $image = $('#zoom_01');

            $image.attr('src', $this.attr('data-image'));
            $image.attr('data-zoom-image', $this.attr('data-zoom-image'));

            var ez = $('#zoom_01').data('elevateZoom');

            ez.swaptheimage($this.attr('data-image'), $this.attr('data-zoom-image'));

            e.preventDefault();
        });

        </script>
@endsection
        