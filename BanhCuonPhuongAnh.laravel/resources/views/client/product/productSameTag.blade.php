@extends('client.layouts.master')
@section('title','Sản Phẩm Bánh Cuốn Phương Anh')
@section('content')     
 <section role="main" class="bgr_body">
    <div class="bgr_content">
        <div id="main" class="container">
            <div id="col_main" class="col-md-9 page">
                <div class="breadCrumb">
                    <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <i class="fa fa-home" aria-hidden="true"></i>
                        <a href="/" itemprop="url"><span itemprop="title">Trang chủ</span></a> ›
                    </span>
                    <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="/san-pham" itemprop="url"><span itemprop="title">Sản phẩm</span> </a>
                    </span>
                    <span>
                    </span>
                    <!-- ./ breadCrumb rich snippets -->
                </div>

                <h1 class="post-title">
                    Bánh cuốn Phương Anh
                </h1>
                <div class="block_grid clearfix">
                    <div class="row">
                        <!-- ./PRODUCTS -->
                        @foreach($product_k as $product)
                            @if($product->status)
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <article class="item" style="animation-delay: 250ms;">
                                        <div class="article_meta">
                                            <div class="aricle_image">
                                                <a href="{{'/'.$product->slug.','.$product->hashids}}" title="{{$product->name}}">
                                                    <img alt="{{$product->name}}" src="{{Storage::url('images/'.$product->image)}}" style="max-height: 160px;width: 100%;" class="featurette-image img-responsive center-block"></a>
                                            </div>
                                            <h2 class="article_title"><a href="{{'/'.$product->slug.','.$product->hashids}}">{{$product->name}}</a></h2>
                                            <b style="color: #ff500b; font-size: 16px;">{{$product->price}} vnđ</b>
                                            <p class="article_short">{!!Str::Words($product->description,25,'...')!!}</p>
                                            <div class="article_more">
                                                <a href="{{'/'.$product->slug.','.$product->hashids}}" class="btn"><i class="fa fa-check-square-o" aria-hidden="true"></i>
                                                    Xem chi tiết</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>

                <div class="block">
                    
                </div>

            </div>
                <div id="col_complementary" class="col-md-3">
                    <div class="widget_box">
                        <h4 class="widget_box_title">
                            <a href="/san-pham"> <i class="fa fa-bars"></i>Danh mục sản phẩm</a>
                        </h4>
                        @foreach($productCategories as $category)
                                <ul class="cate_list">
                                    <li>
                                        <h2 class="tab_title"><a href="{{'/san-pham/'.$category->slug}}">{{$category->name}}</a></h2>
                                    </li>
                                </ul>
                        @endforeach
                                            
                    </div>
                    <div class="widget_box main-field">
                        <h4 class="widget_box_title">
                            <i class="fa fa-film"></i>
                            VIDEO
                        </h4>
                        <div style="text-align: center; width: 100%; margin: 0 auto; position: relative;">
                            <object width="100%" height="226">
                                <param value="http://www.youtube.com/v/imDrIMvZ91g&amp;hl=en_US&amp;fs=1&amp;rel=0&amp;autoplay=0" name="movie">
                                <param value="true" name="allowFullScreen">
                                <param value="Transparent" name="WMode">
                                <param value="always" name="allowscriptaccess">
                                <embed width="100%" height="226" allowfullscreen="true" allowscriptaccess="always" wmode="Transparent" type="application/x-shockwave-flash" src="http://www.youtube.com/v/imDrIMvZ91g&amp;hl=en_US&amp;fs=0&amp;rel=0&amp;autoplay=0"></object>
                        </div>
                        <h5 class="article_title"><a href="#">Cách làm bánh cuốn tại nhà đơn giản</a></h5>
                    </div>

                    <div class="widget widget_box main-field">
                        <h4 class="widget_box_title">
                            <i class="fa fa-film"></i>
                            Bài viết
                        </h4>
                        <ul class="widget_list">
                            <?php $count = 1 ?>
                            @foreach($news as $new)
                                @if($new->status)
                                    <li>
                                        <div class="number left">
                                            {{$count++}}
                                        </div>
                                        <a href="{{'/'.$new->slug.','.$new->hashids}}" title="{{$new->name}}">{{$new->name}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>
            <!-- Extension -->
        </div>
    </section>
@endsection 
@section('javascript') 
    <script src="{{asset('js/plugins/jquery.elevatezoom.js')}}"></script>
    <script type="text/javascript">
        $("#zoom_01").elevateZoom({
            cursor: 'pointer',
            galleryActiveClass: 'active',
            imageCrossfade: true,
            zoomWindowWidth:500,
            zoomWindowHeight:500
        });

        $(".product_image").on('click', function(e) {
            var $this = $(this), $image = $('#zoom_01');

            $image.attr('src', $this.attr('data-image'));
            $image.attr('data-zoom-image', $this.attr('data-zoom-image'));

            var ez = $('#zoom_01').data('elevateZoom');

            ez.swaptheimage($this.attr('data-image'), $this.attr('data-zoom-image'));

            e.preventDefault();
        });

        </script>
@endsection
        