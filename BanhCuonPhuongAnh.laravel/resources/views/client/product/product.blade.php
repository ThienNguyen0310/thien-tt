@extends('client.layouts.master')
@section('title',$product->name)
@section('content')     
    <section role="main" class="bgr_body">
        <div class="bgr_content">
            <div id="main" class="container">
                <div id="col_main" class="col-md-9 page">
                    <div class="breadCrumb">
                        <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <a href="/" itemprop="url"><span itemprop="title">Trang chủ</span></a> ›
                        </span>
                        <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="/san-pham" itemprop="url"><span itemprop="title">Sản phẩm</span> </a>
                        </span>
                        <span>{{$product->name}}</span>
                        <!-- ./ breadCrumb rich snippets -->
                    </div>
                    <div class="product-details"><!--product-details-->
                        <div class="col-sm-5">
                            <div class="view-product">
                                <img id="zoom_01" src="{{Storage::url('images/'.$product->image)}}"
                                     data-zoom-image="{{Storage::url('images/'.$product->image)}}"
                                     alt="{{$product->name}}" style="max-width: 324px; max-height: 324px;"/>
                            </div>
                            @if($product->products_photos)
                                <div id="similar-product" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            @foreach($product->products_photos as $photo)
                                                <a href="#" class="product_image elevatezoom-gallery"
                                               data-image="{{Storage::url('images/'.$photo->filename)}}"
                                               data-zoom-image="{{Storage::url('images/'.$photo->filename)}}">
                                                <img src="{{Storage::url('images/'.$photo->filename)}}"/>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- Controls -->
                                    <a class="left item-control" href="#similar-product" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="right item-control" href="#similar-product" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            @endif
                        </div>
                        <div class="col-sm-7">
                            <div class="product-information"><!--/product-information-->
                                <h2>{{$product->name}}</h2>

                                <div>
                                    <div><label for="">Giá:</label>
                                        <b style="color: #ff500b; font-size: 16px;">{{$product->price}} vnđ</b>
                                    </div>
                                </div>
                                <span>
                                    <p style="margin-top: 10px;"></p>
                                    <div>
                                        <p>{!!$product->content!!}</p><div style="text-align: justify;"><br></div>
                                    </div>
                                    @if($product->key_words)
                                        <p>
                                            <b>Tag:</b> 
                                            <?php $count = 0; ?>
                                            @foreach($product->key_words as $key_word)
                                                @if($count) , @endif
                                                    <a href = "{{'/tag/'.$key_word->slug}}">{{$key_word->name}}</a>  
                                                <?php $count++ ?>
                                            @endforeach
                                        </p>
                                    @endif
                                </span>
                            </div>
                            <!--/product-information-->
                        </div>
                    </div>
                    <div class="clear"></div>
                    <br />
                    <div id="ctl14_ctl00_pnlData">
                        <div class="post">
                            <div class="option cleafix" style="display: block;">
                                <div class="social_google-plus fleft">
                                    <div class="fb-like" data-href="{{'/san-pham/'.$product->slug}}" data-width="800" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                                </div>
                                <div class="social_sharing fleft">
                                </div>
                            </div>
                            <!-- ./ #content -->
                            <!-- <div id="post-relative"> -->
                                <div class="fb-comments" data-href="{{'http://banhcuonphuonganh.com/san-pham/'.$product->slug}}" data-numposts="5"></div>
                                <!-- Social content here -->
                            <!-- </div> -->
                            <!-- ./ #relatives -->

                        </div>
                        <!--/recommended_items-->
                    </div>
                </div>
                <div id="col_complementary" class="col-md-3">
                    <div class="widget_box">
                        <h4 class="widget_box_title">
                            <a href="/san-pham"> <i class="fa fa-bars"></i>Danh mục sản phẩm</a>
                        </h4>
                        @foreach($productCategories as $category)
                                <ul class="cate_list">
                                    <li>
                                        <h2 class="tab_title"><a href="{{'/san-pham/'.$category->slug}}">{{$category->name}}</a></h2>
                                    </li>
                                </ul>
                        @endforeach
                                            
                    </div>
                    <div class="widget_box main-field">
                        <h4 class="widget_box_title">
                            <i class="fa fa-film"></i>
                            VIDEO
                        </h4>
                        <div style="text-align: center; width: 100%; margin: 0 auto; position: relative;">
                            <object width="100%" height="226">
                                <param value="http://www.youtube.com/v/imDrIMvZ91g&amp;hl=en_US&amp;fs=1&amp;rel=0&amp;autoplay=0" name="movie">
                                <param value="true" name="allowFullScreen">
                                <param value="Transparent" name="WMode">
                                <param value="always" name="allowscriptaccess">
                                <embed width="100%" height="226" allowfullscreen="true" allowscriptaccess="always" wmode="Transparent" type="application/x-shockwave-flash" src="http://www.youtube.com/v/imDrIMvZ91g&amp;hl=en_US&amp;fs=0&amp;rel=0&amp;autoplay=0"></object>
                        </div>
                        <h5 class="article_title"><a href="#">Cách làm bánh cuốn tại nhà đơn giản</a></h5>
                    </div>

                    <div class="widget widget_box main-field">
                        <h4 class="widget_box_title">
                            <i class="fa fa-film"></i>
                            Bài viết
                        </h4>
                        <ul class="widget_list">
                            <?php $count = 1 ?>
                            @foreach($news as $new)
                                <li>
                                    <div class="number left">
                                        {{$count++}}
                                    </div>
                                    <a href="{{'/'.$new->slug.','.$new->hashids}}" title="{{$new->name}}">{{$new->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>
            <!-- Extension -->
        </div>
    </section>
@endsection 
@section('javascript') 
    <script src="{{asset('js/plugins/jquery.elevatezoom.js')}}"></script>
    <script type="text/javascript">
        $("#zoom_01").elevateZoom({
            cursor: 'pointer',
            galleryActiveClass: 'active',
            imageCrossfade: true,
            zoomWindowWidth:500,
            zoomWindowHeight:500
        });

        $(".product_image").on('click', function(e) {
            var $this = $(this), $image = $('#zoom_01');

            $image.attr('src', $this.attr('data-image'));
            $image.attr('data-zoom-image', $this.attr('data-zoom-image'));

            var ez = $('#zoom_01').data('elevateZoom');

            ez.swaptheimage($this.attr('data-image'), $this.attr('data-zoom-image'));

            e.preventDefault();
        });

        </script>
@endsection
        