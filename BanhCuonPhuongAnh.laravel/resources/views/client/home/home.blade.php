@extends('client.layouts.master')
@section('title','Bánh Cuốn Phương Anh')
@section('content')     
    <section id="banner">
        <div class="bgr_banner">
            <div class="container">
                <div class="bx-wrapper" style="max-width: 100%;height:281px">
                    <ul id="bxslider" class="bxslider">
                        @foreach($slides as $slide)
                            <li class="bx-clone">
                                    <div class="bxs bxs_device">
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <h2>{{$slide->name}}</h2>
                                            <p>{{$slide->description}}</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-8">
                                            <a href="{{$slide->url}}" title="{{$slide->name}}">
                                                <img class="img-responsive" src="{{Storage::url('images/'.$slide->image)}}" alt="{{$slide->name}}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="bxs bxs_mobile">
                                        <div class="col-xs-12 col-sm-12 col-md-8">
                                            <a href="{{$slide->url}}" title="{{$slide->name}}">
                                                <img class="img-responsive" src="{{Storage::url('images/'.$slide->image)}}" alt="{{$slide->name}}">
                                            </a>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <h2>{{$slide->name}}</h2>
                                            <p>{{$slide->description}}</p>
                                        </div>
                                    </div>
                            </li>                                   
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- ./banner -->
        </div>

    </section>
    
    <section id="main" role="main" class="bgr_body">
        <div class="bgr_content">
            <!-- Adv Top-->
            <section id="block_ads">
                <div class="container">
                    <div class="row">
                        @foreach($hot_products as $hot_product)
                            @if($hot_product->product->status)
                                <div class="col-sm-6 col-md-3">
                                    <article class="wow fadeIn animated" data-wow-offset="10" style="animation-delay: 200ms; visibility: visible; animation-name: fadeIn;">
                                        <a href="{{'/'.$hot_product->product->slug.','.$hot_product->product->hashids}}" title="{{$hot_product->product->name}}">
                                            <img class="featurette-image img-responsive center-block" src="{{Storage::url('images/'.$hot_product->product->image)}}" style="max-height: 197px;width: 100%;" alt="{{$hot_product->product->name}}">
                                        </a>
                                        <h3 class="ads_tit"><a href="{{'/'.$hot_product->product->slug.','.$hot_product->product->hashids}}">{{$hot_product->product->name}}</a></h3>
                                        <b style="color: #ff500b; font-size: 16px;">{{$hot_product->product->price}} vnđ</b>
                                        <p class="ads_brief">{!! Str::words($hot_product->product->description, 30,'...') !!}</p>
                                        <a href="{{'/'.$hot_product->product->slug.','.$hot_product->product->hashids}}" class="ads_more">
                                            Xem thêm<i class="fa fa-chevron-circle-right"></i>
                                        </a>
                                    </article>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </section>

            <!-- Products -->
    
            <div class="block third">
                <!-- ./PRODUCTS -->
                <div class="container clearfix">
                    <h3 class="block_title_fix">
                        Sản phẩm tiêu biểu
                    </h3>
                    <div class="block_grid">
                        <div class="row">
                            @foreach($selective_products as $selective_product)
                                @if($selective_product->product->status)
                                    <div class="col-sm-6 col-md-3">
                                        <article class="wow fadeIn item animated" style="animation-delay: 250ms; visibility: visible; animation-name: fadeIn;">
                                            <div class="article_meta">
                                                <div class="aricle_image">
                                                    <a href="{{'/'.$selective_product->product->slug.','.$selective_product->product->hashids}}">
                                                        <img alt="{{$selective_product->product->name}}" src="{{Storage::url('images/'.$selective_product->product->image)}}" style="max-height: 160px;width: 100%;" class="featurette-image img-responsive center-block"></a>
                                                </div>
                                                <h2 class="article_title"><a href="{{'/'.$selective_product->product->slug.','.$selective_product->product->hashids}}">{{$selective_product->product->name}}</a></h2>
                                                <b style="color: #ff500b; font-size: 16px;">{{$selective_product->product->price}} vnđ</b>
                                                <p class="article_short">{!! Str::words($selective_product->product->description, 30,'...') !!}</p>
                                                <div class="article_more">
                                                    <a href="{{'/'.$selective_product->product->slug.','.$selective_product->product->hashids}}" class="btn"><i class="fa fa-check-square-o" aria-hidden="true"></i>
                                                        Xem chi tiết</a>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <!-- ./PRODUCTS -->
            </div>
        </div>
    </section>
@endsection  
        