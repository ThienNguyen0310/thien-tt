<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::group(['domain'=>'cms.banhcuonphuonganh.com'],function(){

	Route::prefix('product')->group(function(){
		Route::resource('/category','ProductCategoryController');
		Route::resource('/gallery','ProductPhotoController');
		Route::resource('/hot','HotProductController');
		Route::resource('/selective','SelectiveProductController');
	});
	 
	Route::resource('/product','ProductController');
	Route::resource('/','ProductController');
	
	Route::resource('/tag','KeyWordController');

	Route::prefix('post')->group(function(){
		// Route::resource('/category','PostCategoryController');
		Route::get('/category/create', 'PostCategoryController@create')->name('postcategory.create');
		Route::get('/category', 'PostCategoryController@index')->name('postcategory.index');
		Route::post('/category', 'PostCategoryController@store')->name('postcategory.store');
		Route::delete('/category/{category}', 'PostCategoryController@destroy')->name('postcategory.destroy');
		Route::get('/category/{category}/edit', 'PostCategoryController@edit')->name('postcategory.edit');
		Route::patch('/category/{category}', 'PostCategoryController@update')->name('postcategory.update');
		Route::get('/category/{category}', 'PostCategoryController@show')->name('postcategory.show');
	});
	 
	Route::resource('/post','PostController');

	Route::prefix('news')->group(function(){
		Route::get('/category/create', 'NewsCategoryController@create')->name('newscategory.create');
		Route::get('/category', 'NewsCategoryController@index')->name('newscategory.index');
		Route::post('/category', 'NewsCategoryController@store')->name('newscategory.store');
		Route::delete('/category/{category}', 'NewsCategoryController@destroy')->name('newscategory.destroy');
		Route::get('/category/{category}/edit', 'NewsCategoryController@edit')->name('newscategory.edit');
		Route::patch('/category/{category}', 'NewsCategoryController@update')->name('newscategory.update');
		Route::get('/category/{category}', 'NewsCategoryController@show')->name('newscategory.show');
		Route::get('/category/image-crop-upload', 'NewsCategoryController@cropUploadImage');
	});
	 
	Route::resource('/news','NewsController');
	Route::resource('/config','ConfigurationController');
	Route::resource('/user','UserController');

	Route::prefix('menu')->group(function(){
		Route::get('/order','MenuController@order')->name('menu.order');
		Route::post('/orderStore','MenuController@orderStore')->name('menuorder.store');
	});
	Route::resource('/menu','MenuController');

	Route::resource('/slide','SlideController');


	Auth::routes();
	Route::get('/user/verify/{confirmation_code}','Auth\RegisterController@verifyUser')->name('activate.user');
	Route::get('/home', 'HomeController@index')->name('home');
	Route::delete('/deleteRecord/{id}','ProductPhotoController@deleteRecord');
});

Route::group(['domain'=>'banhcuonphuonganh.com'],function(){
	Route::get('/','Client\HomeController@index');
	Route::get('/bai-viet/{slug}','Client\PostController@getDetailPost');

	//Route::get('/tin-tuc/{slug1}/{slug2}','Client\NewsController@getDetailNews');
	Route::get('{slug},{news_id}','Client\NewsController@getDetailNews')->where(['news_id' => '.{4}']);
	Route::get('/tin-tuc/{slug}','Client\NewsController@getNewsSameCategory');
	Route::get('/tin-tuc','Client\NewsController@getListNews');

	Route::get('{slug},{product_id}','Client\ProductController@getDetailProduct')->where(['product_id' => '.{6}']);
	Route::get('/san-pham/{slug}','Client\ProductController@getProductSameCategory');
	Route::get('/tag/{slug}','Client\ProductController@getProductSameTag');
	Route::get('/san-pham','Client\ProductController@getListProduct');
	Route::get('/tim-kiem','Client\ProductController@search');
	// Route::get('/slide/{slug}','Client\SlideController@show');
});
