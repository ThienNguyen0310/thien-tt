<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Key_Word extends Model
{
	use SoftDeletes;
	public $table = "key_words";

	protected $fillable = ['name','slug'];


	public function products(){
		return $this->belongsTomany('App\Product','products_key_words','key_word_id','product_id')->withTimestamps();
	}
}
