<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{
	use SoftDeletes;

    public function post_category(){
    	return $this->belongsTo('App\Post_Category');
    }
}
