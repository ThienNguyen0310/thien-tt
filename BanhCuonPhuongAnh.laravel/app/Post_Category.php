<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post_Category extends Model
{
	use SoftDeletes;
    public $table = "post_categories";

    public function posts(){
    	return $this->hasMany('App\Posts','post_category_id');
    }
}
