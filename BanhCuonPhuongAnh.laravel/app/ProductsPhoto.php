<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsPhoto extends Model
{
	use SoftDeletes;
	
	protected $fillable = [ 'filename', 'product_id'];

    public function product(){
    	return $this->belongsTo('App\Product');
    }
}
