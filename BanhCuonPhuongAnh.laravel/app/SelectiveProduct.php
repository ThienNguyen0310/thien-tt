<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class SelectiveProduct extends Model
{
    //use SoftDeletes;
    public $table = "selective_products";
    
    public function product(){
    	return $this->belongsTo('App\Product');
    }
}
