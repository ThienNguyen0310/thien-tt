<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class News_Category extends Model
{
    use SoftDeletes;
    public $table = "news_categories";

    public function news(){
    	return $this->hasMany('App\News','news_category_id');
    }
}
