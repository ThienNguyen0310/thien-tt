<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;


class HotProduct extends Model
{
    //use SoftDeletes;
    public $table = "hot_products";
    
    public function product(){
    	return $this->belongsTo('App\Product');
    }

}
