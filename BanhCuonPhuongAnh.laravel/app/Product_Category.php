<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Category extends Model
{
	use SoftDeletes;
	public $table = "product_categories";
    
    public function products(){
    	return $this->hasMany('App\Product','product_category_id');
    }
}
