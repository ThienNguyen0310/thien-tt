<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('product_id');
        return [
            'name'  => 'bail|required|min:6|unique:products,name,'.$id,
            'slug'  => 'bail|required|unique:products,slug,'.$id,
            'price' => 'required|regex:/^\d*(\.\d{1,3})?$/',
            'image' => 'bail|nullable|image|max:5000',
            
        ];
    }

    public function messages()
    {
        return [
            'name.required'  => 'Name is required',
            'name.min'       => 'Name is at least 6 characters long',
            'name.unique'    => 'Name already exists',
            'image.max'      => 'Image image has the largest size is 5000kb',
            'image.image'    => 'The image must be formatted jpeg, png, bmp, gif, or svg',
            'price.required' => 'Price is required',
            'price.regex'    => 'Price must be in the format abc.xyz or abc ',
            'slug.required'  => 'Slug is required',
            'slug.unique'    => 'Slug already exists',
        ];     
    }
}
