<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('user_id');
        return [
          'name'        => 'bail|required|min:6',
          'email'       => 'bail|required|email|unique:users,email,'.$id,
          'username'    => 'bail|required|min:6|unique:users,username,'.$id,
          'password'    => 'bail|required|min:8|confirmed',
          'phone_number'=> 'bail|nullable|numeric|digits_between:9,11|unique:users,phone_number,'.$id,       
        ];
    }

    public function messages()
    {
        return [
            'name.required'       => 'Name is required',
            'name.min'            => 'Name is at least 6 characters long',
            'email.required'      => 'Email is required',
            'email.email'         => 'Your email address must be in the format of name@domain.com',
            'email.unique'        => 'Email already exists',
            'username.required'  => 'Username is required',
            'username.min'        => 'Username is at least 6 characters long',
            'username.unique'     => 'Email already exists',
            'password.min'        => 'Password is at least 8 characters long',
            'password.required'   => 'Password is required',
            'password.confirmed'  => 'Password and Confirm Password no matching',
            'phone_number.digits_between'  => 'Phone number is beween 9 and 11 characters long',
            'phone_number.numeric'=> 'Phone number must numeric',
            'phone_number.unique' => 'Phone number already exists',
        ];
    }
}
