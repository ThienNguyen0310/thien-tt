<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SlideUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('slide_id');
        return [
            'name'  => 'bail|required|min:6|unique:slides,name,'.$id,
            'image' => 'bail|nullable|image|max:5000',
            'url'   => 'bail|required|unique:slides,url,'.$id,
        ];
    }

    public function messages()
    {
        return [
            'name.required'  => 'Name is required',
            'name.min'       => 'Name is at least 6 characters long',
            'name.unique'    => 'Name already exists',
            'image.max'      => 'Image image has the largest size is 5000kb',
            'image.image'    => 'The image must be formatted jpeg, png, bmp, gif, or svg',
            'url.required'   => 'Url is required',
            'url.unique'     => 'Url already exists',
        ];     
    }
}
