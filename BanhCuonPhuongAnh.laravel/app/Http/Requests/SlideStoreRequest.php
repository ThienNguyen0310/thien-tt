<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SlideStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'bail|required|min:6|unique:slides,name',
            'url'   => 'bail|required|unique:slides,url',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Name is required',
            'name.min'      => 'Name is at least 6 characters long',
            'name.unique'   => 'Name already exists',
            'url.required'  => 'Url is required',
            'url.unique'    => 'Url already exists',
        ];
    }
}
