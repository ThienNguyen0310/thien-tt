<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsCategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('news_category_id');
        return [
            'name'  => 'bail|required|min:6|unique:news_categories,name,'.$id,
            'slug'  => 'bail|required|min:6|unique:news_categories,slug,'.$id,
        ];
    }

    public function messages(){
        return [
            'name.required'  => 'Name is required',
            'name.min'       => 'Name is at least 6 characters long',
            'name.unique'    => 'Name already exists',
            'slug.required'  => 'Slug is required',
            'slug.min'       => 'Slug is at least 6 characters long',
            'slug.unique'    => 'Slug already exists',
        ];     
    }
}
