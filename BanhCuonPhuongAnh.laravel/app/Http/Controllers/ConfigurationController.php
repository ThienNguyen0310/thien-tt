<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuration;
use App\Http\Requests;
use App\Http\Requests\ConfigurationStoreRequest;
use App\Http\Requests\ConfigurationUpdateRequest;

class ConfigurationController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configs = Configuration::paginate(20); 
        return View('admin.other_information.configuration.index',compact('configs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.other_information.configuration.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConfigurationStoreRequest $request)
    {
        $conf                     = new Configuration();
        $conf->name               = $request->name;
        $conf->value              = $request->value;
        $conf->description        = $request->description;
        try {
            $conf->save();
            return redirect()->route('config.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $config = Configuration::find($id);
        return View('admin.other_information.configuration.edit',compact('config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConfigurationUpdateRequest $request, $id)
    {
        $conf                     = Configuration::find($id);
        $conf->name               = $request->name;
        $conf->value              = $request->value;
        $conf->description        = $request->description;
        try {
            $conf->save();
            return redirect()->route('config.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $config             = Configuration::find($id);
        try {
            $config->delete();
            return redirect()->route('config.index');
        } catch (Exception $e) {
            die($e->getMessages());
        }
    }
}
