<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProductCategoryStoreRequest;
use App\Http\Requests\ProductCategoryUpdateRequest;
use App\Product_Category;

class ProductCategoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_categories = Product_Category::paginate(10);
        return View('admin.product.product_category.index',compact('product_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_categories = Product_Category::all();
        return View('admin.product.product_category.create',compact('product_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryStoreRequest $request)
    {
        $pc                     = new Product_Category();
        $pc->name               = $request->name;
        $pc->slug               = $request->slug;
        $pc->description        = $request->description;
        $pc->parent_id          = $request->parent_id;
        $pc->meta_title         = $request->meta_title;
        $pc->meta_description   = $request->meta_description;
        $pc->meta_keyword       = $request->meta_keyword;

        try {
            $pc->save();
            return redirect()->route('category.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_categories = Product_Category::all();
        $product_category   = Product_Category::find($id);
        return View('admin.product.product_category.edit',compact('product_categories','product_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductCategoryUpdateRequest $request, $id)
    {
        $pc                     = Product_Category::find($id);
        $pc->name               = $request->name;
        $pc->slug               = $request->slug;
        $pc->description        = $request->description;
        $pc->parent_id          = $request->parent_id;
        $pc->meta_title         = $request->meta_title;
        $pc->meta_description   = $request->meta_description;
        $pc->meta_keyword       = $request->meta_keyword;

        try {
            $pc->save();
            return redirect()->route('category.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_category             = Product_Category::find($id);
        $product_categories           = Product_Category::all();
        $product_category->delete();

        foreach($product_categories as $category)
        {
            if($category->parent_id == $product_category->id)
            {
                $category->delete();
                foreach($category->products as $product)
                {
                    $product->delete();
                }
            }
        }

        foreach($product_category->products as $product)
        {
            $product->delete();
        }
        
        return redirect()->route('category.index'); 
    }
}
