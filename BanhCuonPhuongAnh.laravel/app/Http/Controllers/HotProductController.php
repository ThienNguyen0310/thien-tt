<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HotProduct;
use App\Product;



class HotProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hot_products       = HotProduct::where('deleted_at',null)->paginate(10);
        return View('admin.product.hot_product.index',compact('hot_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hot_products=  HotProduct::all();
        $ids = $request->get('ids');
        $result = route('hot.index');

        foreach($ids as $id){
            $tmp = 0;
            foreach($hot_products as $hotp){
                if($id == $hotp->product_id){
                    if($hotp->deleted_at){
                        $hot_p=  HotProduct::find($hotp->id);
                        $hot_p->deleted_at = null;
                        $hot_p->save();
                    }
                    $tmp = 1;
                    break;
                }
            }
            if(!$tmp){
                $hot_product = new HotProduct();
                $hot_product->product_id = $id;
                $hot_product->save();
            }
        }
        die(json_encode($result));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hot_product             = HotProduct::where('product_id',$id);
        $hot_product->delete();
        return redirect()->route('hot.index'); 
    }
}
