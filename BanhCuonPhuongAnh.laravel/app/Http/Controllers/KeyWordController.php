<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Key_Word;
use App\Http\Requests;
use App\Http\Requests\KeyWordStoreRequest;
use App\Http\Requests\KeyWordUpdateRequest;


class KeyWordController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $key_words = Key_word::paginate(10); 
        return View('admin.other_information.key_word.index',compact('key_words'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.other_information.key_word.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KeyWordStoreRequest $request)
    {
        $kw                     = new Key_Word();
        $kw->name               = $request->name;
        $kw->slug               = $request->slug;
        try {
            $kw->save();
            return redirect()->route('tag.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $key_word = Key_Word::find($id);
        return View('admin.other_information.key_word.edit',compact('key_word'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KeyWordUpdateRequest $request, $id)
    {
        $kw                     = Key_Word::find($id);
        $kw->name               = $request->name;
        $kw->slug               = $request->slug;
        try {
            $kw->save();
            return redirect()->route('tag.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $key_word             = Key_Word::find($id);
        $key_word->delete();

        $key_word->products()->detach();
        
        return redirect()->route('tag.index'); 
    }
}
