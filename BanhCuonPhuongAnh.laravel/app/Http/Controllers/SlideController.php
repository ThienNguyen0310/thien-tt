<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\SlideStoreRequest;
use App\Http\Requests\SlideUpdateRequest;
use App\Slide;
use Illuminate\Support\Facades\Storage;
use Image;

class SlideController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::paginate(10);
        return View('admin.other_information.slide.index',compact('slides'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.other_information.slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SlideStoreRequest $request)
    {
        $slide                     = new Slide();
        $slide->name               = $request->name;
        $slide->url                = $request->url;
        $slide->description        = $request->description;
        try {
            $slide->save();
            return redirect()->route('slide.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slide   = Slide::find($id);
        return View('admin.other_information.slide.see',compact('slide'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide    = Slide::find($id);
        return View('admin.other_information.slide.edit',compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SlideUpdateRequest $request, $id)
    {
        $slide                     = Slide::find($id);
        $slide->name               = $request->name;
        $slide->url                = $request->url;
        $slide->description        = $request->description;

        if ($request->hasFile('image')){
            $imagePath = $request->file('image')->store('public/images');
            $image = Image::make(Storage::get($imagePath))->resize(300,300)->encode();
            Storage::put($imagePath,$image);
            $imagePath = explode('/',$imagePath);
            $imagePath = $imagePath[2];
            $slide->image = $imagePath;
        }

        try {
            $slide->save();
            return redirect()->route('slide.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide             = Slide::find($id);
        try {
            $slide->delete();
            return redirect()->route('slide.index'); 
        } catch (Exception $e) {
            die($e->getMessage());
        }  
    }
}
