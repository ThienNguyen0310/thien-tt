<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Client\MenuController;
use Illuminate\Http\Request;
use App\Product;
use App\News;
use App\News_Category;
use App\Configuration;
use App\Menu;

class NewsController extends MenuController
{

    public function getListNews(){
        $menus              =   Menu::all();
        $newsCategories     =   News_Category::all();
        $configs            =   Configuration::all();
        $news               =   News::all();

        $menu_parent = $this->getMenus();

        return View('client.news_event.listNews',compact('menus','configs','menu_parent','news','newsCategories'));
    }

    public function getDetailNews($slug,$hashids){
        $menus              =   Menu::all();  
        $configs            =   Configuration::all();
        $newsCategories     =   News_Category::all();
        $newsDetail         =   News::where('slug',$slug)->first();
        
        $menu_parent = $this->getMenus();

        return View('client.news_event.detailNews',compact('menus','configs','menu_parent','newsDetail','newsCategories'));
    }

    public function getNewsSameCategory($slugCategory){
        $menus              =   Menu::all();  
        $configs            =   Configuration::all();
        $newsCategories     =   News_Category::all();
        $newsCategory       =   News_Category::where('slug',$slugCategory)->first();

        $menu_parent = $this->getMenus();

        return View('client.news_event.newsSameCategory',compact('menus','configs','menu_parent','newsCategory','newsCategories'));
    }

}
