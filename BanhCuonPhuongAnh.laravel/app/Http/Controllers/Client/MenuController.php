<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{

    public static function getMenus(){
        $menus              =   Menu::all();  
        $menu_parent = $menus->where('parent_id',null)->sortBy('sort');

        foreach($menu_parent as $menu){
            $myArray = [];
            foreach($menus as $m){
                if($menu->id == $m->parent_id){
                    array_push($myArray,$m);
                }
            }
            $menu->menu_children=$myArray;
        }
        return $menu_parent;
    }
}

    