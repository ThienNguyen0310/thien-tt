<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Client\MenuController;
use Illuminate\Http\Request;
use App\Product;
use App\News;
use App\News_Category;
use App\Product_Category;
use App\Configuration;
use App\Menu;

class ProductController extends MenuController
{

    public function getListProduct(){
        $menus              =   Menu::all();  
        $configs            =   Configuration::all();
        $productCategories  =   Product_Category::all();
        $products           =   Peoduct::all();
        $news               =   News::all();
        $menu_parent = $this->getMenus();

        return View('client.product.listproduct',compact('menus','configs','menu_parent','products','news','productCategories'));
    }

    public function getDetailProduct($slug,$hashids){
        $menus              =   Menu::all();  
        $configs            =   Configuration::all();
        $productCategories  =   Product_Category::all();
        $product            =   Product::where('slug',$slug)->first();
        $news               =   News::all();
        
        $menu_parent = $this->getMenus();

        return View('client.product.product',compact('menus','configs','menu_parent','product','news','productCategories'));
    }

    public function getProductSameTag($slug){
        $productCategories  =   Product_Category::all();
        $menus              =   Menu::all();  
        $configs            =   Configuration::all();
        $products           =   Product::all();
        $news               =   News::all();
        
        $product_k = [];
        foreach($products as $product){
            foreach($product->key_words as $key_word)
                if($key_word->slug == $slug){
                    array_push($product_k,$product);
                    break;
                }
        }

        $menu_parent = $this->getMenus();

        return View('client.product.productSameTag',compact('menus','configs','menu_parent','products','product_k','news','productCategories'));
    }

    public function getProductSameCategory($slugCategory){
        $menus              =   Menu::all();  
        $configs            =   Configuration::all();
        $productCategories  =   Product_Category::all();
        $productCategory    =   Product_Category::where('slug',$slugCategory)->first();
        $news               =   News::all();
        
        $menu_parent = $this->getMenus();

        return View('client.product.productSameCategory',compact('menus','configs','menu_parent','productCategory','productCategories','news'));
    }

    public function search(Request $request){
        $menus              =   Menu::all();  
        $configs            =   Configuration::all();
        $productCategories  =   Product_Category::all();

        $search = $request->search;

        $products = Product::where('name', 'like', "$search%")->get();
        
        if(!$products){
            $key_word = Key_Word::where('name', 'like', "$search%")->first();
            $products = $key_word->products;
        }

        $news = $this->getNews();
        $menu_parent = $this->getMenus();

        return View('client.product.searchproduct',compact('menus','configs','menu_parent','products','productCategories','news','search'));
    }
}
