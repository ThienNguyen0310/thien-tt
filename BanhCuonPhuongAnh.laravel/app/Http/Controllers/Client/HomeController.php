<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Slide;
use App\HotProduct;
use App\SelectiveProduct;
use App\Configuration;
use App\Menu;

class HomeController extends MenuController
{
    public function index(){
        $menus              =   Menu::all();  
        $configs            =   Configuration::all();
        $slides             =   Slide::all();
        $hot_products       =   HotProduct::all()->take(4);
        $selective_products =   SelectiveProduct::all()->take(4);

        $menu_parent = $this->getMenus();

        return View('client.home.home',compact('menus','configs','menu_parent','slides','hot_products','selective_products'));
    }
}
