<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Client\MenuController;
use Illuminate\Http\Request;
use App\News;
use App\News_Category;
use App\Configuration;
use App\Menu;
use App\Posts;

class PostController extends MenuController
{

    public function getDetailPost($slug){
        $menus              =   Menu::all();  
        $configs            =   Configuration::all();
        $post               =   Posts::where('slug',$slug)->first();
        $news               =   News::all();
        $menu_parent = $this->getMenus();

        return View('client.post.detailPost',compact('menus','configs','menu_parent','post','news'));
    }
}
