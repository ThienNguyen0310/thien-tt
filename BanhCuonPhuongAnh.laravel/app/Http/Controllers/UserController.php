<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Event;
use App\Events\SendMail;
use App\VerifyUser;

class UserController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        return View('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $user                     = new User();
        $user->username           = $request->username;
        $user->email              = $request->email;
        $user->password           = Hash::make($request->password);
        $user->name               = $request->name;
        $user->address            = $request->address;
        $user->level              = $request->level;
        $user->phone_number       = $request->phone_number;
        
        try {
            $user->save();
            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'confirmation_code'  => str_random(30).time()
            ]);
            Event::fire(new SendMail($user));
            
            return redirect()->route('user.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return View('admin.user.see',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user   = User::find($id);
        return View('admin.user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user                     = User::find($id);
        $user->username           = $request->username;
        $user->email              = $request->email;
        $user->password           = Hash::make($request->password);
        $user->name               = $request->name;
        $user->address            = $request->address;
        $user->phone_number       = $request->phone_number;

        // if(Auth::user()->id != $id){
        //     $user->level          = $request->level;
        // }
        
        try {
            $user->save();
            return redirect()->route('user.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user             = User::find($id);
        try {
            $user->delete();
            return redirect()->route('user.index'); 
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
