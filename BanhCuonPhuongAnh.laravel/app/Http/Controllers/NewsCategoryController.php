<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\NewsCategoryStoreRequest;
use App\Http\Requests\NewsCategoryUpdateRequest;
use App\News_Category;

class NewsCategoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news_categories = News_Category::paginate(10);
        return View('admin.news.news_categories.index',compact('news_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.news.news_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsCategoryStoreRequest $request)
    {
        $news                     = new News_Category();
        $news->name               = $request->name;
        $news->slug               = $request->slug;
        $news->description        = $request->description;
        $news->meta_title         = $request->meta_title;
        $news->meta_description   = $request->meta_description;
        $news->meta_keyword       = $request->meta_keyword;

        try {
            $news->save();
            return redirect()->route('newscategory.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news_categories = News_Category::all();
        $news_category   = News_Category::find($id);
        return View('admin.news.news_categories.edit',compact('news_categories','news_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsCategoryUpdateRequest $request, $id)
    {
        $news                     = News_Category::find($id);
        $news->name               = $request->name;
        $news->slug               = $request->slug;
        $news->description        = $request->description;
        $news->meta_title         = $request->meta_title;
        $news->meta_description   = $request->meta_description;
        $news->meta_keyword       = $request->meta_keyword;

        try {
            $news->save();
            return redirect()->route('newscategory.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news_category             = News_Category::find($id);
        $news_category->delete();

        foreach($news_category->news as $new)
        {
            $new->delete();
        }
        
        return redirect()->route('newscategory.index');
    }
}
