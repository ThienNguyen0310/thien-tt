<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Post_Category;
use App\Posts;

class PostController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::paginate(10);
        return View('admin.post.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post_categories = Post_Category::all();
        return View('admin.post.post.create',compact('post_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        $p                     = new Posts();
        $p->name               = $request->name;
        $p->slug               = $request->slug;
        $p->post_category_id   = $request->post_category_id;
        $p->description        = $request->description;
        $p->content            = $request->content;
        $p->status             = $request->filled('status');

        try {
            $p->save();
            return redirect()->route('post.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post   = Posts::find($id);
        return View('admin.post.post.see',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post_categories   = Post_Category::all();
        $post              = Posts::find($id);
        return View('admin.post.post.edit',compact('post_categories','post'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, $id)
    {
        $p                     = Posts::find($id);
        $p->name               = $request->name;
        $p->slug               = $request->slug;
        $p->post_category_id   = $request->post_category_id;
        $p->description        = $request->description;
        $p->content            = $request->content;
        $p->status             = $request->filled('status');

        try {
            $p->save();
            return redirect()->route('post.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post             = Posts::find($id);
        try {
            $post->delete();
            return redirect()->route('post.index'); 
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }
}
