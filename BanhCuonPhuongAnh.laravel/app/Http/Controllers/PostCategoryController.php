<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\PostCategoryStoreRequest;
use App\Http\Requests\PostCategoryUpdateRequest;
use App\Post_Category;

class PostCategoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post_categories = Post_Category::paginate(10);
        return View('admin.post.post_categories.index',compact('post_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.post.post_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCategoryStoreRequest $request)
    {
        $pc                     = new Post_Category();
        $pc->name               = $request->name;
        $pc->slug               = $request->slug;
        $pc->description        = $request->description;
        $pc->meta_title         = $request->meta_title;
        $pc->meta_description   = $request->meta_description;
        $pc->meta_keyword       = $request->meta_keyword;

        try {
            $pc->save();
            return redirect()->route('postcategory.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post_category   = Post_Category::find($id);
        return View('admin.post.post_categories.edit',compact('post_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostCategoryUpdateRequest $request, $id)
    {
        $pc                     = Post_Category::find($id);
        $pc->name               = $request->name;
        $pc->slug               = $request->slug;
        $pc->description        = $request->description;
        $pc->meta_title         = $request->meta_title;
        $pc->meta_description   = $request->meta_description;
        $pc->meta_keyword       = $request->meta_keyword;

        try {
            $pc->save();
            return redirect()->route('postcategory.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post_category             = Post_Category::find($id);
        $post_category->delete();

        foreach($post_category->posts as $post)
        {
            $post->delete();
        }
        
        return redirect()->route('postcategory.index');
    }
}
