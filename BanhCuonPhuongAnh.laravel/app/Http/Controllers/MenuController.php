<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\MenuStoreRequest;
use App\Http\Requests\MenuUpdateRequest;
use App\Menu;

class MenuController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::paginate(20);
        return View('admin.other_information.menu.index',compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::all();
        return View('admin.other_information.menu.create',compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuStoreRequest $request)
    {
        $menu                     = new Menu();
        $menu->name               = $request->name;
        $menu->url                = $request->url;
        $menu->parent_id          = $request->parent_id;
        $menu->sort               = $request->sort;
        $menu->status             = $request->filled('status');

        try {
            $menu->save();
            return redirect()->route('menu.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menus  = Menu::all();
        $menu   = Menu::find($id);
        return View('admin.other_information.menu.see',compact('menu','menus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menus           = Menu::all();
        $menu            = Menu::find($id);
        return View('admin.other_information.menu.edit',compact('menus','menu')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuUpdateRequest $request, $id)
    {
        $menu                     = Menu::find($id);
        $menu->name               = $request->name;
        $menu->url                = $request->url;
        $menu->parent_id          = $request->parent_id;
        $menu->sort               = $request->sort;
        $menu->status             = $request->filled('status');

        try {
            $menu->save();
            return redirect()->route('menu.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu             = Menu::find($id);
        $menus            = Menu::all();
        $menu->delete();

        foreach($menus as $m)
        {
            if($m->parent_id == $menu->id)
            {
                $m->delete();
            }
        }

        return redirect()->route('menu.index'); 
    }

    public function order()
    {
        $menus = Menu::all();
        $menu_parent = $menus->where('parent_id',null)->sortBy('sort');

        foreach($menu_parent as $menu){
            $myArray = [];
            foreach($menus as $m){
                if($menu->id == $m->parent_id){
                    array_push($myArray,$m);
                }
            }
            $menu->menu_children=$myArray;
        }
        return View('admin.other_information.menu.sort',compact('menu_parent'));
    }


    public function orderStore(Request $request)
    {

        $orders = json_decode($request->get('orders'),true);
        $this->update_priority_data($orders);
        die();
    }

    public function update_priority_data($data, $parent = NULL) 
    {
        $i = 0;
        foreach ($data as $d) {
            if (array_key_exists("children", $d)) {
               $this->update_priority_data($d['children'], $d['id']);
            } 
            $menu = Menu::find($d['id']);
            $menu->sort = $i;
            $menu->parent_id = $parent;
            $menu->save();
            $i++;
        }
        //return true;
    }
}
