<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductsPhoto;
use App\Product;
use Illuminate\Support\Facades\Storage;
use Image;

class ProductPhotoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return View('Admin.product.product_photo.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        foreach ($request->file as $photo) {
                $imagePath = $photo->store('public/images');
                $image = Image::make(Storage::get($imagePath))->resize(300,300)->encode();
                Storage::put($imagePath,$image);
                $imagePath = explode('/',$imagePath);
                $imagePath = $imagePath[2];

                ProductsPhoto::create([
                    'product_id' => $id,
                    'filename'   => $imagePath,
                ]);
        }
        return redirect()->route('product.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_photo             = ProductsPhoto::find($id);
        try {
            $product_photo->delete();
            return redirect()->route('product.edit',$product_photo->product_id); 
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }

    public function deleteRecord($id){
        $product_photo             = ProductsPhoto::find($id);
        $product_photo->delete();
        $result = route('product.edit',$product_photo->product_id);
        die(json_encode($result)); 
    }
}
