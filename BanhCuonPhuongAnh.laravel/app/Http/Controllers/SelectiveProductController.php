<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SelectiveProduct;

class SelectiveProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $selective_products       = SelectiveProduct::paginate(10);
        return View('admin.product.selective_product.index',compact('selective_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $selective_products=  SelectiveProduct::all();
        
        $ids = $request->get('ids');

        $result = route('selective.index');
        foreach($ids as $id){
            $tmp = 0;
            foreach($selective_products as $selective){
                if($id == $selective->product_id){
                    if($selective->deleted_at){
                        $selective_p=  SelectiveProduct::find($selective->id);
                        $selective_p->deleted_at = null;
                        $selective_p->save();
                    }
                    $tmp = 1;
                    break;
                }
            }
            if(!$tmp){
                $selective_product = new SelectiveProduct();
                $selective_product->product_id = $id;
                $selective_product->save();
            }
        }
        die(json_encode($result));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $selective_product             = SelectiveProduct::where('product_id',$id);
        $selective_product->delete();
        return redirect()->route('selective.index'); 
    }
}
