<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Product_Category;
use App\Product;
use App\Key_Word;
use Illuminate\Support\Facades\Storage;
use Image;
use Hashids\Hashids;
use App\Hot_Product;
use App\Selective_Product;

class ProductController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $product_categories =   Product_Category::all();
        $status             =   $request->status;
        $id                 =   $request->keyword;
        $category_id        =   $request->category_id;
        $action             =   $request->action;

        if($status||($id && $action == 'id')||$category_id){

            // $products = new Product();
            // if($status){
            //     $product = Product::where('status',$status)->get();
            //     $products = $product;
            //     if($id&& $action == 'id'){
            //         $product->where('id',$id);
            //         $products = $product;
            //         if($category_id){
            //             $product->where('product_category_id',$category_id);
            //             $products = $product;
            //         }
            //     }
            //     else{
            //         if($category_id){
            //             $product->where('product_category_id',$category_id);
            //             $products = $product;
            //         }
            //     }
            // }
            // else{
            //     if($id&& $action == 'id'){
            //         $product = Product::where('id',$id)->get();
            //         if($category_id){
            //             $product->where('product_category_id',$category_id);
            //             $products = $product;
            //         }
            //     }
            //     else{
            //         if($category_id){
            //             $product->where('product_category_id',$category_id)->get();
            //             $products = $product;
            //         }
            //     }
            // }


            if($category_id && ($id && $action == 'id') && $status){
                $products = Product::where('status',$status)->where('product_category_id',$category_id)->where('id',$id)->get();
            }
            elseif($status && ($id && $action == 'id')){
                $products = Product::where('status',$status)->where('id',$id)->get();
            }
            elseif($category_id && ($id && $action == 'id')){
                $products = Product::where('id',$id)->where('product_category_id',$category_id)->get();
            }
            elseif($status && $category_id){
                $products = Product::where('status',$status)->where('product_category_id',$category_id)->get();
            }
            elseif($id && $action == 'id'){
                $products = Product::where('id',$id)->get();
            }
            elseif($category_id){
                $products = Product::where('product_category_id',$category_id)->get();
            }else{
                $products = Product::where('status',$status)->get();
            }
            
            return View('admin.product.product.index',compact('products','product_categories','status','id','category_id','action'));
        }

        $products    = Product::all();
        $status             =   "";
        $id                 =   "";
        $category_id        =   "";
        return View('admin.product.product.index',compact('products','product_categories','status','id','category_id','action'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_categories = Product_Category::all();
        $key_words          = Key_Word::all();
        return View('admin.product.product.create',compact('product_categories','key_words'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $product                      = new Product();
        $product->name                = $request->name;
        $product->slug                = $request->slug;
        $product->content             = $request->content;
        $product->description         = $request->description;
        $product->product_category_id = $request->product_category_id;
        $product->price               = $request->price;
        $product->meta_title          = $request->meta_title;
        $product->meta_description    = $request->meta_description;
        $product->meta_keyword        = $request->meta_keyword;
        $product->status              = $request->filled('status');

        try {
            
            $product->save();
            // $hashids = new Hashids('', 6);
            // $hashids->encode(6));
            if($request->key_words){ 
                foreach ($request->key_words as $key_word) {
                    $product->key_words()->attach($key_word);
                }
            }
            return redirect()->route('product.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product_categories = Product_Category::all();
        $product            = Product::find($id);
        $key_words          = Key_Word::all();
        return View('admin.product.product.see',compact('product_categories','product','key_words'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product            = Product::find($id);
        $product_categories = Product_Category::all();
        $key_words          = Key_Word::all();
        return View('admin.product.product.edit',compact('product','product_categories','key_words'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        $product                      = Product::find($id);
        $product->name                = $request->name;
        $product->slug                = $request->slug;
        $product->content             = $request->content;
        $product->description         = $request->description;
        $product->product_category_id = $request->product_category_id;
        $product->price               = $request->price;
        $product->meta_title          = $request->meta_title;
        $product->meta_description    = $request->meta_description;
        $product->meta_keyword        = $request->meta_keyword;
        $product->status              = $request->filled('status');
        $hashids = new Hashids('', 6);
        $product->hashids = $hashids->encode($product->id);
        if ($request->hasFile('image')){
            $imagePath = $request->file('image')->store('public/images');
            $image = Image::make(Storage::get($imagePath))->resize(300,300)->encode();
            Storage::put($imagePath,$image);
            $imagePath = explode('/',$imagePath);
            $imagePath = $imagePath[2];
            $product->image = $imagePath;
        }
        try {
            $product->save();
            if($request->key_words){ 
                $product->key_words()->detach();
                foreach ($request->key_words as $key_word) {
                    $product->key_words()->attach($key_word);
                }
            }
            return redirect()->route('product.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product             = Product::find($id);
        $product->delete();

        $product->key_words()->detach();

        foreach($product->products_photos as $products_photo)
        {
            $products_photo->delete();
        }

        if($product->hot_product)
        {
            $product->hot_product->delete();
        }elseif($product->selective_product){
            $product->selective_product->delete();
        }
        
        return redirect()->route('product.index'); 
    }
}
