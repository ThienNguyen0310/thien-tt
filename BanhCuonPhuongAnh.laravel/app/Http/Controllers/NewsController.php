<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\NewsStoreRequest;
use App\Http\Requests\NewsUpdateRequest;
use App\News_Category;
use App\News;
use Illuminate\Support\Facades\Storage;
use Image;
use Hashids\Hashids;

class NewsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::paginate(10);
        return View('admin.news.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $news_categories = News_Category::all();
        return View('admin.news.news.create',compact('news_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsStoreRequest $request)
    {
        $news                     = new News();
        $news->name               = $request->name;
        $news->slug               = $request->slug;
        $news->news_category_id   = $request->news_category_id;
        $news->description        = $request->description;
        $news->content            = $request->content;
        $news->status             = $request->filled('status');
        try {
            $news->save();
            return redirect()->route('news.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news   = News::find($id);
        return View('admin.news.news.see',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news_categories   = News_Category::all();
        $news              = News::find($id);
        return View('admin.news.news.edit',compact('news','news_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsUpdateRequest $request, $id)
    {
        $news                     = News::find($id);
        $news->name               = $request->name;
        $news->slug               = $request->slug;
        $news->news_category_id   = $request->news_category_id;
        $news->description        = $request->description;
        $news->content            = $request->content;
        $news->status             = $request->filled('status');
        $hashids = new Hashids('', 4);
        $news->hashids =  $hashids->encode($news->id);


        if ($request->hasFile('image')){
            $imagePath = $request->file('image')->store('public/images');
            $image = Image::make(Storage::get($imagePath))->resize(300,300)->encode();
            Storage::put($imagePath,$image);
            $imagePath = explode('/',$imagePath);
            $imagePath = $imagePath[2];
            $news->image = $imagePath;
        }

        try {
            $news->save();
            return redirect()->route('news.index');  
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news             = News::find($id);
        try {
            $news->delete();
            return redirect()->route('news.index'); 
        } catch (Exception $e) {
            die($e->getMessage());
        }    
    }
}
