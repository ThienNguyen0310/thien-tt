<?php 
	define('ROOT', dirname(__DIR__).DIRECTORY_SEPARATOR);
	// C:\xampp\htdocs\project_demo\
	define('APP', ROOT.'app'.DIRECTORY_SEPARATOR);

	require APP . 'core/App.php';
	require APP . 'core/Database.php';
	require APP . 'core/Route.php';
	require APP . 'core/View.php';
	require APP . 'core/Helper.php';

	require APP . 'models/Department.php';
	require APP . 'models/User.php';

	$app = new App();
 ?>