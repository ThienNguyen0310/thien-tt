$(document).ready(function(){
	function validateEmail(value,element,param){
		re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if(re.test(value)){
			return true;
		}else{
			return false;
		}
	}
	jQuery.validator.addMethod('checkEmail',validateEmail,'Please enter a valid email address');

	function validatePassword(value,element,param){
		if(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(value)){
			return true;
		}else{
			return false;
		}
	}
	jQuery.validator.addMethod('checkPassword',validatePassword,'Please enter a valid password');

	function validateImage(value,element,param){
		var fuData = document.getElementById('fileChooser');
        var FileUploadPath = fuData.value;
        if (FileUploadPath == '') {
            return false;
		} else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
			if (Extension == "gif" || Extension == "png" || Extension == "bmp"|| Extension == "jpeg" || Extension == "jpg") {
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(fuData.files[0]);
                }
            }
			else{
                return false;
            }
        }
        return true;
	}
	jQuery.validator.addMethod('checkImage',validateImage,'Please enter a valid image');

	$("form[name = 'register']").validate(
		{
			debug: true,

			rules: {
				username: {
					required: true,
					minlength: 6
				},
				fullname: {
					required: true,
					minlength: 6
				},
				email: {
					required: true,
					minlength: 6,
					email: true,
					checkEmail: true
				},
				password: {
					required: true,
					minlength:8,
					checkPassword: true
				},
				confirmPassword: {
					required: true,
					minlength:8,
					checkPassword: true,
					equalTo: "#password"
				}
			},
			messages: {
			    username: {
			    	required: "Please specify your name",
			    	minlength: "Name's length must greater than 6 character"
				},
				fullname: {
			    	required: "Please specify your fullname",
			    	minlength: "fullame's length must greater than 6 character"
				},
			    email: {
			      	required: "We need your email address to contact you",
			      	email: "Your email address must be in the format of name@domain.com"
			    },
			    password: {
			      	required: "Please specify your password",
			      	checkPassword: "Passwords include at least 1 character and a number and a character flower",
			      	minlength: "Password's length must greater than 6 character"
				},
				confirmPassword: {
			      	required: "Please specify your confirm password",
			      	equalTo: "Password confirm does not match password"
				}
			},
			errorPlacement: function(error, element) {
				var elementName = element.attr("name");
				error.appendTo($("#" + elementName + "Error"));
				error.addClass('alert alert-danger');
			},
			errorElement: 'div',
			submitHandler: function(form) {
      			form.submit();
      		}
		}
	);
	$('#create_user, #edit_user').validate(
		{
			debug: true,

			rules: {
				username: {
					required: true,
					minlength: 6
				},
				fullname: {
					required: true,
					minlength: 6
				},
				email: {
					required: true,
					minlength: 6,
					email: true,
					checkEmail: true
				},
				password: {
					required: true,
					minlength:8,
					checkPassword: true
				},
				confirmPassword: {
					required: true,
					minlength:8,
					checkPassword: true,
					equalTo: "#password"
				},
				image: {
					checkImage: true
				}
			},
			messages: {
			    username: {
			    	required: "Please specify your name",
			    	minlength: "Name's length must greater than 6 character"
				},
				fullname: {
			    	required: "Please specify your fullname",
			    	minlength: "fullame's length must greater than 6 character"
				},
			    email: {
			      	required: "We need your email address to contact you",
			      	email: "Your email address must be in the format of name@domain.com"
			    },
			    password: {
			      	required: "Please specify your password",
			      	checkPassword: "Passwords include at least 1 character and a number and a character flower",
			      	minlength: "Password's length must greater than 8 character"
				},
				confirmPassword: {
			      	required: "Please specify your confirm password",
			      	equalTo: "Password confirm does not match password"
				},
				image: {
					checkImage: "Photo only allows file types of GIF, PNG, JPG, JPEG and BMP."
				}
			
			},
			errorPlacement: function(error, element) {
				var elementName = element.attr("name");
				error.appendTo($("#" + elementName + "Error"));
				error.addClass('alert alert-danger');
			},
			errorElement: 'div',
			submitHandler: function(form) {
      			form.submit();
      		}
		}
	);
	$('#create_dep, #edit_dep').validate(
		{
			debug: true,

			rules: {
				name: {
					required: true,
					minlength: 6,
				}
				
			},
			messages: {
			    name: {
			    	required: "Please specify your name",
			    	minlength: "Name's length must greater than 6 character",
				}
			},
			errorPlacement: function(error, element) {
				var elementName = element.attr("name");
				error.appendTo($("#" + elementName + "Error"));
				error.addClass('alert alert-danger');
			},
			errorElement: 'div',
			submitHandler: function(form) {
      			form.submit();
      		}
		}
	);
	$('#update_user').validate(
		{
			debug: true,

			rules: {
				fullname: {
					required: true,
					minlength: 6
				},
				password: {
					required: true,
					minlength:8,
					checkPassword: true
				},
				image: {
					checkImage: true
				}
			},
			messages: {
				fullname: {
			    	required: "Please specify your fullname",
			    	minlength: "fullame's length must greater than 6 character"
				},
			    password: {
			      	required: "Please specify your password",
			      	checkPassword: "Passwords include at least 1 character and a number and a character flower",
			      	minlength: "Password's length must greater than 8 character"
				},
				image: {
					checkImage: "Photo only allows file types of GIF, PNG, JPG, JPEG and BMP."
				}
			
			},
			errorPlacement: function(error, element) {
				var elementName = element.attr("name");
				error.appendTo($("#" + elementName + "Error"));
				error.addClass('alert alert-danger');
			},
			errorElement: 'div',
			submitHandler: function(form) {
      			form.submit();
      		}
		}
	);
	$('#reset_pass').validate(
		{
			debug: true,

			rules: {
				password: {
					required: true,
					minlength:8,
					checkPassword: true
				},
				confirmPassword: {
					required: true,
					minlength:8,
					checkPassword: true,
					equalTo: "#password"
				}
			},
			messages: {
			    password: {
			      	required: "Please specify your password",
			      	checkPassword: "Passwords include at least 1 character and a number and a character flower",
			      	minlength: "Password's length must greater than 8 character"
				},
				confirmPassword: {
			      	required: "Please specify your confirm password",
			      	equalTo: "Password confirm does not match password"
				}
			},
			errorPlacement: function(error, element) {
				var elementName = element.attr("name");
				error.appendTo($("#" + elementName + "Error"));
				error.addClass('alert alert-danger');
			},
			errorElement: 'div',
			submitHandler: function(form) {
      			form.submit();
      		}
		}
	);

	$('#forgot_pass').validate(
		{
			debug: true,

			rules: {
				email: {
					required: true,
					minlength: 6,
					email: true,
					checkEmail: true
				}
			},
			messages: {
			    email: {
			      	required: "We need your email address to contact you",
			      	email: "Your email address must be in the format of name@domain.com"
			    }
			},
			errorPlacement: function(error, element) {
				var elementName = element.attr("name");
				error.appendTo($("#" + elementName + "Error"));
				error.addClass('alert alert-danger');
			},
			errorElement: 'div',
			submitHandler: function(form) {
      			form.submit();
      		}
		}
	);

})
