<!-- Start Header -->
<?php 
  require ROOT . 'resources/views/admin/layouts/header.php';

  $paginationHTML= '';
   if($totalPage > 1){
       $start = "";
       $prev  = "";
       if($currentPage > 1){
           $start = "<li><a href=" . location('usercontroller/index/1') . ">Start</a></li>";
           $prev_currentPage = $currentPage - 1;
           $prev = "<li><a href=" . location("usercontroller/index/$prev_currentPage") . ">Previous</a></li>";
       }
        $next = "";
        $end  = "";
        if($currentPage < $totalPage){
           $next_currentPage = $currentPage + 1;
           $next = "<li><a href=" . location("usercontroller/index/$next_currentPage") . ">Next</a></li>";
           $end = "<li><a href=" . location("usercontroller/index/$totalPage") . ">End</a></li>";
        }       
        if($pageRange < $totalPage){
           if(($currentPage == 1) || ( $pageRange> (2*$currentPage)) ){
               $startPage = 1;
               $endPage   = $pageRange;
           }else if($currentPage > ($totalPage - floor($pageRange/2))){
               $startPage = $totalPage - $pageRange + 1;
               $endPage   = $totalPage;
           }else{
                $startPage = $currentPage - floor(($pageRange - 1)/2);
                $endPage   = $currentPage + ceil(($pageRange - 1)/2);
           }
        }else{
             $startPage   = 1;
             $endPage     = $totalPage;
           }
        $listPages="";
        for($i = $startPage ; $i <= $endPage ; $i++){
           if($i == $currentPage){
               $listPages .= " <li class='active'><a href=" . location("usercontroller/index/$i") . ">".$i."</a></li>";
           }
           else{
               $listPages .="<li><a href=" . location("usercontroller/index/$i") . ">" .$i."</a></li>";
          }
        }
       $paginationHTML = "<ul class = 'pagination'>" . $start .$prev .$listPages . $next . $end . "</ul>";
    } 
    ?>
<!-- Start Content -->  
  <div class="content container-fluid">
    <h2>List Users</h2>
    <div class="control clear-fix">
      <a class="btn btn-primary create" href="<?php echo location('usercontroller/create') ?>">Create</a>
      <!-- <a class="btn btn-warning search" href="#">Search</a>
      <input class="input-search" type="text" name="search" placeholder="Enter keyword to search"> -->
    </div>
    <table class="table table-stripped">
      <tbody>
        <tr>
          <th>Id</th>
          <th>Username</th>
          <th>Password</th>
          <th>Fullname</th>
          <th>Department</th>
          <th>Email</th>
          <th>Role</th>
          <th>Image</th>
          <th>Created at</th>
          <th>Updated at</th>
          

          <th></th>
          <th></th>
        </tr>
        <?php foreach($users as $us){ ?>
          <tr>
          <td><?php echo $us->id ?></td>
          <td><?php echo $us->username ?></td>
          <td><?php echo $us->password ?></td>
          <td><?php echo $us->fullname ?></td>
          <td><?php echo $us->department->name ?></td>
          <td><?php echo $us->email ?></td>
          <td><?php 
              if(!$us->level){
                echo 'Member';
              }
              else{
                echo 'Admin';
              }
           ?></td>
          <td><img src="<?php echo asset($us->image)?>"></td>
          <td><?php echo $us->created_at ?></td>
          <td><?php echo $us->updated_at ?></td>

          <th><a class="btn btn-success" href="<?php echo location("usercontroller/edit/$us->id") ?>">Edit</a></th>
          <th>
            <form method="POST" action="<?php echo location("usercontroller/delete/$us->id")?>" accept-charset="UTF-8">
            <button class="btn btn-danger">Delete</button>
            </form>
          </th>
        </tr>
         <?php }?>
      </tbody>
    </table>

    <!-- pagingation -->
    <div class="fix_posision" style=" bottom: 10px;float: left">
    <nav aria-label="Page navigation" >
      <ul class="pagination">
        <?php echo $paginationHTML ?>
      </ul>
    </nav>
  </div>

<!-- Start Footer -->
<?php require ROOT . 'resources/views/admin/layouts/footer.php' ?>
<!-- End Footer -->
<script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
  </script>
</body>
</html>
