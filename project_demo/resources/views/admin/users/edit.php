<!-- Start Header -->
<?php 
  require ROOT . 'resources/views/admin/layouts/header.php';
 ?>

	<div class="content container">
		<form method="post" action="<?php echo location('usercontroller/update/' . $user->id) ?>" enctype="multipart/form-data" autocomplete="off" id = "edit_user">
			<div class="row">
				<div class="title col-md-3"><h3>Edit users</h3></div>
				<div class="nav-function col-md-3 col-md-offset-6">
					<h3>
						<button id= "btn-submit" type="submit" class="btn btn-primary" style="float: right; margin-left: 5px">Save</button>
						<a href="<?php echo location('usercontroller/index/1') ?>" class="btn btn-warning" style="float: right;">Back</a>
					</h3>
				</div>
			</div>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="infor">
					<div class="form-group">
						<label for="username">Username</label>
						<input type="text" required autocomplete="off" class="form-control" placeholder="Username" name="username" id="username" value="<?php echo $user->username ?>">
						<div id="usernameError"></div>
						<div id="usernameError1"></div>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" required autocomplete="off" class="form-control" placeholder="Password" id = "password" name="password" value="<?php echo $user->password ?>">
						<div id="passwordError"></div>
					</div>
					<div class="form-group">
						<label for="fullname">Fullname</label>
						<input type="text" required autocomplete="off" class="form-control" placeholder="Fullname" name="fullname" value="<?php echo $user->fullname ?>">
						<div id="fullnameError"></div>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="text" required autocomplete="off" class="form-control" placeholder="email" name="email" id="name" value="<?php echo $user->email ?>">
						<div id="emailError"></div>
						<div id="emailError1"></div>
					</div>
					<div class="form-group">
						<label for="level">Level</label>
						<select name="level" class="form-control">
							<?php if(!$user->level){ ?>
								<option value='0' selected="selected">Member</option>
								<option value='1'>Admin</option>
							 
							<?php }else{ ?>
								<option value='0' >Member</option>
								<option value='1' selected="selected">Admin</option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label for="department">Department</label>
						<select name="department_id" class="form-control">
							<?php 
								foreach($department as $dep){
									if($dep->id == $users->department_id){ 
							?>
										<option value="<?php echo $dep->id ?>" selected><?php echo $dep->name?></option>
									<?php }else{ ?>
										<option value="<?php echo  $dep->id?>"><?php echo $dep->name?></option>
									<?php }
								} 
							?>
			
						</select>
					</div>
					<div class="form-group">
						<label for="image">Image</label>
	
							<input type="file" name="image" class="form-file-control" id ="fileChooser" onchange="previewFile();">
							<img id = "preview" src="<?php echo asset($user->image)?>"  width  ="300px">
							<div id="imageError"></div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<!-- Start Footer -->
<?php require ROOT . 'resources/views/admin/layouts/footer.php' ?>
<!-- End Footer -->
<script type="text/javascript" src="<?php echo asset('js/ckeditor.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('js/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('js/validate.js')?>"></script>
<script type="text/javascript">
	CKEDITOR.replace('content');

	function previewFile(){
		var preview = $('#preview');//chọn vị trí ảnh sẽ hiển thị
		var file = $('input[type=file]').prop('files')[0];
		//console.log(hello);
		var reader = new FileReader();

		if(file){
			reader.readAsDataURL(file);//đọc trong tệp dươí dạng hình ảnh ,Thuộc tính kết quả sẽ chứa dữ liệu của tệp được mã hoá dưới dạng URL dữ liệu.
		}else{
			preview.attr('src', '');//lấy giá trị thuộc tính src
		}
		reader.onloadend = function() {
			preview.attr('src', reader.result);//result trả lại nội dung của tệp
		}
	}

	// $('#username, #email').keyup(function(){
 //        html='';
 //        $('#usernameError1').html(html);
 //        $("#usernameError1").removeClass('alert alert-danger');
 //        $('#emailError1').html(html);
 //        $("#emailError1").removeClass('alert alert-danger');
 //    });

 //    $('#btn-submit').click(function (){
 //        $('#usernameError1').html('');
 //        $('#emailError1').html('');
 //        var username = $('#username').val();
 //        var email = $('#email').val();
 //        $.ajax({
 //            url : 'index.php?url=UserController/checkNameEmailEdit',
 //            type : 'post',
 //            dataType : 'json',
 //            data : {
 //                username : username,
 //                email : email,      
 //            },
 //            success : function (result){
 //              console.log(result.message1);
 //              console.log(result.message2);
 //                var html1 = '';
 //                var html2 = '';
 //                if (result.error == true){
 //                    html1 += result.message1 ;
 //                    html2 += result.message2 ;
 //                }

 //                if (html1 != ''|| html2 !=''){
 //                  if(html1 != ''){
 //                      $('#usernameError1').append(html1);
 //                      $('#usernameError1').addClass('alert alert-danger');
 //                  }
 //                    if (html2 != ''){
 //                      $('#emailError1').append(html2);
 //                      $('#emailError1').addClass('alert alert-danger');
 //                  }
 //                }
                 
 //                else {
 //                    $('form').submit();
 //                }
 //            }
 //        });
 //        return false;
 //    });

</script>
	
</body>
</html>