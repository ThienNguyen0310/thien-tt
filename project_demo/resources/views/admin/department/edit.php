<!-- Start Header -->
<?php require_once ROOT . '/resources/views/admin/layouts/header.php' ?>
<!-- End Header -->

<!-- Start Content -->
	<div class="content container">
		<form id="edit_dep" autocomplete="off" method="post" action="<?php echo location("departmentcontroller/update/$department->id") ?>" >
			<div class="row">
				<div class="Name col-md-3"><h3>Edit Department</h3></div>
				<div class="nav-function col-md-3 col-md-offset-6">
					<h3>
						<button type="submit" class="btn btn-primary" style="float: right; margin-left: 5px">Save</button>
						<a href="<?php echo location('departmentcontroller/index/1') ?>" class="btn btn-warning" style="float: right;">Back</a>
					</h3>
				</div>
			</div>
			<div class="form-group">
		    	<label for="name">Name</label>
		    	<input type="text" required autocomplete="off" class="form-control" name="name" placeholder="Name" value="<?php echo $department->name ?>">
		    	<div id="nameError"></div>
		  	</div>
		  	
		</form>
	</div>
	<!-- End Content -->

	<!-- Start Footer -->
	<?php require_once ROOT . '/resources/views/admin/layouts/footer.php' ?>
	<!-- End Footer -->

	<script type="text/javascript" src="<?php echo asset('js/jquery.validate.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('js/validate.js')?>"></script>
	
</body>
</html>