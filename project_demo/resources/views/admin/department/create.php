<?php require ROOT . 'resources/views/admin/layouts/header.php' ?>

	<div class="content container">
	<!-- nhấn save sẽ gọi đến store trong action -->
		<form id="create_dep" method="post" autocomplete="off" action="<?php  echo location('departmentcontroller/store') ?>" >
			<div class="row">
				<div class="title col-md-3"><h3>Create Department</h3></div>
				<div class="nav-function col-md-3 col-md-offset-6">
					<h3>
						<button id="btn-submit" class="btn btn-primary" style="float: right; margin-left: 5px">Save</button>
						<a href="<?php echo location('departmentcontroller/index/1') ?>" class="btn btn-warning" style="float: right;">Back</a>
					</h3>
				</div>
			</div>
			<div class="form-group">
		    	<label for="name">Name</label>
		    	<input type="text" id="name" required autocomplete="off" class="form-control" name="name" placeholder="Name">
				<div id="nameError"></div>
				<div id="nameError1"></div>	  	
			</div>
		</form>
	</div>
<!-- Start Footer -->
<?php require_once ROOT . '/resources/views/admin/layouts/footer.php' ?>
<!-- End Footer -->

	<script type="text/javascript" src="<?php echo asset('js/jquery.validate.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('js/validate.js')?>"></script>
	<!-- <script language="javascript" src="http://code.jquery.com/jquery-2.0.0.min.js"></script> -->
	<script type="text/javascript">
		$('#name').keyup(function(){
		    html='';
		    $('#nameError1').html(html);
		    $("#nameError1").removeClass('alert alert-danger');
		});

		$('#btn-submit').click(function (){
	    	$('#nameError1').html('');
	    	var name = $('#name').val();
	   		$.ajax({
	        	url : 'index.php?url=DepartmentController/checkName',
	        	type : 'post',
	        	dataType : 'json',
	        	data : {
	            	name : name,      
	        	},
	        	success : function (result){
		            var html = '';
		            if (result.error == true){
		                html += result.message;
		            }

		            if (html != ''){
		                $('#nameError1').append(html);
		                $('#nameError1').addClass('alert alert-danger');
		            }
		            else {
		                $('form').submit();
		            }
		        }
		    });
	    	return false;
		});
	</script>
</body>
</html>
	