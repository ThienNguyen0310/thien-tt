<!-- Start Header -->
<?php 
  require ROOT . 'resources/views/admin/layouts/header.php';
  
  $paginationHTML= '';
   if($totalPage > 1){
       // $start = '<li>Start</li>';
       // $prev =  '<li>Previous</li>';
       $start = "";
       $prev  = "";
       if($currentPage > 1){
           $start = "<li><a href=" . location('departmentcontroller/index/1') . ">Start</a></li>";
           $prev_currentPage = $currentPage - 1;
           $prev = "<li><a href=" . location("departmentcontroller/index/$prev_currentPage") . ">Previous</a></li>";
          // var_dump($prev); die();
       }
        // $next = '<li>Next</li>';
        // $end = '<li>End</li>';
        $next = "";
        $end  = "";
        if($currentPage < $totalPage){
           $next_currentPage = $currentPage + 1;
           $next = "<li><a href=" . location("departmentcontroller/index/$next_currentPage") . ">Next</a></li>";
           $end = "<li><a href=" . location("departmentcontroller/index/$totalPage") . ">End</a></li>";
        }       
        if($pageRange < $totalPage){
           if(($currentPage == 1) || ( $pageRange> (2*$currentPage)) ){
               $startPage = 1;
               $endPage   = $pageRange;
           }else if($currentPage > ($totalPage - floor($pageRange/2))){
               $startPage = $totalPage - $pageRange + 1;
               $endPage   = $totalPage;
           }else{
                $startPage = $currentPage - floor(($pageRange - 1)/2);
                $endPage   = $currentPage + ceil(($pageRange - 1)/2);
           }
        }else{
             $startPage   = 1;
             $endPage     = $totalPage;
           }
        $listPages="";
        for($i = $startPage ; $i <= $endPage ; $i++){
           if($i == $currentPage){
               $listPages .= " <li class='active'><a href=" . location("departmentcontroller/index/$i") . ">".$i."</a></li>";
           }
           else{
               $listPages .="<li><a href=" . location("departmentcontroller/index/$i") . ">" .$i."</a></li>";
          }
        }
       $paginationHTML = "<ul class = 'pagination'>" . $start .$prev .$listPages . $next . $end . "</ul>";
    } 
    ?>
<!-- Start Content --> 
  <div class="content container-fluid">
    <h2>List Department</h2>
    <div class="control clear-fix">
      <a class="btn btn-primary create" href="<?php echo location('departmentcontroller/create') ?>">Create</a>
      <!-- <a class="btn btn-warning search" href="#">Search</a>
      <input class="input-search" type="text" name="search" placeholder="Enter keyword to search"> -->
    </div>
    <table class="table table-stripped">
      <tbody>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Created at</th>
          <th>updated at</th>
          <th></th>
          <th></th>
        </tr>
        <?php foreach($department as $dep){ ?>
          <tr>
          <td><?php echo $dep->id ?></td>
          <td><?php echo $dep->name ?></td>
          <td><?php echo $dep->created_at ?></td>
          <td><?php echo $dep->updated_at ?></td>
          <td>
            <a href="<?php echo location("departmentcontroller/edit/$dep->id") ?>" class="btn btn-success btn-mini pull-left">Edit</a>
          </td>
          <td>
              <form method="POST" action="<?php echo location("departmentcontroller/delete/$dep->id") ?>" accept-charset="UTF-8">
              <button type="submit" class="btnDelete btn btn-danger btn-mini">Delete</button>
              </form>
          </td>
          </tr>
        <?php }?>
      </tbody>
    </table>

    <div id= "pagination" style="position: fixed;bottom: 10px; float:right">
        <?php echo $paginationHTML ?>
    </div>
  </div>

<!-- Start Footer -->
<?php require ROOT . 'resources/views/admin/layouts/footer.php' ?>
<!-- End Footer -->
  <script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
  </script>
</body>
</html>