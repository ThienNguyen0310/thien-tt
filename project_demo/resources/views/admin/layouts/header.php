<!DOCTYPE html>
<html>
<head>
  <title>Admin</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/stylesheet.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo asset('css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo asset('css/font-awesome.min.css') ?>">
</head>
<body>
  <div class="header container-fluid">
    <div class="header-top row">
      <button class="btn btn-success" style="float:right;margin-right: 40px"  ><a style="color: white" href="<?php echo location('HomepageController/logout') ?>">Logout</a></button>
    </div>
    <div class="main-menu">
      <nav class="navbar navbar-inverse">
        <a class="navbar-brand" href="<?php echo location('DepartmentController/index/1') ?>">Department</a>
        <a class="navbar-brand" href="<?php echo location('UserController/index/1') ?>">Users</a>
        
      </nav>
    </div>
  </div>  