<!DOCTYPE html>
<html>
<head>
  <title>Reset Your Password</title>
  <link rel="stylesheet" type="text/css" href="<?php echo asset('css/bootstrap.min.css') ?>">
</head>

<body>  
  <div class="col-md-5 col-md-offset-3">
    <h1>Reset Your Password</h1>
    <form action="<?php echo location('HomepageController/forgot_processing')?>" method="post" autocomplete="off" id="forgot_pass">
      <div class="form-group">
              <label>Email</label>
              <input type="email" name="email" class="form-control" id="email" required autocomplete="off" id="email">
              <div id="emailError"></div>
              <div id="emailError1"></div>
        </div>
      <button id="btn-submit" class="button button-block"/>Reset</button>
    </form>
  </div>
          
<script type="text/javascript" src="<?php echo asset('js/jquery-3.2.1.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo asset('js/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('js/validate.js')?>"></script>
  <script type="text/javascript">
    $('#email').keyup(function(){
        html='';
        $('#emailError1').html(html);
        $("#emailError1").removeClass('alert alert-danger');
    });

    $('#btn-submit').click(function (){
        $('#emailError1').html('');
        var email = $('#email').val();
        $.ajax({
            url : 'index.php?url=HomepageController/checkReset',
            type : 'post',
            dataType : 'json',
            data : {
                email : email,      
            },
            success : function (result){
                var html1 = '';
                if (result.error == true){
                    html1 += result.message1 ;
                }

                if (html1 != ''){
                    $('#emailError1').append(html1);
                    $('#emailError1').addClass('alert alert-danger');
                }  
                else {
                    $('form').submit();
                }
            }
        });
        return false;
    });

  </script>
</body>