<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sign-UP/login Form</title>
	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/bootstrap.min.css') ?>">
</head>

<body>
  
    <div class="col-md-5 col-md-offset-3">
      <h1>Wellcom Back</h1>
      <form method="post" action="<?php  echo location('HomepageController/login_processing') ?>" autocomplete="off">
          <div id="loginError" ></div>
          <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" class="form-control" id="username" required autocomplete="off" value="<?php if(isset($_COOKIE['username'])){echo $_COOKIE['username'];} ?>">
              <!-- <div id="usernameError"></div> -->
          </div>
          <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" id="password" required autocomplete="off" value="<?php if(isset($_COOKIE['password'])){echo $_COOKIE['password'];} ?>">
              <!-- <div id="passwordError"></div> -->
          </div>
          <div class="form-group custom-control custom-checkbox">
              <label class="custom-control-label" for="rememberMe">Remember me </label>
              <input type="checkbox" class="custom-control-input" name="remember" autocomplete="off" value=" <?php if(isset($_COOKIE['username'])){echo 'checked';} ?>">
          </div>
          <p class="forgot"><a style="float:right" href="<?php echo location('HomepageController/forgot')?>">Forgot Password?</a></p>
          <button id="btn-submit" class="btn btn-success">Login</button>
          <p class="register">If you do not have an account then register here!  <a class="btn btn-success" href="<?php echo location('HomepageController/register')?>">Register</a></p>
      </form>
    </div>
    <script type="text/javascript" src="<?php echo asset('js/jquery-3.2.1.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo asset('js/jquery.validate.min.js')?>"></script>
  <script type="text/javascript">
    $('#username, #password').keyup(function(){
        html='';
        $('#loginError').html(html);
        $("#loginError").removeClass('alert alert-danger');
    });

    $('#btn-submit').click(function (){
        $('#loginError').html('');
        var username = $('#username').val();
        var password = $('#password').val();
        
        $.ajax({
            url : 'index.php?url=HomepageController/checkLogin',
            type : 'post',
            dataType : 'json',
            data : {
                username : username,
                password : password,      
            },
            success : function (result){
                var html1 = '';
                if (result.error == true){
                    html1 += result.message1 ;
                }

                if (html1 != ''){
                    $('#loginError').append(html1);
                    $('#loginError').addClass('alert alert-danger');
                }             
                else {
                    $('form').submit();
                }
            }
        });
        return false;
    });

  </script>
</body>
</html>