 <!-- <?php 
	//session_start();
 ?> -->
 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Error</title>
 	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
 	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
 	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/style.css') ?>">
 </head>
 <body>
 	<div class = "form">
 		<h1>Error</h1>
 		<p>
 			<?php 
 				if(isset($_SESSION['message']) AND !empty($_SESSION['message'])){
 					echo $_SESSION['message'];
 				}
 				else{
 					header('location:'.location('HomepageController/login'));
 				}
 			 ?>
 		</p>
 		<a href="<?php echo location('HomepageController/login') ?>"><button class = "button button-block">Home</button></a>
 	</div>
 </body>
 </html>