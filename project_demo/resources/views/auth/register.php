<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sign-UP</title>
	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/bootstrap.min.css') ?>">
</head>

<body>
  <div class="col-md-5 col-md-offset-3">
    <h1>Register</h1>
    <form method="post" action="<?php  echo location('HomepageController/register_processing') ?>"  autocomplete="off" name="register">
        <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" class="form-control" id="username" required autocomplete="off" id="username">
              <div id="usernameError"></div>
              <div id="usernameError1"></div>
        </div>
        <div class="form-group">
              <label>Fullname</label>
              <input type="text" name="fullname" class="form-control" id="fullname" required autocomplete="off" >
              <div id="fullnameError"></div>
        </div>
        <div class="form-group">
              <label>Email</label>
              <input type="email" name="email" class="form-control" id="email" required autocomplete="off" id="email">
              <div id="emailError"></div>
              <div id="emailError1"></div>
        </div>
        <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" id="password" required autocomplete="off">
              <div id="passwordError"></div>
        </div>
        <div class="form-group">
              <label>Confirm Password</label>
              <input type="password" name="confirmPassword" class="form-control" id="confirm-password" required autocomplete="off">
              <div id="confirmPasswordError"></div>
        </div>
        <div class="form-group">
              <label>Department</label>
              <select name="department_id">
                  <?php 
                      foreach ($department as $dep) {
                        echo "<option value='$dep->id' >$dep->name</option>";
                      }
                  ?>  
              </select>
        </div>
        <button id="btn-submit" type="submit" class="btn btn-primary" >Register</button>
    </form>
  </div>
  <script type="text/javascript" src="<?php echo asset('js/jquery-3.2.1.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo asset('js/jquery.validate.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('js/validate.js')?>"></script>
  <script type="text/javascript">
    $('#username, #email').keyup(function(){
        html='';
        $('#usernameError1').html(html);
        $("#usernameError1").removeClass('alert alert-danger');
        $('#emailError1').html(html);
        $("#emailError1").removeClass('alert alert-danger');
    });

    $('#btn-submit').click(function (){
        $('#usernameError1').html('');
        $('#emailError1').html('');
        var username = $('#username').val();
        var email = $('#email').val();
        $.ajax({
            url : 'index.php?url=UserController/checkNameandEmail',
            type : 'post',
            dataType : 'json',
            data : {
                username : username,
                email : email,      
            },
            success : function (result){
              console.log(result.message1);
              console.log(result.message2);
                var html1 = '';
                var html2 = '';
                if (result.error == true){
                    html1 += result.message1 ;
                    html2 += result.message2 ;
                }

                if (html1 != ''|| html2 !=''){
                  if(html1 != ''){
                      $('#usernameError1').append(html1);
                      $('#usernameError1').addClass('alert alert-danger');
                  }
                    if (html2 != ''){
                      $('#emailError1').append(html2);
                      $('#emailError1').addClass('alert alert-danger');
                  }
                }
                 
                else {
                    $('form').submit();
                }
            }
        });
        return false;
    });

  </script>
</body>
</html>