<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Reset Your Password</title>
  <link rel="stylesheet" type="text/css" href="<?php echo asset('css/bootstrap.min.css') ?>">
</head>

<body>
  <div class="col-md-5 col-md-offset-3">
    <h1>Choose Your New Password</h1>
    <div class="form">
        <form action="<?php echo location('HomepageController/reset_password')?>" method="post"id = "reset_pass" autocomplete="off">
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" placeholder="Password" id="password" name="password" required autocomplete="off">
            <div id="passwordError"></div>
          </div>
          <div class="form-group">
              <label>Confirm Password</label>
              <input type="password" name="confirmPassword" placeholder="Confirm Password" class="form-control" required autocomplete="off">
              <div id="confirmPasswordError"></div>
          </div>
        
          <input type="hidden" name="email" value="<?= $email ?>">    
          <input type="hidden" name="hash" value="<?= $hash ?>">    
              
          <button type="submit" class="btn btn-primary""/>Apply</button>
        </form>
    </div>
  </div>
  <script type="text/javascript" src="<?php echo asset('js/jquery-3.2.1.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo asset('js/jquery.validate.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo asset('js/validate.js')?>"></script>


</body>
</html>