<?php
  if($_SESSION['logged_in'] != 1 ){
      $_SESSION['message'] = "You must log in before viewing your profile page!";
      header("location:".location('Homepagecontroller/error'));    
  }
  else {
      $fullname   = $_SESSION['fullname'];
      $email      = $_SESSION['email'];
      $active     = $_SESSION['active'];
  }
?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Welcome <?= $fullname ?></title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo asset('css/style.css') ?>">
  </head>

  <body>
    <div class="form">
        <h1>Welcome</h1>
        <p>
        <?php 
            if (isset($_SESSION['message']) ){
                echo $_SESSION['message'];
                unset( $_SESSION['message'] );
            }
        ?>
        </p>
        <?php
            if( !$active ){
                echo '<div class="info">Account is unverified, please confirm your email by clicking on the email link!</div>';
              }
        ?>
        <h2><?php echo $fullname ?></h2>
        <p><?php echo $email ?></p>
        <a href="<?php echo location('Homepagecontroller/logout') ?>"><button class="button button-block" name="logout"/>Home</button></a>

    </div>
      
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="<?php echo asset('js/index.js') ?>"></script>

  </body>
</html>
