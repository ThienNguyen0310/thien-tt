<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Viewlist</title>
	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/bootstrap.min.css') ?>">

</head>
<body>
	<div class="content container-fluid"  >
		<!-- <form method="post" action="<?php echo location("usercontroller/update/$user->id") ?>" enctype="multipart/form-data" autocomplete="off" id = 'update_user'> -->
			<div style="background-color:pink">
				<table class="table table-stripped">
					<tr>
						<a href="">
							<th width=100px><img src="<?php echo asset($user->image)?>" class="img-circle" width="100" height="70" margin-right=0px></th>
							<th><h2><?php echo $user->fullname ?></h2></th>
						</a>	
						<th><a style="float:right" class="btn btn-primary create" href="<?php echo location('Homepagecontroller/logout') ?>">Logout</a>
						</th>	
					</tr>
				</table>
				<div class="control clear-fix" style="float:right">
					<a href="<?php echo location('usercontroller/index/1') ?>" class="btn btn-warning" style="float: right;">Back</a>
				</div>
			</div>
			<div>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="infor">
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" required autocomplete="off" class="form-control" name="username" disabled value="<?php echo $user->username ?>">
							<!-- <div id="usernameError"></div> -->
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" disabled required autocomplete="off" class="form-control" placeholder="Password" id = "password" name="password" value="<?php echo $user->password ?>">
							
						</div>
						<div class="form-group">
							<label for="fullname">Fullname</label>
							<input type="text" disabled required autocomplete="off" class="form-control" placeholder="Fullname" name="fullname" value="<?php echo $user->fullname ?>">
							
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="text" required autocomplete="off" disabled class="form-control" placeholder="email" name="email" value="<?php echo $user->email ?>">
							
						</div>
						<div class="form-group">
							<label for="level">Level</label>
							<select name="level" class="form-control" disabled>
								<?php if(!$user->level){ ?>
									<option value='0' selected="selected">Member</option>
									<option value='1'>Admin</option>
								 
								<?php }else{ ?>
									<option value='0' >Member</option>
									<option value='1' selected="selected">Admin</option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label for="department">Department</label>
							<select name="department_id" disabled class="form-control">
								<?php 
									foreach($department as $dep){
										if($dep->id == $user->department_id){ 
								?>
											<option value="<?php echo $dep->id ?>" selected><?php echo $dep->name?></option>
										<?php }else{ ?>
											<option value="<?php echo  $dep->id?>"><?php echo $dep->name?></option>
										<?php }
									} 
								?>
							</select>
						</div>
						<div class="form-group">
								<label for="image">Image</label>
								<input type="file" disabled name="image" class="form-file-control" id ="fileChooser" onchange="previewFile();">
								<img id = "preview" src="<?php echo asset($user->image)?>">
								
						</div>
					</div>
				</div>
			</div>
		<!-- </form> -->
	</div>
</body>
</html>