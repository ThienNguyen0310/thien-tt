<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Viewlist</title>
	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/bootstrap.min.css') ?>">

</head>
<body>
	<div class="content container-fluid"  >
		<div style="background-color:pink">
			<table class="table table-stripped">
				<tr>
					<th width=100px>
						<a href="<?php echo location("usercontroller/viewdetail/$userMy->id") ?>">
							<img src="<?php echo asset($userMy->image)?>" class="img-circle" width="100" height="70" margin-right=0px>
						</a>
					</th>
					<th>
						<a href="<?php echo location("usercontroller/viewdetail/$userMy->id") ?>">
							<h2><?php echo $userMy->fullname ?></h2>
						</a>
					</th>	
					<th>
						<a style="float:right" class="btn btn-primary create" href="<?php echo location('Homepagecontroller/logout') ?>">Logout</a>
					</th>	
				</tr>
			</table>
	    	<!-- <div class="control clear-fix" style="float:right">
				<a class="btn btn-warning search" href="#">Search</a>
	      		<input class="input-search" type="text" name="search" placeholder="Enter keyword to search">
			</div> -->
		</div>
		<div>
			<h2>Member cùng thuộc Department: <?php echo $userMy->department->name ?> với <?php echo $userMy->fullname ?></h2>
			<table class="table table-stripped">
		      <tbody>
		        <tr>
		          <th>Id</th>
		          <th>Fullname</th>
		          <th>Department</th>
		          <th>Role</th>
		          <th>Image</th>
		          <th>Created at</th>
		          <th>Updated at</th>
		        </tr>
		        <?php foreach($users as $us){ ?>
		          <tr>
		          <td><?php echo $us->id ?></td>
		          <td><?php echo $us->fullname ?></td>
		          <td><?php echo $us->department->name ?></td>
		          <td><?php 
		              if(!$us->level){
		                echo 'Member';
		              }
		              else{
		                echo 'Admin';
		              }
		           ?></td>
		          <td><img src="<?php echo asset($us->image)?>"></td>
		          <td><?php echo $us->created_at ?></td>
		          <td><?php echo $us->updated_at ?></td>

		          <th><a class="btn btn-success" href="<?php echo location("usercontroller/viewdetailSamDep/$us->id") ?>">Details</a></th>
		        </tr>
		         <?php }?>
		      </tbody>
  			</table>
		</div>
	</div>
</body>
</html>