<?php 
	use Models\Department;
	session_start();
	class DepartmentController
	{
		public function __construct(){

		}

		public function index($currentPage){
			if($_SESSION['logged_in'] == true && $_SESSION['level']){
				$totalItems = Department::totalItems();
				$totalItemsPerPage = 10;
				$totalPage =ceil($totalItems/$totalItemsPerPage);
				if($currentPage < 1){	
					$currentPage = 1;
				}
				else if($currentPage > $totalPage){
					$currentPage = $totalPage;
				}
				$pageRange = 3;
				$position = ($currentPage - 1) * $totalItemsPerPage;
				$department = Department::list($position, $totalItemsPerPage);
				//var_dump($department); die();
				$templatePath = ROOT . 'resources/views/admin/department/index.php';
				$view = new View($templatePath,compact('pageRange','department','currentPage','totalPage'));
				$view->render();
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function create(){
			if($_SESSION['logged_in'] == true && $_SESSION['level']){
				$department = Department::all();
				$templatePath = ROOT . 'resources/views/admin/department/create.php';
				$view = new View($templatePath,compact('department'));
				$view->render();
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function store(){
			if($_SESSION['logged_in'] == true && $_SESSION['level']){
				$dep = new Department();
				$dep->name = $_POST['name'];
				$dep->created_at = date('Y:m:d H:i:s');
				try {
					$dep->insert();
					header('Location: '.location('departmentcontroller/index/1'));
				} catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function edit($id){
			if($_SESSION['logged_in'] == true && $_SESSION['level']){
				$department = Department::find($id);
				$templatePath = ROOT . 'resources/views/admin/department/edit.php';
				$view = new View($templatePath,compact('department'));
				$view->render();
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function update($id){
			if($_SESSION['logged_in'] == true && $_SESSION['level']){
				$dep = new Department();
				$dep->id = $id;
				$dep->name = $_POST['name'];
				$dep->updated_at = date('Y:m:d H:i:s');
				try {
					$dep->update();
					header('Location: '.location('departmentcontroller/index/1'));
				} catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function delete($id){
			if($_SESSION['logged_in'] == true && $_SESSION['level']){
				$dep = new Department();
				$dep->id = $id;
				$dep->deleted_at = date('Y:m:d H:i:s');
				try {
					$users = User::all();
					foreach ($users as $us) {
						if($us->department_id == $id){
							$us->delete();
						}
					}
					$dep->delete();
					header('Location:'.location('departmentcontroller/index/1'));
				} catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function checkName(){
			$name = isset($_POST['name']) ? $_POST['name'] : false; 
			$result = array(
			    'error' => false,
			    'message' => '',
			);
			// Kiểm tra tên đăng nhập
			if ($name){
			    if (Department::checkName($name)){
			        $result['message'] = 'name already exists';
			        $result['error'] = true;
			    }
			    
			} 
			die (json_encode($result));
		}
	}

 ?>