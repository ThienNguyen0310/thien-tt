<?php 
	use Models\User;
	use Models\Department;
	session_start();


	class UserController
	{
		public function __construct(){

		}

		public function index($currentPage){
			if($_SESSION['logged_in'] == true){
				if($_SESSION['level']){
					$totalItems = User::totalItems();
					$totalItemsPerPage = 10;
					$totalPage =ceil($totalItems/$totalItemsPerPage);
					if($currentPage < 1){	
						$currentPage = 1;
					}
					else if($currentPage > $totalPage){
						$currentPage = $totalPage;
					}
					$pageRange = 10;
					$position = ($currentPage - 1) * $totalItemsPerPage;
					$users = User::list($position, $totalItemsPerPage);
					$templatePath = ROOT.'resources/views/admin/users/index.php';
					$view = new View($templatePath,compact('pageRange','users','currentPage','totalPage'));
					$view->render();
				}
				else{
					$email = $_SESSION['email'];
					$userMy = User:: findByEmail($email);
					$users = User:: userSameDepartment($userMy->department_id);
					$templatePath = ROOT.'resources/views/member/viewlist.php';
					$view = new View($templatePath,compact('users','userMy'));
					$view->render();
				}
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function create(){//var_dump($_SESSION); die();
			if($_SESSION['logged_in'] == true && $_SESSION['level']){
				$templatePath = ROOT . 'resources/views/admin/users/create.php';
				$department = Department::all();
				$view = new View($templatePath,compact('department'));
				$view->render();
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function store(){
			if($_SESSION['logged_in'] ==true && $_SESSION['level']){
				$us = new User();
				$us->username = $_POST['username'];
				$us->password = password_hash($_POST['password'],PASSWORD_BCRYPT);
				$us->fullname = $_POST['fullname'];
				$us->email = $_POST['email'];
				$us->level = $_POST['level'];
				$us->hash = md5( rand(0,1000) );
				$us->active = '0';
				$us->department_id = $_POST['department_id'];
				$us->created_at = date('Y:m:d H:i:s');
				$image = $_FILES['image'];
				$infor = pathinfo($image['name']);
				$extension = $infor['extension'];
				$url = 'storage/images/'.uniqid().'.'.$extension;
				$targetPath = ROOT .  'public/'.$url;
				$result = move_uploaded_file($image['tmp_name'],$targetPath);
				// $folder = ROOT . 'public/storage/images/';
				$list = getimagesize($targetPath);
				$width1  = $list[3];
				$str = str_replace( 'width="', '', $width1 );
				$str = str_replace( 'height="', '', $str );
				$str = str_replace( '"', '', $str );
				$str = explode(" ",$str);
				$width = $str[0];$height = $str[1];
				$newwidth = 300;
				$newheight = 300;

				$thumb = imagecreatetruecolor( $newwidth, $newheight );
				$source = imagecreatefromjpeg( $targetPath );
				imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				imagejpeg( $thumb, $targetPath, 100 );
				if($result){
					$us->image = $url;
				}
				try {
					$us->insert();
					$nTo = $us->fullname;
                    $mTo = $us->email;
                    $title = 'Account Verification';
                    $content = 'Hello '.$us->fullname.',Thank tou for signing up! Please click this link to activate your account: 
                    http://localhost/project_demo/public/index.php?url=HomepageController/verify/'.$us->email.'/'.$us->hash;
                     
                    require APP . 'core/mail/function.php';
                    $mail = sendMail($title, $content, $nTo, $mTo);
                    if($mail==1){
                        header('Location: '.location('usercontroller/index/1'));
                    }          
					
				} catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function edit($id){
			if($_SESSION['logged_in'] == true ){
				if($_SESSION['level']){
					$user = User::find($id);
					$department = Department::all();
					$templatePath = ROOT . 'resources/views/admin/users/edit.php';
					$view = new View($templatePath,compact('user','department'));
					$view->render();
				}
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function update($id){
			if($_SESSION['logged_in']){
				if($_SESSION['level']){
					
					$us = new User();
					$us->id = $id;
					$us->username = $_POST['username'];
					$us->password = password_hash($_POST['password'],PASSWORD_BCRYPT);
					$us->fullname = $_POST['fullname'];
					$us->email = $_POST['email'];
					$us->level = $_POST['level'];
					$us->hash = md5( rand(0,1000) );
					$us->department_id = $_POST['department_id'];
					$us->updated_at = date('Y:m:d H:i:s');
					$image = $_FILES['image'];
					$infor = pathinfo($image['name']);
					$extension = $infor['extension'];
					$url = 'storage/images/'.uniqid().'.'.$extension;
					$targetPath = ROOT .  'public/'.$url;
					$result = move_uploaded_file($image['tmp_name'],$targetPath);
					$list = getimagesize($targetPath);
					$width1  = $list[3];
					$str = str_replace( 'width="', '', $width1 );
					$str = str_replace( 'height="', '', $str );
					$str = str_replace( '"', '', $str );
					$str = explode(" ",$str);
					$width = $str[0];$height = $str[1];
					$newwidth = 300;
					$newheight = 300;

					$thumb = imagecreatetruecolor( $newwidth, $newheight );
					$source = imagecreatefromjpeg( $targetPath );
					imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
					imagejpeg( $thumb, $targetPath, 100 );
					if($result){
						$us->image = $url;
					}
					try {
						$us->update();
						header('Location: '.location('usercontroller/index/1'));
					} catch (Exception $e) {
						echo $e->getMessage();
					}
				}
				else{
					$us = new User();
					$us->password = password_hash($_POST['password'],PASSWORD_BCRYPT);
					$us->fullname = $_POST['fullname'];
					$us->hash = md5( rand(0,1000) );
					$us->department_id = $_POST['department_id'];
					$us->updated_at = date('Y:m:d H:i:s');
					$image = $_FILES['image'];
					$infor = pathinfo($image['name']);
					$extension = $infor['extension'];
					$url = 'storage/images/'.uniqid().'.'.$extension;
					$targetPath = ROOT .  'public/'.$url;
					$result = move_uploaded_file($image['tmp_name'],$targetPath);
					if($result){
						$us->image = $url;
					}
					try {
						$us->update_Member($id,$us->password,$us->fullname,$us->hash,$us->department_id,$us->updated_at,$us->image);
						header('Location: '.location("usercontroller/viewdetail/$id"));
					} catch (Exception $e) {
						echo $e->getMessage();
					}
				}
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function delete($id){
			if($_SESSION['logged_in'] ==true && $_SESSION['level']){
				$us = new User();
				$us->id = $id;
				$us->deleted_at = date('Y:m:d H:i:s');
				try {
					$us->delete();
					header('Location:'.location('usercontroller/index/1'));
				} catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			else{
				header('location: '.location('HomepageController/login'));
			}
		}

		public function viewdetail($id){
			if($_SESSION['logged_in'] == true ){
				$user = User::find($id);
				$department = Department::all();
				$templatePath = ROOT . 'resources/views/member/viewdetail.php';
				$view = new View($templatePath,compact('user','department'));
				$view->render();
			}
		}

		public function viewdetailSamDep($id){
			if($_SESSION['logged_in'] == true ){
				$user = User::find($id);
				$department = Department::all();
				$templatePath = ROOT . 'resources/views/member/viewdetailSamDep.php';
				$view = new View($templatePath,compact('user','department'));
				$view->render();
			}
		}

		public function checkNameandEmail(){
			$username = isset($_POST['username']) ? $_POST['username'] : false; 
			$email = isset($_POST['email']) ? $_POST['email'] : false; 
			$result = array(
			    'error' => false,
			    'message1' => '',
			    'message2' => '',
			);
			if ($email&&$username){
			    if (User::checkUserName($username)){
			        $result['message1'] = 'username already exists';
			        $result['error'] = true;
			    }
			    if(User::checkEmail($email)){
			    	$result['message2'] = 'email already exists';
			        $result['error'] = true;
			    }
			} 
			die (json_encode($result));
		}

		// public function checkNameandEmailEdit(){
		// 	$username = isset($_POST['username']) ? $_POST['username'] : false; 
		// 	$email = isset($_POST['email']) ? $_POST['email'] : false; 
		// 	$result = array(
		// 	    'error' => false,
		// 	    'message1' => '',
		// 	    'message2' => '',
		// 	);
		// 	if ($email&&$username){
		// 	    if (User::checkUserName($username)){
		// 	        $result['message1'] = 'username already exists';
		// 	        $result['error'] = true;
		// 	    }
		// 	    if(User::checkEmail($email)){
		// 	    	$result['message2'] = 'email already exists';
		// 	        $result['error'] = true;
		// 	    }
		// 	} 
		// 	die (json_encode($result));
		// }
	}
 ?>