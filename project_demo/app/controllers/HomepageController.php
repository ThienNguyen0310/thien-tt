<?php 
    use Models\User;
    use Models\Department;
    session_start();
    class HomepageController
    {
        public function __construct(){

        }

        public function login(){
            $templatePath = ROOT . 'resources/views/auth/login.php';
            $view = new View($templatePath,compact('department'));
            $view->render();
        }

        public function login_processing(){
            $username = isset($_POST['username']) ? $_POST['username'] : false;
            $result = User::findUsername($username);
            if(!$result){
                //$_SESSION['message'] = "User with that username doesn't exist";
                header("location: ".location('HomepageController/login'));
            }else{
                $password = $_POST['password'];
                if(password_verify($password,$result->password)){
                    if($result->active == 1){
                        $_SESSION['email'] = $result->email;
                        $_SESSION['fullname'] = $result->fullname;
                        $_SESSION['active'] = $result->active;
                        $_SESSION['logged_in'] = true;
                        $_SESSION['level'] = $result->level;
                        if (isset($_POST['remember'])) {
                            setcookie('username',$username,time()+3600);
                            setcookie('password',$password,time()+3600);
                        }
                        header("location: ".location('UserController/index/1'));
                    }
                    else{       
                        //$_SESSION['message'] = "You have not yet confirmed your account!";
                        header("location: ".location('HomepageController/login'));
                    }
                } 
                else{
                    //$_SESSION['message'] = "You have entered wrong password, try again!";
                    header("location: ".location('HomepageController/login'));
                }
            }
            
        }

        public function checkLogin(){
            $username = isset($_POST['username']) ? $_POST['username'] : false; 
            $password = isset($_POST['password']) ? $_POST['password'] : false; 
            $result = array(
                'error' => false,
                'message1' => '',
            );

            $user = User::findUsername($username);
            if(!$user){
                $result['message1'] = "username or password does't exists";
                $result['error'] = true;
            }
            else{
                if(!password_verify($password,$user->password)){
                    $result['message1'] = "username or password does't exists or Your account has not confirmed by email";
                    $result['error'] = true;
                }
            }
            die (json_encode($result));
        }

        public function checkReset(){
            $email = isset($_POST['email']) ? $_POST['email'] : false; 
            $result = array(
                'error' => false,
                'message1' => '',
            );
            // Kiểm tra tên đăng nhập
            if ($email){
                if (!User::checkEmail($email)){
                    $result['message1'] = "User with that email doesn't exist ";
                    $result['error'] = true;
                } 
            } 
            die (json_encode($result));
        }

        public function register(){
            $department = Department::all();
            $templatePath = ROOT . 'resources/views/auth/register.php';
            $view = new View($templatePath,compact('department'));
            $view->render();
        }

        public function register_processing(){
            $_SESSION['email']   = $_POST['email'];
            $_SESSION['fullname'] = $_POST['fullname'];
            $us = new User();
            $us->email  = $_POST['email'];
            $us->fullname = $_POST['fullname'];
            $us->username = $_POST['username'];
            $us->department_id = $_POST['department_id'];
            $us->password = password_hash($_POST['password'],PASSWORD_BCRYPT);
            $us->hash = md5(rand(0,1000));
            $us->created_at = date('Y:m:d H:i:s');
            $us->level = "";
            $us->image = "";
            $us->active = 0;
            $result = User::checkrow($us->username,$us->email);
            //var_dump($result);die();
            if($result>0){
                //$_SESSION['message'] = 'User with this email or username already exists!';
                header('location:'. location('HomepageController/register'));
            }

            else{
                if($us->insert()){
                    $_SESSION['active'] = 0;
                    $_SESSION['logged_in'] = true;
                    $_SESSION['message'] = "Confirmation link has been sent to $us->email, please verify your account by clickng on the link in the message";
                    $nTo = $us->fullname;
                    $mTo = $us->email;
                    $title = 'Account Verification';
                    $content = 'Hello '.$us->fullname.',Thank tou for signing up! Please click this link to activate your account: 
                    http://localhost/project_demo/public/index.php?url=HomepageController/verify/'.$us->email.'/'.$us->hash;
                     
                    require APP . 'core/mail/function.php';
                    $mail = sendMail($title, $content, $nTo, $mTo);
                    if($mail==1)
                        header('location:'. location('HomepageController/profile'));
                    else {
                        $_SESSION['message'] = 'Co loi , khong gui duoc mail';
                        header('location: '.location('HomepageController/error'));
                    }           
                }
                else{
                    $_SESSION['message'] = 'Register failed!';
                    header('location:'. location('HomepageController/profile'));
                }
            }
        }

        public function forgot(){
            $templatePath = ROOT . 'resources/views/auth/forgot.php';
            $view = new View($templatePath);
            $view->render();
        }

        public function forgot_processing(){
            $email = $_POST['email'];
            $result = User::findByEmail($email);
            if(!$result){
                $_SESSION['message'] = "User with that email doesn't exist!";
                header("location:".location('HomepageController/error'));
            }
            else{
                $email     = $result->email;
                $hash      = $result->hash;
                $fullname  = $result->fullname;

                $_SESSION['message'] = "<p>Please check your email <span>$result->email</span>". " for a confirmation link to complete your password reset!</p>";
                $nTo = $result->fullname;
                $mTo = $result->email;
                $title = 'Password Reset Link';
                $content = 'Hello '.$us->fullname.',You have requested password reset!
                    Please click this link to reset your password:
                    http://localhost/project_demo/public/index.php?url=HomepageController/reset/'.$result->email.'/'.$result->hash;
                     
                require APP . 'core/mail/function.php';
                $mail = sendMail($title, $content, $nTo, $mTo);
                if($mail==1)
                    header('location:'. location('HomepageController/success'));
                else {
                    $_SESSION['message'] = 'Co loi , khong gui duoc mail';
                    header('location: '.location('HomepageController/error'));
                }
            }
        }

        public function reset($email,$hash){
            $templatePath = ROOT . 'resources/views/auth/reset.php';
            $view = new View($templatePath,compact('email','hash'));
            $view->render();
        }

        public function reset_password(){
            if($_POST['password'] == $_POST['confirmPassword']){
                $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
                $email = $_POST['email'];
                $hash = $_POST['hash'];
                $updated_at = date('Y:m:d H:i:s');
                $user = new User();

                if ( $user->update_password($email,$hash,$password,$updated_at) ) {
                    $_SESSION['message'] = "Your password has been reset successfully!";
                    header("location: ".location('HomepageController/success'));    
                }
            }
            else{
                //$_SESSION['message'] = "Two passwords you entered don't match, try again!";
                header("location:".location('HomepageController/reset/$email/$hash'));
            }
        }

        public function verify($email,$hash){
            if(isset($email) && !empty($email) AND isset($hash) && !empty($hash)){
                $result = User::checkactive($email,$hash);
                if($result == 0){
                    $_SESSION['message'] = "Account has already been activated or the URL is invalid";
                    header('location: '. location('HomepageController/error') );
                }
                else{
                    $_SESSION['message'] = 'Your account has been activated';
                    $user = new User();
                    $updated_at = date('Y-m-d H-i-s');
                    $user->update_active($email,$updated_at);
                    $_SESSION['active'] = 1;
                    header('location:'.location('HomepageController/login')) ;               
                }
               
            }else{
                $_SESSION['message'] = "Invalid parameters provided for account verification!";
                header("location: ". location('HomepageController/error'));
            }
        }

        public function profile(){
            $templatePath = ROOT . 'resources/views/auth/profile.php';
            $view = new View($templatePath);
            $view->render();
        }

        public function error(){
            $templatePath = ROOT . 'resources/views/auth/error.php';
            $view = new View($templatePath);
            $view->render();
        }
        public function success(){
            $templatePath = ROOT . 'resources/views/auth/success.php';
            $view = new View($templatePath);
            $view->render();
        }

        public function logout(){
            $templatePath = ROOT . 'resources/views/auth/logout.php';
            $view = new View($templatePath);
            $view->render();
        }
    }
 ?>