<?php 
	namespace Models;
	use Database;
	use PDO;

	class User
	{
		protected $attributes = [];
		public function __construct(){

		}

		public function __set($name,$value){
			$this->attributes[$name] = $value;
		}

		public function __get($name){
			if(array_key_exists($name, $this->attributes)){
				return $this->attributes[$name];
			}
		}

		public function insert(){
			$con = Database::getInstance();
			$sql = 'insert into Users(username,password,fullname,email,image,level,hash,active,department_id,created_at) values (:username,:password,:fullname,:email,:image,:level,:hash,:active,:department_id,:created_at)';
			$pstm = $con->prepare($sql);
			return $pstm->execute($this->attributes);
		}

		public function update(){
			$con = Database::getInstance();
			$sql = "update Users set username = :username, password = :password, fullname = :fullname, email = :email, image = :image, level = :level,hash = :hash, department_id = :department_id, updated_at = :updated_at where id = :id";
			$pstm = $con->prepare($sql);
			return $pstm->execute($this->attributes);
		}

		public function update_Member($id,$password,$fullname,$hash,$department_id,$updated_at,$image){
			$con = Database::getInstance();
			$sql = "update Users set password = '$password', fullname = '$fullname', image = '$image', hash = '$hash', department_id = '$department_id', updated_at = '$updated_at' where id = '$id'";
			return $con->query($sql);
		}

		public static function all(){
			$con = Database::getInstance();
			$users = [];
			$sql = 'select * from Users where deleted_at is null';
			$pstm = $con->query($sql,PDO::FETCH_CLASS,__CLASS__);
			$users = $pstm->fetchAll();

			foreach($users as $us){
				$departmentID = $us->department_id;
				if(!empty($departmentID)){
					$department = Department::find($departmentID);
				}
				$us->department = $department;
			}
			return $users;
		}

		public function delete(){
			$con = Database::getInstance();
			$sql = "update Users set deleted_at = :deleted_at where id = :id";
			$pstm = $con->prepare($sql);
			return $pstm->execute($this->attributes);
		}

		public static function find($id){
			$con = Database::getInstance();
			$users = [];
			$sql  = "select * from Users where deleted_at is null and id = '$id'";
			$users = $con->query($sql)->fetchObject(__CLASS__);
			return $users;
		}

		public static function findUsername($username){
			$con = Database::getInstance();
			$users = [];
			$sql  = "select * from Users where deleted_at is null and username = '$username' and active = '1'";
			$users = $con->query($sql)->fetchObject(__CLASS__);
			return $users;
		}

		public static function findByEmail($email){
			$con = Database::getInstance();
			$users = [];
			$sql  = "select * from Users Where deleted_at is null and email = '$email'";
			$users = $con->query($sql)->fetchObject(__CLASS__);
			$department = Department::find($users->department_id);
			$users->department = $department;
			return $users;
		}

		public static function totalItems(){
			$con = Database::getInstance();
			$sql = "select count(id) from Users where deleted_at is null and active = '1'";
			$nRows = $con->query($sql)->fetchColumn();
			return $nRows;
		}

		public static function list($position, $totalItemsPerPage){
			$con = Database::getInstance();
			$sql = "select * from Users where deleted_at is null and active = '1' Limit $position,$totalItemsPerPage";
			$pstm = $con->query($sql,PDO::FETCH_CLASS,__CLASS__);
			$users = $pstm->fetchAll();
			foreach($users as $us){
				$departmentID = $us->department_id;
				if(!empty($departmentID)){
					$department = Department::find($departmentID);
				}
				$us->department = $department;
			}
			return $users;
		}

		public static function checkrow($username, $email){
			$con = Database::getInstance();
			$sql = "select count(id) from Users where email = '$email' or username = '$username'";
			$nRows = $con->query($sql)->fetchColumn();
			return $nRows;
		}

		public static function checkactive($email, $hash){
			$con = Database::getInstance();
			$sql = "select count(id) from Users where deleted_at is null and email = '$email' and hash = '$hash' and active = '0'";
			$nRows = $con->query($sql)->fetchColumn();//var_dump($result); die(); 
			return $nRows;
		}

		public static function checkUserName($username){
			$con = Database::getInstance();
			$sql = "select count(id) from Users where username = '$username'";
			$nRows = $con->query($sql)->fetchColumn();
			return $nRows;
		}

		public static function checkEmail($email){
			$con = Database::getInstance();
			$sql = "select count(id) from Users where email = '$email'";
			$nRows = $con->query($sql)->fetchColumn();
			return $nRows;
		}

		public static function update_active($email,$updated_at){
			$con = Database::getInstance();
			$sql = "update Users set active = '1', updated_at = '$updated_at' where email = '$email'";
			return $con->query($sql);
		}

		public static function update_password($email,$hash,$newpassword,$updated_at){
			$con = Database::getInstance();
			$sql = "update Users set password = '$newpassword', hash = '$hash', updated_at = '$updated_at' where email = '$email'";
			return $con->query($sql);
		}
		public static function userSameDepartment($department_id){
			$con = Database::getInstance();
			$users = [];
			$sql = "select * from Users where deleted_at is null and department_id = '$department_id' and active = '1'";
			$pstm = $con->query($sql,PDO::FETCH_CLASS,__CLASS__);
			$users = $pstm->fetchAll();

			foreach($users as $us){
				$departmentID = $us->department_id;
				if(!empty($departmentID)){
					$department = Department::find($departmentID);
				}
				$us->department = $department;
			}
			return $users;
		}
	}
 ?>