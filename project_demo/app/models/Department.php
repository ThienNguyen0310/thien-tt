<?php 
	namespace Models;
	use Database;
	use PDO;

	class Department
	{
		protected $attributes = [];
		public function __construct(){

		}

		public function __set($name,$value){
			$this->attributes[$name] = $value;
		}

		public function __get($name){
			if(array_key_exists($name, $this->attributes)){
				return $this->attributes[$name];
			}
		}

		public function insert(){
			$con = Database::getInstance();
			$sql = 'insert into Department(name,created_at) values (:name,:created_at)';
			$pstm = $con->prepare($sql);
			return $pstm->execute($this->attributes);
		}

		public function update(){
			$con = Database::getInstance();
			$sql = "update Department set name = :name, updated_at = :updated_at where id = :id";
			$pstm = $con->prepare($sql);
			return $pstm->execute($this->attributes);
		}

		public static function all(){
			$con = Database::getInstance();
			$department = [];
			$sql = 'select * from Department where deleted_at is null';
			$pstm = $con->query($sql,PDO::FETCH_CLASS,__CLASS__);
			$department = $pstm->fetchAll();
			return $department;
		}

		public function delete(){
			$con = Database::getInstance();
			$sql = "update Department set deleted_at = :deleted_at where id = :id";
			$pstm = $con->prepare($sql);
			return $pstm->execute($this->attributes);
		}

		public static function find($id){
			$con = Database::getInstance();
			$department = [];
			$sql  = "select * from Department where deleted_at is null and id = '$id'";
			$department = $con->query($sql)->fetchObject(__CLASS__);
			return $department;
		}

		public static function totalItems(){
			$con = Database::getInstance();
			$sql = "select count(id) from Department where deleted_at is null";
			$nRows = $con->query($sql)->fetchColumn();
			return $nRows;
		}

		public static function list($position, $totalItemsPerPage){
			$con = Database::getInstance();
			$sql = "select * from Department where deleted_at is null Limit $position,$totalItemsPerPage";
			$pstm = $con->query($sql,PDO::FETCH_CLASS,__CLASS__);
			$department = $pstm->fetchAll();
			return $department;
		}

		public static function checkName($name){
			$con = Database::getInstance();
			$sql = "select count(id) from Department where name = '$name'";
			$nRows = $con->query($sql)->fetchColumn();
			return $nRows;
		}
	}
 ?>