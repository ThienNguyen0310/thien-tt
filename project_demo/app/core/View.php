<?php 
	class View
	{
		private $templatePath;
		private $data;

		public function __construct($templatePath, $data = []){
			$this->templatePath = $templatePath;
			$this->data = $data;
		}

		public function getTemplatePath(){
			return $this->templatePath;
		}

		public function getData(){
			return $this->data;
		}

		public function render(){
			foreach ($this->getData() as $key => $value){
				$$key = $value;
			}
			require $this->getTemplatePath();
		}
	}
?>