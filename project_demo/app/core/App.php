<?php 

	class App
	{
		public function __construct(){
			$url = $this->getUrl();
			$route = Route::getRouteByUrl($url);
			$route->run();
		}

		public function getUrl(){
			$url = isset($_REQUEST['url']) ? $_REQUEST['url'] : null;
			return $url;
		}

	}
 ?>