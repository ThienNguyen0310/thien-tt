<?php 
	class Database
	{
		public static $connection;

		public function __construct(){

		}

		public static function getInstance(){
			if(!isset(self::$connection)){
				$driverName = 'mysql';
				$host = '127.0.0.1';
				$databaseName = 'project_demo';
				$dsn = $driverName . ':host=' . $host . ';dbname=' . $databaseName . ';charset=utf8';
				$userName = 'root';
				$password = '';

				try{
					self::$connection = new PDO($dsn, $userName, $password);
					self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				}catch(PDOException $e){
					echo $e->getMessage();
					die();
				}

			}
			return self::$connection;
		}
	}
 ?>