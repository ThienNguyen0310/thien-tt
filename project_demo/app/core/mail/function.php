<?php
    function sendMail($title, $content, $nTo, $mTo){
        require "PHPMailer.php";
        require "SMTP.php";
        $nFrom = 'Sky.net';
        $mFrom = 'n.v.thien.0310@gmail.com';  //dia chi email cua ban 
        $mPass = 'khongcopass';       //mat khau email cua ban
        $mail             = new PHPMailer();
        $body             = $content;
        $mail->IsSMTP(); 
        $mail->CharSet   = "utf-8";
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth   = true;                    // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host       = "smtp.gmail.com";        
        $mail->Port       = 465;
        $mail->Username   = $mFrom;
        $mail->Password   = $mPass;              
        $mail->SetFrom($mFrom, $nFrom);

        $mail->Subject    = $title;
        $mail->MsgHTML($body);
        $address = $mTo;
        $mail->AddAddress($address, $nTo);
        $mail->AddReplyTo('info@freetuts.net', 'Freetuts.net');
        if(!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
    }
?>