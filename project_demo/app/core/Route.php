<?php 
	class Route
	{
		private $controller;
		private $action;
		private $params;

		public function __construct(){
			$this->controller = 'HomeController';
			$this->action = 'index';
			$this->params = [];
		}
		// public function __construct($controller,$action,$params){
		// 	$this->controller = $controller;
		// 	$this->action = $action;
		// 	$this->params = $params;
		// }

		public function setController($controller){
			$this->controller = $controller;
		}

		public function setAction($action){
			$this->action = $action;
		}

		public function setParams($params){
			$this->params = $params;
		}

		public static function getRouteByUrl($url){
			$route = new Route();

			if(!empty($url)){
				$url = trim($url,'/');
				$url = explode('/',$url);

				$controller = isset($url[0]) ? $url[0] : null;
				$action = isset($url[1]) ? $url[1] : null;
				unset($url[0],$url[1]);
				$params = array_values($url);
				$route->setController($controller);
				$route->setAction($action);
				$route->setParams($params);
			}

			return $route;
		}

		public function run(){
			$controllerPath = APP . 'controllers/' . $this->controller . '.php';
			if(file_exists($controllerPath)){
				require_once $controllerPath;
				$controller = new $this->controller();
				if(method_exists($controller, $this->action)){
					call_user_func_array([$controller, $this->action], $this->params);
				}
				else{
					echo 'Method is not exist';
				}
			}
			else{
				echo 'Controller is not exists';
			}

		}
	}
 ?>