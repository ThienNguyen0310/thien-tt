<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('department_id');
        return [
          'name' => 'bail|required|min:6|unique:department,name,'.$id,
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'name.min'  => 'Name is at least 6 characters long',
            'name.unique' => 'Name already exists'
        ];
    }
}
