<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'bail|required|min:6',
          'email' => 'bail|required|email|unique:users,email',
          'password' => 'bail|required|min:8|confirmed',
          'image' => 'bail|nullable|image|max:5000',
          //'date_of_birth' =>'date_format:"d-m-y"',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'name.min' => 'Name is at least 6 characters long',
            'email.required'  => 'Email is required',
            'email.email'  => 'Your email address must be in the format of name@domain.com',
            'email.unique' => 'Email already exists',
            'password.min' => 'Password is at least 8 characters long',
            'password.required' => 'Password is required',
            'password.confirmed' => 'Password and Confirm Password no matching',
            'image.max' => 'Image image has the largest size is 5000kb',
            'image.image' => 'The image must be formatted jpeg, png, bmp, gif, or svg',
            //'date_of_birth.date_format' => 'Date of birth not in the correct format',
            //'date_of_birth.date' => 'Date of birth not in the correct format',

        ];
    }
}
