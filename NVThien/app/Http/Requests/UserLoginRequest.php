<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'email' => 'bail|required|email|max:255|unique:users,email',
          'password' => 'bail|required|min:8|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'email.required'  => 'Email is required',
            'email.email'  => 'Your email address must be in the format of name@domain.com',
            'email.unique' => 'Email already exists',
            'email.max' => 'Email is at biggest 255 characters long',
            'password.min' => 'Password is at least 8 characters long',
            'password.required' => 'Password is required',
            'password.confirmed' => 'Password and Confirm Password no matching',

        ];
    }
}
