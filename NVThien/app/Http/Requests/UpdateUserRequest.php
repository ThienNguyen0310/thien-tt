<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $id = $this->request->get('users_id');
        return [
          'name' => 'bail|required|min:6',
          'password' => 'bail|required|min:8|confirmed',
          'image' => 'bail|nullable|image|max:5000',
          //'date_of_birth' =>'date_format:"d-m-y"',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'name.min' => 'Name is at least 6 characters long',
            'password.min' => 'Password is at least 8 characters long',
            'password.required' => 'Password is required',
            'password.confirmed' => 'Password and Confirm Password no matching',
            'image.max' => 'Image image has the largest size is 5000kb',
            'image.image' => 'The image must be formatted jpeg, png, bmp, gif, or svg',
            //'date_of_birth.date_format' => 'Date of birth not in the correct format',
            //'date_of_birth.date' => 'Date of birth not in the correct format',

        ];
    }
}
