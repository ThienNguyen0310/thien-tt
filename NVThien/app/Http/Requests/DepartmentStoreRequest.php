<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class DepartmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'bail|required|min:6|unique:department,name',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A name attribute is required',
            'name.min'  => 'Name attribute is at least 6 characters long',
            'name.unique' => 'Name attribute already exists'
        ];
    }
}
