<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\Role;
use Event;
use Image;
use App\Department;
use App\VerifyUsers;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\VerifyMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Storage;
use App\Events\SendMail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    

    public function showRegistrationForm()
    {
        $department = Department::all();
        return view('auth.register',compact('department'));
    }


    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $imagePath = "";
        if ($request->hasFile('image')){
                $imagePath = $request->file('image')->store('public/images');
                $image = Image::make(Storage::get($imagePath))->resize(300,300)->encode();
                Storage::put($imagePath,$image);
                $imagePath = explode('/',$imagePath);
                $imagePath = $imagePath[2];
        }
        Event::fire(new SendMail($user = $this->create($request->all(),$imagePath)));
        //event(new Registered($user = $this->create($request->all(),$imagePath)));

        $this->guard()->login($user);


        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }


    /**
     * Activate the user with given activation code.
     * @param string $activationCode
     * @return string
     */
    public function verifyUser($confirmation_code)
    {
        $verifyUser = VerifyUsers::where('confirmation_code', $confirmation_code)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->confirmed) {
                $verifyUser->user->confirmed = 1;
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }
 
        return redirect('/login')->with('status', $status);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'image' => 'bail|nullable|image|max:5000',
        ],[
            'name.string' => 'Name is not string',
            'name.required' => 'Name is required',
            'name.min' => 'Name is at least 6 characters long',
            'name.max' => 'Name is at biggest 255 characters long',
            'email.required'  => 'Email is required',
            'email.email'  => 'Your email address must be in the format of name@domain.com',
            'email.unique' => 'Email already exists',
            'email.max' => 'Email is at biggest 255 characters long',
            'password.min' => 'Password is at least 8 characters long',
            'password.required' => 'Password is required',
            'password.confirmed' => 'Password and Confirm Password no matching',
            'image.max' => 'Image image has the largest size is 5000kb',
            'image.image' => 'The image must be formatted jpeg, png, bmp, gif, or svg',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @param  string $imagePath
     * @return \App\User
     */
    protected function create(array $data,$imagePath)
    {
         $user = Users::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'department_id' => $data['department_id'],
            'date_of_birth' => $data['date_of_birth'],
            'image' => $imagePath,
        ]);

         $user->roles()->attach(Role::where('name', 'Member')->first());

         $verifyUser = VerifyUsers::create([
            'user_id' => $user->id,
            'confirmation_code'  => str_random(30).time()
        ]);
         
        //Mail::to($user->email)->send(new VerifyMail($user));
 
        return $user;
    }
}
