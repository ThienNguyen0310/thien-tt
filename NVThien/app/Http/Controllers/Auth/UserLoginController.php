<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\MessageBag;



class UserLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
      return view('auth.login');
    }

    public function login(Request $request)
    {
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);

      if (Auth::attempt(['email' => $request['email'],'password' => $request['password']],$request->filled('remember'))) {
            if($user = Auth::getLastAttempted()->confirmed){
                return redirect()->intended('/home');
            }
            else{
                auth()->logout();
                return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
            }
    } else {
        $error = "Tên đăng nhập hoặc mật khẩu không đúng";
        return redirect('/login')->with('error', $error);
        }
    }
}