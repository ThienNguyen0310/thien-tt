<?php

namespace App\Http\Controllers;

use App\Users;
use App\Department;
use App\Role;
use Event;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\VerifyUsers;
use Image;
use Auth;
use App\Events\SendMail;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role:Admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Users::paginate(1);
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $role = Role::all();
        $department = Department::all();
        return view('admin.users.create',compact('department','role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $user = new Users();
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->date_of_birth = $request->date_of_birth;
        $user->department_id = $request->department_id;
        if ($request->hasFile('image')){
            $imagePath = $request->file('image')->store('public/images');
            $image = Image::make(Storage::get($imagePath))->resize(300,300)->encode();
            Storage::put($imagePath,$image);
            $imagePath = explode('/',$imagePath);
            $imagePath = $imagePath[2];
            $user->image = $imagePath;
        }
        try {
            $user->save();
            $verifyUser = VerifyUsers::create([
                'user_id' => $user->id,
                'confirmation_code'  => str_random(30).time()
            ]);
         	Event::fire(new SendMail($user));

            $user->roles()->attach($request->role_id);
            return redirect()->route('users.index');
        } catch (Exception $e) {
            die($e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {//var_dump($users);die();
        $role = Role::all();
        $users = Users::find($id);
        $department = Department::all();
        return view('admin.users.edit',compact('users','department','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user = Users::find($id);
        $user->name = $request->name;   
        $user->password = Hash::make($request->passwork);
        $user->email = $request->email;
        $user->date_of_birth = $request->date_of_birth;
        $user->department_id = $request->department_id;
        if ($request->hasFile('image')){
            $imagePath = $request->file('image')->store('public/images');
            $image = Image::make(Storage::get($imagePath))->resize(300,300)->encode();
            Storage::put($imagePath,$image);
            $imagePath = explode('/',$imagePath);
            $imagePath = $imagePath[2];
            $user->image = $imagePath;
        }  
        if(Auth::user()->id != $id){
            $user->roles()->detach();
            $user->roles()->attach($request->role_id);
        }
        try {
            $user->save();
            return redirect()->route('users.index');
        } catch (Exception $e) {
            die($e-getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $users = Users::find($id);
            $users->deleted_at = date('Y-m-d H:i:s');
            $users->save();
            //$users->delete();
            return redirect()->route('users.index');
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
