<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Department;
use App\Http\Requests;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Image;
use Auth;

class MemberController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function getViewList(){
        $users =  Users::Where('department_id','=',Auth::user()->department_id)->paginate(2);
        return view('member.viewlist',compact('users'));
    }

    public function getViewDetail($id){
        $department = Department::all();
        $users = Users::find($id);
        return view('member.viewdetail',compact('users','department'));
    }
    public function updateUsers(UpdateUserRequest $request,$id){
        $user = Users::find($id);
        $user->name = $request->name;
        $user->password = $request->password;
        $user->date_of_birth = $request->date_of_birth;
        $user->department_id = $request->department_id;
        
        if ($request->hasFile('image')){
            $imagePath = $request->file('image')->store('public/images');
            $image = Image::make(Storage::get($imagePath))->resize(300,300)->encode();
            Storage::put($imagePath,$image);
            $imagePath = explode('/',$imagePath);
            $imagePath = $imagePath[2];
            $user->image = $imagePath;
        }
        try {
            $user->save();
            return redirect()->route('ajax.viewList');
        } catch (Exception $e) {
            die($e-getMessage());

        }
        return view('member.viewdetail',compact('users'));
    }

    public function getViewDetailSamDep($id){
        $department = Department::all();
        $users = Users::find($id);
        return view('member.viewdetailSamDep',compact('users','department'));
    }
}
