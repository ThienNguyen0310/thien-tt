<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Users extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password','department_id','date_of_birth','image','confirmed'
    ];

     protected $hidden = [
        'password', 'remember_token',
    ];

    public function department(){
    	return $this->belongsTo('App\Department');
    }

    public function verifyUsers(){
        return $this->hasOne('App\VerifyUsers','user_id');
    }

    public function roles(){
        return $this->belongsToMany('App\Role','role_user','user_id','role_id')->withTimestamps();
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) 
        {
            return true;
        }
        abort(401, 'This action is unauthorized.');
    }
        
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) 
        {
            foreach ($roles as $role) 
            {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } 
        else 
        {
            if ($this->hasRole($roles)) 
            {
                return true;
            }
        }
        return false;
    }
        
    public function hasRole($role)
    {//var_dump($this->roles());die();
        if ($this->roles()->where('name', $role)->first()) 
        {//var_dump('huhu');die();
            return true;
        }
        return false;
    }
    
    
}
