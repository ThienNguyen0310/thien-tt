<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
	use SoftDeletes;
    public $table = "department";

    public function users(){
    	return $this->hasMany('App\Users');
    }

}
