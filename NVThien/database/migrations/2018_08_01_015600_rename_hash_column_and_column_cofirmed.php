<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameHashColumnAndColumnCofirmed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
            $table->renameColumn('hash', 'confirmation_code');
        });
        Schema::table('users',function(Blueprint $table){
            $table->boolean('confirmed')->default(0)->after('confirmation_code');
            $table->string('confirmation_code')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){
            $table->renameColumn('confirmation_code','hash' );
        });
        Schema::table('users',function(Blueprint $table){
            $table->dropColumn('confirmed');
            $table->string('hash')->change();
        });
    }
}
