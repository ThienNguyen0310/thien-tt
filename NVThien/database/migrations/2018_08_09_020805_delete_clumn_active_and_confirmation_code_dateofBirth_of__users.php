<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteClumnActiveAndConfirmationCodeDateofBirthOfUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
            $table->boolean('confirmed')->default(0)->change();
            $table->string('date_of_birth')->nullable()->change();
            $table->dropColumn('active');
            $table->dropColumn('confirmation_code');
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){
            $table->string('confirmation_code')->nullable();
            $table->integer('active')->default(0);
            $table->boolean('confirmed')->change();
            $table->string('date_of_birth')->change();
        }); 
    }
}
