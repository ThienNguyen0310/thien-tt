<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
            $table->text('image')->nullable()->after('password');
            $table->integer('level')->unsigned();
            $table->integer('department_id')->unsigned()->nullable();
            $table->string('hash');
            $table->integer('active');
            $table->date('date_of_birth');
            $table->softDeletes();
            $table->foreign('department_id')->references('id')->on('department')->onDelete('SET NULL');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');

    }
}
