<?php

use Illuminate\Database\Seeder;
use App\Users;
use App\Role;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $role_admin = Role::where('name', 'Admin')->first();
        $role_member  = Role::where('name', 'Member')->first();

        $member = new Users();
        $member->name = 'thiennv1';
        $member->email = 'sky03101@gmail.com';
        $member->image = '';
        $member->department_id = 2;
        $member->confirmed = 0;
        $member->date_of_birth = '';
        $member->password = bcrypt('Khongcopass0310');
        $member->save();
        $member->roles()->attach($role_member);

        $admin = new Users();
        $admin->name = 'thiennv2';
        $admin->email = 'sky03101995@gmail.com';
        $admin->image = '';
        $admin->department_id = 1;
        $admin->confirmed = 1;
        $admin->date_of_birth = '';
        $admin->password = bcrypt('Khongcopass0310');
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
