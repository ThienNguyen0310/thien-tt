<?php

use Illuminate\Database\Seeder;

class Role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        $roles  = array(['id'=> 1,'name'=>'Admin','created_at'=>new DateTime(),
                            'updated_at'=>new DateTime()],
        					['id'=> 2,'name'=>'Member','created_at'=>new DateTime(),
                            'updated_at'=>new DateTime()]);
        DB::table('roles')->insert($roles);
    }
}
