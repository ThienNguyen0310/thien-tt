<?php

use Illuminate\Database\Seeder;

class Department extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('department')->delete();
        $department  = array(['id'=> 1,'name'=>'depart_1','created_at'=>new DateTime(),
                            'updated_at'=>new DateTime()],
        					['id'=> 2,'name'=>'depart_2','created_at'=>new DateTime(),
                            'updated_at'=>new DateTime()],
        					['id'=> 3,'name'=>'depart_3','created_at'=>new DateTime(),
                            'updated_at'=>new DateTime()]);
        DB::table('department')->insert($department);
    }
}
