<?php

use Illuminate\Database\Seeder;

class Role_Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_user')->delete();
        $role_user  = array(['id'=> 1,'role_id'=> 1,'user_id'=> 1,'created_at'=>new DateTime(),
                            'updated_at'=>new DateTime()],
        					['id'=> 2,'role_id'=> 2,'user_id'=> 2,'created_at'=>new DateTime(),
                            'updated_at'=>new DateTime()]);
        DB::table('role_user')->insert($role_user);
    }
}
