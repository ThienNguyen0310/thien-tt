<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'welcome haha';
});

Route::resource('/users','UserController');
Route::resource('/department','DepartmentController');

Auth::routes();
Route::get('/user/verify/{confirmation_code}','Auth\RegisterController@verifyUser')->name('activate.user');


Route::get('/home', 'HomeController@index')->name('home');
Route::prefix('ajax')->group(function () {
    Route::get('/viewList','MemberController@getViewList')->name('ajax.viewList');
	Route::get('/viewDetail/{id}','MemberController@getViewDetail')->name('ajax.viewDetail');
	Route::get('/viewDetailSamDep/{id}','MemberController@getViewDetailSamDep')->name('ajax.viewDetailSamDep');
	Route::post('/updateUsers{id}','MemberController@updateUsers')->name('ajax.updateUsers');
});


Route::post('/post-login','Auth\UserLoginController@login')->name('post-login');
