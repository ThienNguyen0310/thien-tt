<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Viewlist</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css') }}">

</head>
<body>
	<div class="content container-fluid"  >
			<div style="background-color:pink">
				<table class="table table-stripped">
					<tr>
						<a href="">
							<th width=100px><img src="{{asset(Auth::user()->image)}}" class="img-circle" width="100" height="70" margin-right=0px></th>
							<th><h2>{{Auth::user()->name }}</h2></th>
						</a>	
						<form class="" action="{{route('logout')}}" method="post">
          				@csrf
          				<button type="submit" name="logout" class="btn btn-success" style="float:right;margin-right: 40px">Logout</button>
      				</form>		
					</tr>
				</table>
				<div class="control clear-fix" style="float:right">
					<a href="{{route('ajax.viewList') }}" class="btn btn-warning" style="float: right;">Back</a>
				</div>
			</div>
			<div>
				<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="infor">
					<div class="form-group">
						<label for="name">Name</label>
						<input disabled type="text" required autocomplete="off" class="form-control" placeholder="name" name="name" id="name" value="{{ $users->name }}">
						 
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input disabled type="text" required autocomplete="off" class="form-control" placeholder="email" name="email" id="email" value="{{ $users->email}}">
						
					</div>
       			 	<div class="form-group">
						<label for="date_of_birth">Date of birth</label>
            			<div class='input-group date' id='datetimepicker'>
                			<input disabled data-format="yyyy-MM-dd" type='text' name = "date_of_birth" placeholder="Date of birth" class="form-control" value="{{$users->date_of_birth}}">
                				<span class="input-group-addon">
                   					<span class="glyphicon glyphicon-calendar"></span>
                				</span>
            			</div> 
        			</div>
					<div class="form-group">
						<label for="level">Level</label>
						<select name="level" class="form-control" disabled>
							@if(!$users->level)
								<option value='0' selected="selected">Member</option>
								<option value='1'>Admin</option>
							@else
								<option value='0' >Member</option>
								<option value='1' selected="selected">Admin</option>
							@endif
						</select>
					</div>
					<div class="form-group">
						<label for="department">Department</label>
						<select name="department_id" class="form-control" disabled>
							@foreach($department as $dep)
								@if($dep->id == $users->department_id)
									<option value="{{$dep->id }}" selected>{{$dep->name}}</option>
								@else
									<option value="{{$dep->id}}">{{$dep->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="image">Image</label>
						<input type="file" disabled name="image" class="form-file-control" id ="fileChooser" onchange="previewFile();">
						<img id = "preview" src="{{Storage::url('images/'.$users->image)}}">
					</div>
				</div>
			</div>
			</div>
		<!-- </form> -->
	</div>
</body>
</html>