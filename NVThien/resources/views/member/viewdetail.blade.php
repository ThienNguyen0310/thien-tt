<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Viewlist</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css') }}">

</head>
<body>
	<div class="content container-fluid"  >
		<form method="post" action="{{route('ajax.updateUsers',$users->id) }}" enctype="multipart/form-data" autocomplete="off" >
			@csrf
			<div style="background-color:pink">
				<table class="table table-stripped">
					<tr>
						<a href="">
							<th width=100px><img src="{{ asset($users->image)}}" class="img-circle" width="100" height="70" margin-right=0px></th>
							<th><h2>{{$users->name }}</h2></th>
						</a>	
						<form class="" action="{{route('logout')}}" method="post">
          				@csrf
          				<button type="submit" name="logout" class="btn btn-success" style="float:right;margin-right: 40px">Logout</button></a>
      					</form>		
					</tr>
				</table>
		    	<div class="control clear-fix" style="float:right">
					<a href="{{ route('ajax.viewList') }}" class="btn btn-warning" style="float: right;">Back</a>
					<button type="submit" class="btn btn-primary" style="float: right; margin-right: 5px">Update</button>
				</div>
			</div>
			<div>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="infor">
						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" required autocomplete="off" class="form-control" placeholder="name" name="name" id="name" value="{{ $users->name }}">
							<span class="text-danger">{{ $errors->first('name') }}</span> 
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input disabled type="text" required autocomplete="off" class="form-control" placeholder="email" name="email" id="email" value="{{ $users->email}}">
							<span class="text-danger">{{ $errors->first('email') }}</span> 
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" required autocomplete="off" class="form-control" placeholder="Password" id = "password" name="password" value="{{ $users->password }}">
							<span class="text-danger">{{ $errors->first('password') }}</span> 
						</div>
						<div class="form-group">
	              			<label>Confirm Password</label>
	              			<input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control" required autocomplete="off" value="{{ $users->password }}">
	              			<span class="text-danger">{{ $errors->first('password_confirmation') }}</span> 
	       			 	</div>
	       			 	<div class="form-group">
							<label for="date_of_birth">Date of birth</label>
	            			<div class='input-group date' id='datetimepicker'>
	                			<input data-format="yyyy-MM-dd" type='text' name = "date_of_birth" placeholder="Date of birth" class="form-control" value="{{$users->date_of_birth}}">
	                				<span class="input-group-addon">
	                   					<span class="glyphicon glyphicon-calendar"></span>
	                				</span>
	            			</div>
	            			<span class="text-danger">{{ $errors->first('date_of_birth') }}</span> 
	        			</div>
						<div class="form-group">
							<label for="level">Level</label>
							<select name="level" class="form-control" disabled>
								@if(!$users->level)
									<option value='0' selected="selected">Member</option>
									<option value='1'>Admin</option>
								@else
									<option value='0' >Member</option>
									<option value='1' selected="selected">Admin</option>
								@endif
							</select>
						</div>
						<div class="form-group">
							<label for="department">Department</label>
							<select name="department_id" class="form-control">
								@foreach($department as $dep)
									@if($dep->id == $users->department_id)
										<option value="{{$dep->id }}" selected>{{$dep->name}}</option>
									@else
										<option value="{{$dep->id}}">{{$dep->name}}</option>
									@endif
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="image">Image</label>
							<input type="file" name="image" class="form-file-control" id ="fileChooser" onchange="previewFile();">
							<img id = "preview" src="{{Storage::url('images/'.$users->image)}}">
							<span class="text-danger">{{ $errors->first('image') }}</span>
						</div>
						<div>
							<input type="hidden" value="{{ $users->id }}" name="users_id">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	
	<script type="text/javascript" src="{{asset('js/jquery-3.1.1.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/ckeditor.js')}}"></script>

	
	<script type="text/javascript">
		CKEDITOR.replace('content');
		function previewFile(){
			var preview = $('#preview');
			var file = $('input[type=file]').prop('files')[0];
			var reader = new FileReader();

			if(file){
				reader.readAsDataURL(file);
			}else{
				preview.attr('src', '');
			}
			reader.onloadend = function() {
				preview.attr('src', reader.result);
			}
		}
	</script>
</body>
</html>