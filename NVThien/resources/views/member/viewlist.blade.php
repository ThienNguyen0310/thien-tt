<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Viewlist</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">

</head>
<body>
	<div class="content container-fluid"  >
		<div style="background-color:pink">
			<table class="table table-stripped">
				<tr>
					<th width=100px>
						<a href="{{route('ajax.viewDetail',Auth::user()->id)}}">
							<img src="{{asset(Auth::user()->image)}}" class="img-circle" width="100" height="70" margin-right=0px>
						</a>
					</th>
					<th>
						<a href="{{route('ajax.viewDetail',Auth::user()->id) }}">
							<h2>{{Auth::user()->name}}</h2>
						</a>
					</th>	
					<form class="" action="{{route('logout')}}" method="post">
          				@csrf
          				<button type="submit" name="logout" class="btn btn-success" style="float:right;margin-right: 40px">Logout</button>
      				</form>	
				</tr>
			</table>
		</div>
		<div>
			<h2>Member cùng thuộc Department: {{Auth::user()->department->name}} với {{Auth::user()->name }}</h2>
			<table class="table table-stripped">
		      <tbody>
		        <tr>
		          <th>Id</th>
		          <th>Name</th>
		          <th>Department</th>
		          <th>Role</th>
		          <th>Image</th>
		          <th>Created at</th>
		          <th>Updated at</th>
		        </tr>
		        @foreach($users as $us)
		          	<tr>
			          <td>{{$us->id }}</td>
			          <td>{{$us->name }}</td>
			          <td>{{$us->department->name }}</td>
			          <td>
			              @if(!$us->level)
			                Member
			              @else
			                Admin
			              @endif
			           </td>
			          <td><img src="{{asset($us->image)}}"></td>
			          <td>{{$us->created_at}}</td>
			          <td>{{$us->updated_at}}</td>

			          <th><a class="btn btn-success" href="{{route('ajax.viewDetailSamDep',$us->id)}}">Details</a></th>
		        	</tr>
				@endforeach
		      </tbody>
  			</table>
		</div>
	</div>
</body>
</html>