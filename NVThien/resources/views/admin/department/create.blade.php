@extends('admin.layouts.master')
@section('title','Department Create')
@section('content')

	<div class="content container">
		<form id="create_dep" method="post" autocomplete="off" action="{{route('department.store') }}" >
			@csrf
			<div class="row">
				<div class="title col-md-3"><h3>Create Department</h3></div>
				<div class="nav-function col-md-3 col-md-offset-6">
					<h3>
						<button id="btn-submit" class="btn btn-primary" style="float: right; margin-left: 5px">Save</button>
						<a href="{{route('department.index') }}" class="btn btn-warning" style="float: right;">Back</a>
					</h3>
				</div>
			</div>
			<div class="form-group">
		    	<label for="name">Name</label>
		    	<input type="text" id="name" required autocomplete="off" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}">
				<span class="text-danger">{{ $errors->first('name') }}</span> 	
			</div>
		</form>
	</div>
@endsection
@section('javascript')
	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<!-- <script type="text/javascript" src="{{ asset('js/validate.js') }}"></script> -->
	
	<!-- <script type="text/javascript">
		$('#name').keyup(function(){
		    html='';
		    $('#nameError1').html(html);
		    $("#nameError1").removeClass('alert alert-danger');
		});

		$('#btn-submit').click(function (){
	    	$('#nameError1').html('');
	    	var name = $('#name').val();
	   		$.ajax({
	        	url : 'index.php?url=DepartmentController/checkName',
	        	type : 'post',
	        	dataType : 'json',
	        	data : {
	            	name : name,      
	        	},
	        	success : function (result){
		            var html = '';
		            if (result.error == true){
		                html += result.message;
		            }

		            if (html != ''){
		                $('#nameError1').append(html);
		                $('#nameError1').addClass('alert alert-danger');
		            }
		            else {
		                $('form').submit();
		            }
		        }
		    });
	    	return false;
		});
	</script> -->
@endsection
	