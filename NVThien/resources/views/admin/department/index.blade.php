@extends('admin.layouts.master')
@section('title','Department Index')
@section('content')

  <div class="content container-fluid">
    <h2>List Department</h2>
    <div class="control clear-fix">
      <a class="btn btn-primary create" href="{{route('department.create')}}">Create</a>
    </div>
    <table class="table table-stripped">
      <tbody>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Created at</th>
          <th>updated at</th>
          <th></th>
          <th></th>
        </tr>
        @foreach($department as $dep)
          <tr>
          <td>{{$dep->id}}</td>
          <td>{{$dep->name}}</td>
          <td>{{$dep->created_at}}</td>
          <td>{{$dep->updated_at}}</td>
          <td>
            <a href="{{route('department.edit',$dep->id)}}" class="btn btn-success btn-mini pull-left">Edit</a>
          </td>
          <td>
              <form method="POST" action="{{route('department.destroy',$dep->id)}}" accept-charset="UTF-8">
                @csrf
                @method('DELETE')
              <button type="submit" class="btnDelete btn btn-danger btn-mini">Delete</button>
              </form>
          </td>
          </tr>
        @endforeach
      </tbody>
    </table>

    <div style="float: right">
      {{ $department->links() }}
    </div>

  </div>
@endsection
@section('javascript')
  <script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
  </script>
@endsection