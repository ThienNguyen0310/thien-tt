@extends('admin.layouts.master')
@section('title','Departmant Edit')
@section('content')

	<div class="content container">
		<form id="edit_dep" autocomplete="off" method="post" action="{{route('department.update',$department->id) }}" >
			@csrf
			@method('PUT')
			<div class="row">
				<div class="Name col-md-3"><h3>Edit Department</h3></div>
				<div class="nav-function col-md-3 col-md-offset-6">
					<h3>
						<button type="submit" class="btn btn-primary" style="float: right; margin-left: 5px">Save</button>
						<a href="{{route('department.index') }}" class="btn btn-warning" style="float: right;">Back</a>
					</h3>
				</div>
			</div>
			<div class="form-group">
		    	<label for="name">Name</label>
		    	<input type="text" required autocomplete="off" class="form-control" name="name" placeholder="Name" value="{{$department->name }}">
				<span class="text-danger">{{ $errors->first('name')}}</span>
				<input type="hidden" value="{{ $department->id }}" name="department_id">
			</div>
		  	
		</form>
	</div>
@endsection 
<!-- @section('javascript')

@endsection -->