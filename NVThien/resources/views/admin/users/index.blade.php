@extends('admin.layouts.master')
@section('title','User Index')
@section('content')
  <div class="content container-fluid">
    <h2>List Users</h2>
    <div class="control clear-fix">
      <a class="btn btn-primary create" href="{{route('users.create') }}">Create</a>
    </div>
    <table class="table table-stripped">
      <tbody>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Day of Birth</th>
          <th>Department</th>
          <th>Email</th>
          <th>Role</th>
          <th>Image</th>
          <th>Created at</th>
          <th>Updated at</th>
          

          <th></th>
          <th></th>
        </tr>
        @foreach($users as $us)
          <tr>
          <td>{{ $us->id }}</td>
          <td>{{ $us->name }}</td>
          <td>{{ $us->date_of_birth }}</td>
          @if($us->department=="")
            <td>Null</td>
          @else
            <td>{{ $us->department->name }}</td>
          @endif
          <td>{{ $us->email }}</td>
          <td>@foreach($us->roles as $roles)
              {{$roles->name}}
              @endforeach
          </td>
          <td><img src="{{ Storage::url('images/'.$us->image)}}"></td>

          <td>{{ $us->created_at }}</td>
          <td>{{ $us->updated_at }}</td>

          <th><a class="btn btn-success" href="{{route('users.edit',$us->id)}}">Edit</a></th>
          <th>
            <form method="POST" action="{{route('users.destroy',$us->id)}}" accept-charset="UTF-8">
              @csrf
              @method('DELETE')
            <button class="btn btn-danger btnDelete">Delete</button>
            </form>
          </th>
        </tr>
       @endforeach
      </tbody>
    </table>

    <div style="float: right">
      {{ $users->links() }}
    </div>

  </div>

@endsection
@section('javascript')
<script type="text/javascript">
    $(".btnDelete").click(function (){
      var isDelete = confirm('Are you sure want to delete ?');
      if(isDelete){
        return true;
      }else{
        return false;
      }
    });
</script>
@endsection