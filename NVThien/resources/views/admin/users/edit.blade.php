@extends('admin.layouts.master')
@section('title','User Edit')
@section('content')

	<div class="content container">
		<form method="post" action="{{route('users.update',$users->id) }}" enctype="multipart/form-data" autocomplete="off" id = "edit_user">
			@csrf
			@method('PUT')
			<div class="row">
				<div class="title col-md-3"><h3>Edit users</h3></div>
				<div class="nav-function col-md-3 col-md-offset-6">
					<h3>
						<button id= "btn-submit" type="submit" class="btn btn-primary" style="float: right; margin-left: 5px">Save</button>
						<a href="{{route('users.index') }}" class="btn btn-warning" style="float: right;">Back</a>
					</h3>
				</div>
			</div>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="infor">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" required autocomplete="off" class="form-control" placeholder="name" name="name" id="name" value="{{ $users->name }}">
						<span class="text-danger">{{ $errors->first('name') }}</span> 
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="text" required autocomplete="off" class="form-control" placeholder="email" name="email" id="email" value="{{ $users->email}}">
						<span class="text-danger">{{ $errors->first('email') }}</span> 
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" required autocomplete="off" class="form-control" placeholder="Password" id = "password" name="password" value="{{ $users->password }}">
						<span class="text-danger">{{ $errors->first('password') }}</span> 
					</div>
					<div class="form-group">
              			<label>Confirm Password</label>
              			<input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control" required autocomplete="off" value="{{ $users->password }}">
              			<span class="text-danger">{{ $errors->first('password_confirmation') }}</span> 
       			 	</div>
       			 	<div class="form-group">
						<label for="date_of_birth">Date of birth</label>
            			<div class='input-group date' id='datetimepicker'>
                			<input data-format="yyyy-MM-dd" type='text' name = "date_of_birth" placeholder="Date of birth" class="form-control" value="{{$users->date_of_birth}}">
                				<span class="input-group-addon">
                   					<span class="glyphicon glyphicon-calendar"></span>
                				</span>
            			</div>
            			<span class="text-danger">{{ $errors->first('date_of_birth') }}</span> 
        			</div>
					<div class="form-group">
						<label for="role_id">Level</label>
						<select name="role_id" class="form-control">
							@foreach($role as $rol)
								@if($users->roles[0]->id == $rol->id)
									<option value="{{$rol->id}}" selected="selected">{{$rol->name}}</option>
								@else
									<option value="{{$rol->id}}" >{{$rol->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="department">Department</label>
						<select name="department_id" class="form-control">
							@foreach($department as $dep)
								@if($dep->id == $users->department_id)
									<option value="{{$dep->id }}" selected>{{$dep->name}}</option>
								@else
									<option value="{{$dep->id}}">{{$dep->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="files" style="width:120px" class="btn btn-primary btn-block btn-outlined">Select Image</label>
						<input type="file" style="visibility:hidden;" name="image" class="form-file-control" id ="files" onchange="previewFile();">
						<img id = "preview" src="{{Storage::url('images/'.$users->image)}}">
						<span class="text-danger">{{ $errors->first('image') }}</span>
					</div>
					<div>
						<input type="hidden" value="{{ $users->id }}" name="users_id">
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{ asset('js/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.js')}}" charset="UTF-8"></script>
<script type="text/javascript">
	//CKEDITOR.replace('content');
	$(function() {              
           $('#datetimepicker').datetimepicker({
           		pickTime: false,
        		minView: 2,
        		format: 'dd/mm/yyyy',
        		autoclose: true,
           });
    }); 
    
	function previewFile(){	
		var preview = $('#preview');
		var file = $('input[type=file]').prop('files')[0];
		var reader = new FileReader();
		if(file){
			reader.readAsDataURL(file);
		}else{
			preview.attr('src', '');
		}
		reader.onloadend = function() {
			preview.attr('src', reader.result);
		}
	}
</script>
@endsection