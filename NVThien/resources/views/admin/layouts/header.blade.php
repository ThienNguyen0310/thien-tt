 <div class="header container-fluid">
    <div class="header-top row">
      <!-- <button class="btn btn-success" style="float:right;margin-right: 40px"  ><a style="color: white" href="{{route('logout')}}">Logout</a></button> -->
      <form class="" action="{{route('logout')}}" method="post">
          {{ csrf_field() }}
          <button type="submit" name="logout" class="btn btn-success" style="float:right;margin-right: 40px">Logout</button>
      </form>
    </div>
    <div class="main-menu">
      <nav class="navbar navbar-inverse">
        <a class="navbar-brand" href="{{ route('department.index') }}">Department</a>
        <a class="navbar-brand" href="{{route('users.index') }}">Users</a>   
      </nav>
    </div>
  </div>  